package com.mingroup.zochatvn.ui.mainscreen.group.addgroup;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.DocumentReference;

import java.util.ArrayList;
import java.util.List;

public class CombineListViewModel extends ViewModel {
    private MutableLiveData<List<DocumentReference>> combineList;

    public MutableLiveData<List<DocumentReference>> getCombineList() {
        if (combineList == null) {
            combineList = new MutableLiveData<>();
            combineList.setValue(new ArrayList<>());
        }
        return combineList;
    }

    public void setCombineList(List<DocumentReference> selectedUserList) {
        combineList.setValue(selectedUserList);
    }

    public void addUser(DocumentReference user) {
        List<DocumentReference> list = getCombineList().getValue();
        if (list != null) {
            list.add(user);
        }
        combineList.setValue(list);
    }

    public void removeUser(DocumentReference user) {
        List<DocumentReference> list = getCombineList().getValue();
        if (list != null) {
            list.remove(user);
        }
        combineList.setValue(list);
    }

    public void insertList(List<DocumentReference> newList) {
        List<DocumentReference> list = getCombineList().getValue();
        if (list != null) {
            list.addAll(newList);
        }
        combineList.setValue(list);
    }
}
