package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.chatoptions.groupmembers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.util.Log;
import com.google.firebase.firestore.DocumentReference;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;
import lombok.Getter;

public class GroupMemberAdapter extends RecyclerView.Adapter<GroupMemberAdapter.GroupMemberVH> {
    private static final String TAG = GroupMemberAdapter.class.getSimpleName();
    private List<DocumentReference> users;

    public GroupMemberAdapter(List<DocumentReference> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public GroupMemberVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GroupMemberVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_member_group, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GroupMemberVH holder, int position) {
        DocumentReference userRef = users.get(position);
        userRef.get()
                .addOnSuccessListener(documentSnapshot -> {
                    User user = documentSnapshot.toObject(User.class);
                    ComponentsUtil.displayImage(holder.getIvAvatar(), user.getPhotoUrl());
                    holder.getTvDisplayName().setText(user.getDisplayName());
                })
                .addOnFailureListener(e -> Log.d(TAG, "::onFailure: " + e.getMessage()));

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Getter
    public class GroupMemberVH extends RecyclerView.ViewHolder {
        private RoundedImageView ivAvatar;
        private TextView tvDisplayName;

        public GroupMemberVH(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.gr_member_avatar);
            tvDisplayName = itemView.findViewById(R.id.gr_member_displayName);
        }
    }
}
