package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder;

import android.os.Build;

import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ChatItemType {
    CHAT_ITEM_RIGHT_TEXT(1),
    CHAT_ITEM_RIGHT_IMAGE(2),
    CHAT_ITEM_RIGHT_STICKER(3),
    CHAT_ITEM_RIGHT_VIDEO(4),
    CHAT_ITEM_RIGHT_FILE(5),
    CHAT_ITEM_LEFT_TEXT(6),
    CHAT_ITEM_LEFT_IMAGE(7),
    CHAT_ITEM_LEFT_STICKER(8),
    CHAT_ITEM_LEFT_VIDEO(9),
    CHAT_ITEM_LEFT_FILE(10);


    private int typeId;

    public static ChatItemType getChatItemType(final int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Stream.of(ChatItemType.values())
                    .filter(targetStatus -> targetStatus.getTypeId() == id).findFirst().get();
        } else {
            for (ChatItemType chatItemType : ChatItemType.values()) {
                if (chatItemType.getTypeId() == id)
                    return chatItemType;
            }
        }
        return null;
    }
}
