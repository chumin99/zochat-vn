package com.mingroup.zochatvn.ui.register;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserFormViewModel extends ViewModel {
    private MutableLiveData<String> displayName = new MutableLiveData<>();
    private MutableLiveData<String> username = new MutableLiveData<>();
    private MutableLiveData<String> authMethod = new MutableLiveData<>();

    public void setDisplayName(String dName) {
        displayName.setValue(dName);
    }

    public void setUsername(String phoneNum) {
        username.setValue(phoneNum);
    }

    public MutableLiveData<String> getDisplayName() {
        return displayName;
    }

    public MutableLiveData<String> getUsername() {
        return username;
    }

    public MutableLiveData<String> getAuthMethod() {
        return authMethod;
    }

    public void setAuthMethod(String authType) {
        authMethod.setValue(authType);
    }
}
