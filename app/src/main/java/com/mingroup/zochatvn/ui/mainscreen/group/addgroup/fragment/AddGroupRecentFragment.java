package com.mingroup.zochatvn.ui.mainscreen.group.addgroup.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cpoopc.scrollablelayoutlib.ScrollableHelper;
import com.mingroup.zochatvn.R;

public class AddGroupRecentFragment extends Fragment implements ScrollableHelper.ScrollableContainer {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_group_recent, container, false);
    }

    @Override
    public View getScrollableView() {
        return getView();
    }
}
