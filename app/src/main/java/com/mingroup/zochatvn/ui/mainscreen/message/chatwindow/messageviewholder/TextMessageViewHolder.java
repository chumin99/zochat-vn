package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import com.mingroup.zochatvn.R;
import lombok.Getter;

@Getter
public class TextMessageViewHolder extends RecyclerView.ViewHolder {
    private RoundedImageView ivAvatar;
    private TextView tvDisplayName, tvContent, tvTimestamp, tvBreakTimeStamp;
    private OnMessageClickListener onMessageClickListener;

    public TextMessageViewHolder(@NonNull View itemView, OnMessageClickListener onChatItemListener, boolean fromLeft) {
        super(itemView);
        tvContent = itemView.findViewById(R.id.text_message_item_content);
        tvTimestamp = itemView.findViewById(R.id.text_message_item_timestamp);
        tvBreakTimeStamp = itemView.findViewById(R.id.break_timestamp);

        this.onMessageClickListener = onChatItemListener;

        if (fromLeft) {
            ivAvatar = itemView.findViewById(R.id.text_message_item_avatar);
            tvDisplayName = itemView.findViewById(R.id.text_message_item_display_name);
        }

        itemView.setOnLongClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemLongClickListener(position);
                    return true;
                }
            }
            return false;
        });

        itemView.setOnClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemClickListener(position);
                }
            }
        });
    }
}
