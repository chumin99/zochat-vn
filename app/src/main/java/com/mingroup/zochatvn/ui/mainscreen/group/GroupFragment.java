package com.mingroup.zochatvn.ui.mainscreen.group;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.data.model.ConservationType;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.AddGroupActivity;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.ChatActivity;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

import org.parceler.Parcels;

public class GroupFragment extends Fragment implements ConservationListAdapter.OnConservationItemListener, View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private TableRow btnCreateGroup;
    private TextView btnSort, tvGroupCount;
    private RecyclerView rvGroupList;
    private FirebaseFirestore db;
    private ConservationListAdapter adapter;
    private List<Conservation> participatingGroups;
    private LinearLayout noDataSectionLayout;
    private Button btnAddNewGroup;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mainscreen_group, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        setupGroupRecyclerView();
    }

    private void setupGroupRecyclerView() {
        adapter = new ConservationListAdapter(getActivity(), participatingGroups, this);
        rvGroupList.setAdapter(adapter);
        populateDataToRecyclerView();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void populateDataToRecyclerView() {
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .whereArrayContains("members", ComponentsUtil.getCurrentUserRef(db))
                .whereEqualTo("type", ConservationType.GROUP)
                .addSnapshotListener((value, error) -> {
                    if(error != null) {
                        Log.d(TAG, "Listen Failed: " + error.getMessage());
                        return;
                    }

                    if (value != null) {
                        participatingGroups.clear();
                        value.getDocuments().forEach(documentSnapshot -> {
                            Conservation conservation = documentSnapshot.toObject(Conservation.class);
                            if (conservation != null) {
                                conservation.setId(documentSnapshot.getId());
                                participatingGroups.add(conservation);
                            }
                        });
                        adapter.update(participatingGroups);
                    }

                    updateGroupListUI();
                });
    }

    private void updateGroupListUI() {
        if (adapter.getItemCount() > 0) {
            noDataSectionLayout.setVisibility(View.GONE);
        } else {
            noDataSectionLayout.setVisibility(View.VISIBLE);
        }
        tvGroupCount.setText("Nhóm đang tham gia " + " (" + adapter.getItemCount() + ")");
    }

    private void init(View view) {
        db = FirebaseFirestore.getInstance();
        participatingGroups = new ArrayList<>();
        btnCreateGroup = view.findViewById(R.id.btnCreateGroup);
        btnSort = view.findViewById(R.id.btnGroupSort);
        rvGroupList = view.findViewById(R.id.rvGroupList);
        tvGroupCount = view.findViewById(R.id.label_group_list);
        noDataSectionLayout = view.findViewById(R.id.group_no_data_section);
        btnAddNewGroup = view.findViewById(R.id.btn_add_group);

        btnCreateGroup.setOnClickListener(this);
        btnAddNewGroup.setOnClickListener(this);
    }

    @Override
    public void onConservationItemClick(int position) {
        Conservation selectedGroup = adapter.getItem(position);
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra("conservation", Parcels.wrap(selectedGroup));
        startActivity(intent);
    }

    @Override
    public void onConservationItemLongClick(int position) {
        CharSequence[] actions = {"Xoá nhóm"};
        Conservation group = adapter.getItem(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tác vụ")
                .setItems(actions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Bạn có chắc chắn muốn xoá?")
                                .setPositiveButton("Xoá", (dialogInterface1, i1) -> {

                                    db.collection(CollectionConst.COLLECTION_CONSERVATION)
                                            .document(group.getId())
                                            .delete()
                                            .addOnSuccessListener(unused -> {
                                                dialogInterface1.dismiss();
                                                adapter.removeItem(position);
                                                updateGroupListUI();
                                            })
                                            .addOnFailureListener(e -> {
                                                Log.d(TAG, "deleteGroup::onError: " + e.getMessage());
                                                Toast.makeText(getActivity(), "Xoá nhóm xảy ra sự cố. Chúng tôi sẽ kiểm tra lại sau!", Toast.LENGTH_SHORT).show();
                                            });
                                })
                                .create()
                                .show();
                    }
                })
                .setNegativeButton("Huỷ", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .create()
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_group:
            case R.id.btnCreateGroup:
                startActivity(new Intent(getActivity(), AddGroupActivity.class));
                break;
        }
    }
}
