package com.mingroup.zochatvn.ui.mainscreen.more.profile.editprofile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.makeramen.roundedimageview.RoundedImageView;


import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.constant.StorageRefConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.request.UserProfileRequest;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.file.FileUtils;

import gun0912.tedimagepicker.builder.TedImagePicker;
import lombok.SneakyThrows;

import static com.mingroup.zochatvn.util.ComponentsUtil.displayImageFromApi;
import static com.mingroup.zochatvn.util.StringUtil.convertDatePickerTextToFormattedDate;
import static com.mingroup.zochatvn.util.StringUtil.convertDateToString;
import static com.mingroup.zochatvn.util.StringUtil.convertStringToDate;
import static com.mingroup.zochatvn.util.StringUtil.getInputText;

public class EditProfileActivity extends AppCompatActivity {
    private static final String DOB_DATE_PICKER_TAG = "DatePickerDob";
    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private static final int TAKE_PHOTO_CODE = 444;

    private Toolbar zcToolbar;
    private RoundedImageView avatar;
    private TextInputLayout tipDisplayName;
    private RadioGroup rgGender;
    private RadioButton rbMale, rbFemale;
    private TextView dob;
    private ImageView btnChangeProfilePicture;
    private Button btnUpdate;
    private TableRow itemTakeAPicture, itemChooseExistingPicture;
    private RelativeLayout relativeLayout;
    private FirebaseFirestore db;
    private Long userDob;
    private boolean hasUserProfile = true;
    private Uri avatarUri;
    private AlertDialog showChangeProfilePictureDialog;
    private StorageReference mStorageRef;
    private FirebaseUser currentUser;
    private UserProfileRequest userProfile;

    @SneakyThrows
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen_more_profile_edit_profile);
        init();
        setupActionBar();
        setupDatePickerDialog();
        displayCurrentUserInfo();
        tipDisplayName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateForm();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateForm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        View.OnClickListener changeAvatar = view -> showChangeProfilePictureDialog();
        btnChangeProfilePicture.setOnClickListener(changeAvatar);
        avatar.setOnClickListener(changeAvatar);
    }

    private void displayCurrentUserInfo() throws ParseException {
        userProfile = UserProfileRequest.convertFromUser(new SessionManager(this).getCurrentUserInfo());

        tipDisplayName.getEditText().setText(userProfile.getDisplayName());
        if (userProfile.isMale())
            rbMale.setChecked(true);
        else rbFemale.setChecked(true);

        Date userDate = userProfile.getDob();
        if (userDate == null) {
            userDate = new Date();
        }

        userDob = userDate.getTime();
        dob.setText(convertDateToString(new Date(userDob)));

        displayImageFromApi(avatar, userProfile.getPhotoUrl());
    }

    private void setupActionBar() {
        setSupportActionBar(zcToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.profile_modify));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (!hasUserProfile) {
                btnUpdate.performClick();
            }
            onBackPressed();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showChangeProfilePictureDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.ZOchatTheme_RoundedDialog);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_change_profile_picture, null);
        itemChooseExistingPicture = dialogView.findViewById(R.id.change_avatar_choose_existing_photo_selection);
        itemTakeAPicture = dialogView.findViewById(R.id.change_avatar_take_a_picture_selection);

        builder.setView(dialogView);
        showChangeProfilePictureDialog = builder.create();
        showChangeProfilePictureDialog.show();

        itemTakeAPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TedPermission.with(EditProfileActivity.this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                takeAPictureFromCamera();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(relativeLayout, "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA)
                        .check();
            }
        });
        itemChooseExistingPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TedPermission.with(EditProfileActivity.this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                pickImageFromGallery();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(relativeLayout, "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                        .check();
            }
        });
    }

    private void takeAPictureFromCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    private void pickImageFromGallery() {
        TedImagePicker.with(this)
                .title("Chọn ảnh")
                .buttonText("Xong")
                .buttonBackground(R.color.blue_magenta)
                .buttonTextColor(android.R.color.white)
                .start(uri -> {
                    avatarUri = uri;
                    ComponentsUtil.displayImage(avatar, uri.toString());
                    avatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    showChangeProfilePictureDialog.dismiss();
                });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            avatarUri = FileUtils.getImageUriFromBitmap(EditProfileActivity.this, (Bitmap) data.getExtras().get("data"));
            Bitmap avatarBitmap = (Bitmap) data.getExtras().get("data");
            avatar.setImageBitmap(Bitmap.createScaledBitmap(avatarBitmap,
                    (int) getResources().getDimension(R.dimen._100dp),
                    (int) getResources().getDimension(R.dimen._100dp), true));
            showChangeProfilePictureDialog.dismiss();
        }
    }

    private void setupDatePickerDialog() {
        dob.setOnClickListener(view -> {
            Locale locale = new Locale("vi", "VN");
            Locale.setDefault(locale);
            Configuration config = getBaseContext().getResources().getConfiguration();
            config.setLocale(locale);
            createConfigurationContext(config);

            MaterialDatePicker<Long> materialDatePicker =
                    MaterialDatePicker.Builder.datePicker()
                            .setTitleText("Chọn ngày sinh")
                            .setSelection(userDob)
                            .build();
            materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
                @Override
                public void onPositiveButtonClick(Long selection) {
                    try {
                        Date pickedDate = convertDatePickerTextToFormattedDate(materialDatePicker.getHeaderText());
                        dob.setText(convertDateToString(pickedDate));
                    } catch (ParseException e) {
                        Log.d("DatePicker", e.getMessage());
                    }
                }
            });
            materialDatePicker.show(getSupportFragmentManager(), DOB_DATE_PICKER_TAG);
        });
    }


    private UserProfileRequest getUserInfo() throws ParseException {
        String displayName = getInputText(tipDisplayName);
        boolean isMale = false;
        if (rgGender.getCheckedRadioButtonId() == rbMale.getId())
            isMale = true;
        String dateStr = dob.getText().toString();
        Date birthDate = null;
        if (!dateStr.isEmpty()) {
            birthDate = convertStringToDate(dob.getText().toString());
        }

        String photoUrl = "";
        if(avatarUri != null) {
            photoUrl = avatarUri.toString();
        } else {
            photoUrl = userProfile.getPhotoUrl();
        }

        return new UserProfileRequest(displayName, birthDate, isMale, photoUrl);
    }

    private void init() {
        zcToolbar = findViewById(R.id.zochat_toolbar_no_search_view);
        avatar = findViewById(R.id.edit_profile_avatar);
        tipDisplayName = findViewById(R.id.tipDisplayNameProfile);
        rgGender = findViewById(R.id.rgGender);
        rbFemale = findViewById(R.id.rbGenderFemale);
        rbMale = findViewById(R.id.rbGenderMale);
        dob = findViewById(R.id.tvDisplayDob);
        btnUpdate = findViewById(R.id.btnUpdateProfile);
        btnChangeProfilePicture = findViewById(R.id.btnChangeProfilePicture);
        relativeLayout = findViewById(R.id.edit_profile_rl);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();

        if (getIntent().getExtras() != null) {
            hasUserProfile = getIntent().getBooleanExtra("hasUserProfile", true);
        }
    }

    private void validateForm() {
        if (TextUtils.isEmpty(getInputText(tipDisplayName))) {
            btnUpdate.setEnabled(false);
            btnUpdate.setAlpha(.3f);
        } else {
            btnUpdate.setEnabled(true);
            btnUpdate.setAlpha(1f);
        }
    }

    @SneakyThrows
    public void doUpdateProfile(View view) {
        if (avatarUri != null) {
            uploadAvatar();
        } else {
            try {
                createOrUpdateUserProfile();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    private void uploadAvatar() {
        StorageReference avatarRef = mStorageRef.child(StorageRefConst.FOLDER_USER_AVATAR)
                                                .child(currentUser.getUid());
        avatarRef.putFile(avatarUri)
                .addOnSuccessListener(taskSnapshot -> {
                    Task<Uri> task = Objects.requireNonNull(Objects.requireNonNull(taskSnapshot.getMetadata()).getReference()).getDownloadUrl();
                    task
                            .addOnSuccessListener(uri -> {
                                avatarUri = uri;
                                try {
                                    createOrUpdateUserProfile();
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            })
                            .addOnFailureListener(e -> Log.d(TAG, "upload_avatar::onFailure: " + e.getMessage()));
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(EditProfileActivity.this, "Update ảnh đại diện xảy ra lỗi! Thử lại.", Toast.LENGTH_SHORT).show();
                        Log.d("upload_file_error", e.getMessage());
                    }
                });
    }

    private void createOrUpdateUserProfile() throws ParseException {
        Log.d("getUserInfo", getUserInfo().toString());
        User updateUser = User.updateUserProfile(currentUser, getUserInfo());

        DocumentReference currentUserRef = db.collection(CollectionConst.COLLECTION_USER)
                .document(Objects.requireNonNull(FirebaseAuth.getInstance().getUid()));
        currentUserRef
                .set(updateUser)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUserRef.get().addOnSuccessListener(documentSnapshot -> {
                            User currentUser = documentSnapshot.toObject(User.class);

                            if (currentUser != null) {
                                try {
                                    new SessionManager(getApplicationContext()).saveCurrentUserInfo(currentUser);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Intent returnIntent = new Intent();
                                setResult(RESULT_OK, returnIntent);
                                finish();
                            } else {
                                Log.d(TAG, "createOrUpdateUserProfile::onFailure: " + Objects.requireNonNull(task.getException()).getMessage());
                            }
                        });
                    }
                });
    }
}
