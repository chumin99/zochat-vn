package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.chatoptions;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.makeramen.roundedimageview.RoundedDrawable;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.util.List;
import java.util.Objects;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.constant.StorageRefConst;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.data.model.ConservationType;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.ui.mainscreen.MainScreenActivity;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.AddGroupActivity;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.chatoptions.groupmembers.GroupMemberAdapter;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.StringUtil;
import com.mingroup.zochatvn.util.file.FileUtils;
import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.listener.OnSelectedListener;

public class ChatOptionsActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = ChatOptionsActivity.class.getSimpleName();
    private static final int TAKE_PHOTO_CODE = 132;
    private ImageButton ibnGoToStoredMedia;
    private Toolbar zcToolBar;
    private Conservation conservation;
    private User user;
    private RoundedImageView ivChatAvatar;
    private ImageView ivChangeAvatar;
    private TextView tvChatName, tvGroupMemberCount, tvGroupDesc;
    private ImageButton ibnEditGroupName;
    private LinearLayout btnSearchMessage, btnAddGroupMember, btnChangeBackground, btnMuteNoti;
    private ConstraintLayout btnAddGroupDesc, btnStoredMedia;
    private LinearLayout btnShowGroupMember, btnLeaveGroup, btnRemoveGroup;
    private LinearLayout btnCreateGroupWith, btnAddPersonToGroup, btnShowMutualGroup;
    private LinearLayout singleChatSection, groupChatSection;
    private String selectedPhotoUrl;
    private BottomSheetDialog changeGroupPhotoDialog;

    private FirebaseFirestore db;
    private StorageReference mStorageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_options);
        init();
        setupToolbar();
        if (conservation.getType() == ConservationType.SINGLE) {
            displaySingleChatDetail();
        } else {
            displayGroupChatDetail();
        }
    }

    private void displayGroupChatDetail() {
        singleChatSection.setVisibility(View.GONE);
        groupChatSection.setVisibility(View.VISIBLE);

        ComponentsUtil.displayImage(ivChatAvatar, conservation.getGroupPhotoUrl());
        tvChatName.setText(conservation.getTitle());
        tvGroupMemberCount.setText("(" + conservation.getMembers().size() + ")");
    }

    private void displaySingleChatDetail() {
        ivChangeAvatar.setVisibility(View.GONE);
        ibnEditGroupName.setVisibility(View.GONE);
        btnAddGroupMember.setVisibility(View.GONE);
        groupChatSection.setVisibility(View.GONE);
        btnAddGroupDesc.setVisibility(View.GONE);
        singleChatSection.setVisibility(View.VISIBLE);
        tvChatName.setText(user.getDisplayName());
        ComponentsUtil.displayImageFromApi(ivChatAvatar, user.getPhotoUrl());
    }

    private void setupToolbar() {
        zcToolBar.setTitle("Tuỳ chọn");
        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init() {
        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);

        if (getIntent().getExtras() != null) {
            conservation = Parcels.unwrap(getIntent().getParcelableExtra("conservation"));
            user = Parcels.unwrap(getIntent().getParcelableExtra("chat_friend"));
        }

        ivChatAvatar = findViewById(R.id.ivChatAvatar);
        ivChatAvatar.setOnClickListener(this);

        tvChatName = findViewById(R.id.tvChatName);
        tvChatName.setOnClickListener(this);

        btnSearchMessage = findViewById(R.id.btn_chat_search_message);
        btnChangeBackground = findViewById(R.id.btn_chat_change_bg);
        btnMuteNoti = findViewById(R.id.btn_mute_unmute_noti);
        btnStoredMedia = findViewById(R.id.btnStoredMedia);

        btnCreateGroupWith = findViewById(R.id.createGroupWith);
        btnAddPersonToGroup = findViewById(R.id.btnAddToGroup);
        btnShowMutualGroup = findViewById(R.id.btnShowMutualGroup);
        groupChatSection = findViewById(R.id.chat_group_section);
        btnAddGroupDesc = findViewById(R.id.btnAdddGroupDesc);

        btnShowGroupMember = findViewById(R.id.btnGroupMember);
        btnShowGroupMember.setOnClickListener(this);

        btnLeaveGroup = findViewById(R.id.btnLeaveGroup);
        btnRemoveGroup = findViewById(R.id.btnRemoveGroup);
        btnAddGroupMember = findViewById(R.id.btn_add_gr_member);

        ivChangeAvatar = findViewById(R.id.btnChangeAvatar);
        ivChangeAvatar.setOnClickListener(this);

        ibnEditGroupName = findViewById(R.id.ibnEditGroupName);
        ibnEditGroupName.setOnClickListener(this);

        singleChatSection = findViewById(R.id.chat_single_section);
        tvGroupMemberCount = findViewById(R.id.tvGroupMemberCount);
        tvGroupDesc = findViewById(R.id.tvGroupDesc);

        db = FirebaseFirestore.getInstance();
        mStorageReference = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivChatAvatar:
            case R.id.btnChangeAvatar:
                changeGroupPhoto();
                break;
            case R.id.tvChatName:
            case R.id.ibnEditGroupName:
                doEditGroupName();
                break;
            case R.id.btnGroupMember:
                doShowGroupMembers();
                break;
            case R.id.btnLeaveGroup:
                doLeaveGroup();
                break;

        }
    }

    private void doLeaveGroup() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle("Bạn có chắc chắn muốn rời nhóm?")
                .setPositiveButton(Html.fromHtml("<span style=\"color: #ff0000;\">RỜI NH&Oacute;M</span>"), (dialogInterface, i) -> {

                    conservation.getMembers().remove(ComponentsUtil.getCurrentUserRef(db));
                    db.collection(CollectionConst.COLLECTION_CONSERVATION)
                            .document(conservation.getId())
                            .update("members", conservation.getMembers())
                            .addOnSuccessListener(unused -> {
                                Intent intent = new Intent(ChatOptionsActivity.this, MainScreenActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            })
                            .addOnFailureListener(e -> {
                                Toast.makeText(this, "Tác vụ rời nhóm xảy ra sự cố. Chúng tôi sẽ xem xét lại sau!", Toast.LENGTH_SHORT).show();
                                Log.d("leaveGroup", "onError::" + e.getMessage());
                            });
                })
                .setNegativeButton("Huỷ", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .create()
                .show();
    }

    private void doShowGroupMembers() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_show_member_group, null);
        RecyclerView rvGroupMembers = view.findViewById(R.id.rvGroupMembers);
        LinearLayout btnAddGroupMembers = view.findViewById(R.id.btnAddGroupMembers);

        GroupMemberAdapter adapter = new GroupMemberAdapter(conservation.getMembers());
        rvGroupMembers.setAdapter(adapter);

        builder.setView(view)
                .create()
                .show();

        btnAddGroupMembers.setOnClickListener(view1 -> {

        });

    }

    private void doEditGroupName() {
        View editGroupNameDialogView = getLayoutInflater().inflate(R.layout.bsd_edit_group_name, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(editGroupNameDialogView);
        dialog.show();

        ImageButton ibnClose = editGroupNameDialogView.findViewById(R.id.edit_gr_name_close);
        Button btnCancel = editGroupNameDialogView.findViewById(R.id.edit_gr_name_cancel);
        Button btnSave = editGroupNameDialogView.findViewById(R.id.edit_gr_name_save);
        TextInputLayout tipEditGroupName = editGroupNameDialogView.findViewById(R.id.tipEditGroupName);
        tipEditGroupName.getEditText().setText(conservation.getTitle());

        ibnClose.setOnClickListener(view -> {
            dialog.dismiss();
        });

        btnCancel.setOnClickListener(view -> {
            dialog.dismiss();
        });

        btnSave.setOnClickListener(view -> {
            String groupName = StringUtil.getInputText(tipEditGroupName);
            if (groupName.isEmpty()) {
                dialog.dismiss();
            } else {
                db.collection(CollectionConst.COLLECTION_CONSERVATION)
                        .document(conservation.getId())
                        .update("title", groupName)
                        .addOnSuccessListener(unused -> {
                            conservation.setTitle(groupName);
                            tvChatName.setText(groupName);
                            dialog.dismiss();
                        })
                        .addOnFailureListener(e -> {
                            Toast.makeText(this, "Đổi tên nhóm xảy ra sự cố. Chúng tôi sẽ kiểm tra lại sau!", Toast.LENGTH_SHORT).show();
                            Log.d("edit_gr_name", "onFailure::" + e.getMessage());
                        });
            }
        });
    }

    private void changeGroupPhoto() {
        View v = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_dialog_change_group_photo, null);
        changeGroupPhotoDialog = new BottomSheetDialog(this);
        changeGroupPhotoDialog.setContentView(v);
        changeGroupPhotoDialog.show();

        final TableRow btnTakePicture = v.findViewById(R.id.change_grphoto_take_a_picture_selection);
        final TableRow btnChoosePicture = v.findViewById(R.id.change_grphoto_choose_existing_photo_selection);
        final ImageView ivCalendarGrPhoto = v.findViewById(R.id.calendar_group_photo);
        ivCalendarGrPhoto.setTag("calendar.jpg");
        final ImageView ivTaskGrPhoto = v.findViewById(R.id.task_group_photo);
        ivTaskGrPhoto.setTag("task.jpg");
        final ImageView ivSchoolGrPhoto = v.findViewById(R.id.school_group_photo);
        ivSchoolGrPhoto.setTag("school.jpg");
        final ImageView ivFamilyGrPhoto = v.findViewById(R.id.family_group_photo);
        ivFamilyGrPhoto.setTag("family.jpg");
        final TextView tvDone = v.findViewById(R.id.btn_update_group_photo_done);
        tvDone.setVisibility(View.VISIBLE);

        View.OnClickListener onClickListener = view1 -> {
            Bitmap bitmap = ((RoundedDrawable) ((ImageView) view1).getDrawable()).getSourceBitmap();
            ivChatAvatar.setImageBitmap(bitmap);
            ivChatAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ivChatAvatar.setMinimumHeight(100);
            ivChatAvatar.setMinimumWidth(50);
            ivChatAvatar.setPadding(0, 0, 0, 0);
            mStorageReference.child(StorageRefConst.FOLDER_DEFAULT)
                    .child((String) view1.getTag())
                    .getDownloadUrl()
                    .addOnSuccessListener(uri -> {
                        selectedPhotoUrl = uri.toString();
                        Log.d(TAG, "photoUrl " + uri.toString());
                    })
                    .addOnFailureListener(e -> Log.d(TAG, "default_group_photo::onFailure: " + e.getMessage()));
        };

        ivCalendarGrPhoto.setOnClickListener(onClickListener);
        ivTaskGrPhoto.setOnClickListener(onClickListener);
        ivSchoolGrPhoto.setOnClickListener(onClickListener);
        ivFamilyGrPhoto.setOnClickListener(onClickListener);

        btnTakePicture.setOnClickListener(view1 -> {
            TedPermission.with(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            takeAPictureFromCamera();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Toast.makeText(getApplicationContext(), "Cấp phép quyền truy cập để tiếp tục.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.CAMERA)
                    .check();
        });

        btnChoosePicture.setOnClickListener(view1 -> {
            TedPermission.with(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            pickImageFromGallery();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Toast.makeText(getApplicationContext(), "Cấp phép quyền truy cập để tiếp tục.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.CAMERA)
                    .check();
        });

        tvDone.setOnClickListener(view -> {
            if (selectedPhotoUrl != null) {
                if (!selectedPhotoUrl.contains(getString(R.string.google_storage_bucket))) {
                    updateGroupPhoto();
                } else {
                    doChangeGroupPhoto();
                }
            }
        });
    }

    private void updateGroupPhoto() {
        Uri groupPhotoUri = Uri.parse(selectedPhotoUrl);
        StorageReference avatarRef = mStorageReference.child(StorageRefConst.FOLDER_GROUP_PHOTO)
                .child(FileUtils.getFileName(this, groupPhotoUri));

        avatarRef.putFile(groupPhotoUri)
                .addOnSuccessListener(taskSnapshot -> {
                    Task<Uri> task = Objects.requireNonNull(Objects.requireNonNull(taskSnapshot.getMetadata()).getReference()).getDownloadUrl();
                    task
                            .addOnSuccessListener(uri -> {
                                selectedPhotoUrl = uri.toString();
                                doChangeGroupPhoto();
                            })
                            .addOnFailureListener(e -> {
                                Toast.makeText(this, "Đổi hình nhóm xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "get_group_photo_url::onFailure: " + e.getMessage());
                            });
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(ChatOptionsActivity.this, "Đổi hình nhóm xảy ra sự cố. Thử lại!", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "change_group_photo::onFailure: " + e.getMessage());
                });
    }

    private void doChangeGroupPhoto() {
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .document(conservation.getId())
                .update("groupPhotoUrl", selectedPhotoUrl)
                .addOnSuccessListener(unused -> {
                    conservation.setGroupPhotoUrl(selectedPhotoUrl);
                    changeGroupPhotoDialog.dismiss();
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(this, "Đổi hình nhóm xảy ra sự cố. Chúng tôi sẽ kiểm tra lại sau!", Toast.LENGTH_SHORT).show();
                    Log.d("edit_gr_photo", "onFailure::" + e.getMessage());
                });
    }


    private void takeAPictureFromCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    private void pickImageFromGallery() {
        TedImagePicker.with(this)
                .title("Chọn ảnh")
                .buttonText("Xong")
                .buttonBackground(R.color.blue_magenta)
                .buttonTextColor(android.R.color.white)
                .start(new OnSelectedListener() {
                    @Override
                    public void onSelected(@NotNull Uri uri) {
                        selectedPhotoUrl = uri.toString();
                        ComponentsUtil.displayImage(ivChatAvatar, selectedPhotoUrl);
                        ivChatAvatar.setPadding(0, 0, 0, 0);
                        ivChatAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            selectedPhotoUrl = ComponentsUtil.getImageUri(ChatOptionsActivity.this, (Bitmap) data.getExtras().get("data")).toString();
            ComponentsUtil.displayImage(ivChatAvatar, selectedPhotoUrl);
            ivChatAvatar.setPadding(0, 0, 0, 0);
            ivChatAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
    }
}