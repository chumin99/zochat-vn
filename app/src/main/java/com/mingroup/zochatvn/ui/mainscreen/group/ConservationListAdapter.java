package com.mingroup.zochatvn.ui.mainscreen.group;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.data.model.MessageType;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.response.MessageResponse;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.data.model.ConservationType;
import com.mingroup.zochatvn.util.ComponentsUtil;

import lombok.Getter;

public class ConservationListAdapter extends RecyclerView.Adapter<ConservationListAdapter.ConservationViewHolder> {
    private static final String TAG = ConservationListAdapter.class.getSimpleName();
    private Context context;
    private List<Conservation> conservationList;
    private OnConservationItemListener onConservationItemListener;
    private AsyncListDiffer<Conservation> mDiffer;
    private String currentUserUid;
    private PrettyTime prettyTime;
    private FirebaseFirestore db;
    private DiffUtil.ItemCallback<Conservation> mCallback = new DiffUtil.ItemCallback<Conservation>() {
        @Override
        public boolean areItemsTheSame(@NonNull Conservation oldItem, @NonNull Conservation newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Conservation oldItem, @NonNull Conservation newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) && oldItem.getCreatedAt().equals(newItem.getCreatedAt());
        }
    };

    public ConservationListAdapter(Context context, List<Conservation> conservationList, OnConservationItemListener onConservationItemListener) {
        this.context = context;
        this.conservationList = conservationList;
        this.onConservationItemListener = onConservationItemListener;
        this.mDiffer = new AsyncListDiffer<>(this, mCallback);
        this.currentUserUid = ComponentsUtil.currentUserUid;
        this.prettyTime = new PrettyTime(ComponentsUtil.getCurrentLocale(context));
        this.db = FirebaseFirestore.getInstance();
    }

    @NonNull
    @Override
    public ConservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ConservationViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_group_conservation, parent, false), onConservationItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ConservationViewHolder holder, int position) {
        Conservation item = getItem(position);
        if (item.getType() == ConservationType.GROUP) {
            setTitleAndAvatarByType(holder, item.getTitle(), item.getGroupPhotoUrl(), false);
        } else {
            DocumentReference userFriendRef = ComponentsUtil.getFriendUser(item.getMembers());
            Objects.requireNonNull(userFriendRef).get()
                    .addOnSuccessListener(documentSnapshot -> {
                        User userFriend = documentSnapshot.toObject(User.class);
                        setTitleAndAvatarByType(holder, userFriend.getDisplayName(), userFriend.getPhotoUrl(), true);
                    })
                    .addOnFailureListener(e -> {
                        Log.d(TAG, "getUserFriend::onError: " + e.getMessage());
                    });
        }

        MessageResponse lastMessage = item.getLastMessage();
        if (lastMessage != null) {
            holder.getTvLastMessageTime().setText(prettyTime.format(lastMessage.getCreatedAt()));
            switch (lastMessage.getType()) {
                case TEXT:
                    holder.getTvLastMessage().setText(lastMessage.getContent());
                    break;
                case IMAGE:
                    holder.getTvLastMessage().setText("[Hình Ảnh]");
                    break;
                case VIDEO:
                    holder.getTvLastMessage().setText("[Video]");
                    break;
                case FILE:
                    holder.getTvLastMessage().setText("[File]");
                    break;
                case STICKER:
                    holder.getTvLastMessage().setText("[Sticker]");
                    break;
            }
        } else {
            holder.getTvLastMessageTime().setVisibility(View.GONE);
            holder.getTvLastMessage().setText("Nhấp để trò chuyện");
        }
    }

    private void setTitleAndAvatarByType(ConservationViewHolder holder, String title, String avatarUrl, boolean isUser) {
        holder.getTvConservationName().setText(title);
        if (isUser) {
            ComponentsUtil.displayImageFromApi(holder.getIvConservationPhoto(), avatarUrl);
        } else {
            if (avatarUrl != null && !avatarUrl.isEmpty()) {
                ComponentsUtil.displayImageFromApi(holder.getIvConservationPhoto(), avatarUrl);
            } else {
                ComponentsUtil.displayImage(holder.getIvConservationPhoto(), ComponentsUtil.convertFromResource(context.getResources(), R.drawable.avatar).toString());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public void update(List<Conservation> newConservationList) {
        mDiffer.submitList(newConservationList);
    }

    public Conservation getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void removeItem(int position) {
        conservationList.remove(position);
        update(conservationList);
        notifyItemRemoved(position);
    }

    @Getter
    public class ConservationViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView ivConservationPhoto;
        private TextView tvConservationName;
        private TextView tvLastMessage;
        private TextView tvLastMessageTime;

        OnConservationItemListener onConservationItemListener;

        public ConservationViewHolder(@NonNull View itemView, OnConservationItemListener onConservationItemListener) {
            super(itemView);
            ivConservationPhoto = itemView.findViewById(R.id.chat_conservation_photo);
            tvConservationName = itemView.findViewById(R.id.chat_conservation_name);
            tvLastMessage = itemView.findViewById(R.id.chat_conservation_last_message);
            tvLastMessageTime = itemView.findViewById(R.id.chat_conservation_last_status);

            this.onConservationItemListener = onConservationItemListener;
            itemView.setOnClickListener(view -> {
                if (onConservationItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onConservationItemListener.onConservationItemClick(position);
                    }
                }
            });

            itemView.setOnLongClickListener(view -> {
                if (onConservationItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onConservationItemListener.onConservationItemLongClick(position);
                        return true;
                    }
                }
                return false;
            });
        }
    }

    public interface OnConservationItemListener {
        void onConservationItemClick(int position);

        void onConservationItemLongClick(int position);
    }
}
