package com.mingroup.zochatvn.ui.forgetpassword;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.ui.mainscreen.MainScreenActivity;
import com.mingroup.zochatvn.util.StringUtil;

import static com.mingroup.zochatvn.util.StringUtil.getInputText;
import static com.mingroup.zochatvn.util.ValidatorUtil.isConfirmPasswordMatch;
import static com.mingroup.zochatvn.util.ValidatorUtil.isValidPasswordForChangePassword;

public class ForgetPasswordStepFourFragment extends Fragment {
    private TextInputLayout tipNewPassword, tipNewConfirmPassword;
    private Button btnUpdatePassword;
    private FirebaseUser currentUser;
    private RelativeLayout relativeLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forget_password_step_four, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ForgetPasswordActivity) getActivity()).getSupportActionBar().setTitle("Tạo mật khẩu mới");

        tipNewPassword = view.findViewById(R.id.tipNewPassword);
        tipNewConfirmPassword = view.findViewById(R.id.tipNewConfirmPassword);
        btnUpdatePassword = view.findViewById(R.id.btnUpdateResetPassword);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        relativeLayout = view.findViewById(R.id.forget_password_change_password_rl);

        validateForm();

        btnUpdatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doChangePassword();
            }
        });

    }

    private void validateForm() {
        tipNewPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipNewConfirmPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipNewConfirmPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isValidPasswordForChangePassword(editable.toString(), currentUser.getDisplayName())) {
                    tipNewPassword.setError("Mật khẩu phải từ 6-32 ký tự, gồm chữ kèm theo số hoặc ký tự đặc biệt");
                } else {
                    tipNewPassword.setError(null);
                }
            }
        });

        tipNewConfirmPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipNewPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipNewPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isConfirmPasswordMatch(StringUtil.getInputText(tipNewPassword), editable.toString())) {
                    tipNewConfirmPassword.setError("Mật khẩu không trùng khớp");
                } else {
                    tipNewConfirmPassword.setError(null);
                }
            }
        });
    }

    private void validateFormNotEmpty(String str1, String str2) {
        if (!str1.isEmpty() && !str2.isEmpty()) {
            btnUpdatePassword.setClickable(true);
            btnUpdatePassword.setAlpha(1f);
        } else {
            btnUpdatePassword.setClickable(false);
            btnUpdatePassword.setAlpha(.2f);
        }
    }

    private void doChangePassword() {
        currentUser.updatePassword(getInputText(tipNewPassword))
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            showNotificationDialog();
                        } else {
                            Snackbar.make(relativeLayout, "Cập nhật mật khẩu thất bại. Hãy thử lại", BaseTransientBottomBar.LENGTH_INDEFINITE)
                                    .setAction("Thử lại", null).show();
                        }
                    }
                });
    }

    private void showNotificationDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getActivity());
        builder.setTitle("Cập nhật thành công")
                .setMessage("Mật khấu của bạn đã được cập nhật. Vui lòng sử dụng mật khẩu này vào lần đăng nhập tiếp theo.")
                .setPositiveButton("Hoàn tất", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getActivity().getApplicationContext(), MainScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }).create().show();
    }
}
