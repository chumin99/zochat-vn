package com.mingroup.zochatvn.ui.mainscreen.group.addgroup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.DocumentReference;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;

import lombok.Getter;

public class ResultListAdapter extends RecyclerView.Adapter<ResultListAdapter.UserResultViewHolder> implements Filterable {
    private Context context;
    private List<DocumentReference> userList;
    private List<DocumentReference> userFilteredList;
    private OnResultListListener onResultListListener;

    public ResultListAdapter(Context context, List<DocumentReference> userList, OnResultListListener onResultListListener) {
        this.context = context;
        this.userList = userList;
        this.userFilteredList = userList;
        this.onResultListListener = onResultListListener;
    }

    @NonNull
    @Override
    public UserResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserResultViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_result_user_list, parent, false), onResultListListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UserResultViewHolder holder, int position) {
        userList.get(position).get()
                .addOnSuccessListener(documentSnapshot -> {
                    User item = documentSnapshot.toObject(User.class);
                    if (item != null) {
                        ComponentsUtil.displayImageFromApi(holder.getIvAvatar(), item.getPhotoUrl());
                        holder.getTvDisplayName().setText(item.getDisplayName());
                    }

                })
                .addOnFailureListener(e -> {
                    Log.d(ResultListAdapter.class.getSimpleName(), "::onFailure: " + e.getMessage());
                });

    }

    @Override
    public int getItemCount() {
        return userList == null ? 0 : userList.size();
    }

    public DocumentReference getItem(int position) {
        return userList != null ? userList.get(position) : null;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if (charSequence == null || charSequence.length() == 0) {
                    filterResults.count = userFilteredList.size();
                    filterResults.values = userFilteredList;
                } else {
                    String strQuery = charSequence.toString().toLowerCase();
                    List<DocumentReference> resultData = new ArrayList<>();
                    if (resultData.isEmpty()) {
                        for (DocumentReference item : userFilteredList) {
                            item.get()
                                    .addOnSuccessListener(documentSnapshot -> {
                                        User user = documentSnapshot.toObject(User.class);
                                        if (user != null && (user.getDisplayName().toLowerCase().contains(strQuery))) {
                                            resultData.add(item);
                                        }
                                    });
                            filterResults.count = resultData.size();
                            filterResults.values = resultData;
                        }
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                userList = (List<DocumentReference>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Getter
    public class UserResultViewHolder extends RecyclerView.ViewHolder {
        public RoundedImageView ivAvatar;
        public TextView tvDisplayName;
        public CheckBox cbSelect;
        private OnResultListListener onResultListListener;

        public UserResultViewHolder(@NonNull View itemView, OnResultListListener onResultListListener) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.ivAvatar2);
            tvDisplayName = itemView.findViewById(R.id.result_user_display_name);
            cbSelect = itemView.findViewById(R.id.cbSelect2);

            this.onResultListListener = onResultListListener;

            itemView.setOnClickListener(view -> {
                if (onResultListListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        cbSelect.setChecked(!cbSelect.isChecked());
                    }
                }
            });

            cbSelect.setOnCheckedChangeListener((compoundButton, b) -> {
                if (onResultListListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onResultListListener.onCheckboxCheckedChange(position, compoundButton, b);
                    }
                }
            });
        }
    }

    public interface OnResultListListener {
        void onUserItemClick(int position);

        void onCheckboxCheckedChange(int position, CompoundButton compoundButton, boolean isChecked);
    }
}
