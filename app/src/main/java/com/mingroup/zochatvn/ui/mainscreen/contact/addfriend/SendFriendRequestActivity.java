package com.mingroup.zochatvn.ui.mainscreen.contact.addfriend;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;

import org.parceler.Parcels;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.StringUtil;

import java.text.ParseException;

import lombok.SneakyThrows;

public class SendFriendRequestActivity extends AppCompatActivity {
    private Toolbar zcToolbar;
    private RoundedImageView ivAvatar;
    private TextView tvDisplayName;
    private TextInputLayout tipMessage;
    private Button btnSendRequest;
    private User user;
    private FirebaseFirestore db;
    private User currentUser;

    @SneakyThrows
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_friend_request);
        init();
        setupActionBar();
        displayUserInfo();
    }

    private void setupActionBar() {
        setSupportActionBar(zcToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_friend));
    }

    private void displayUserInfo() {
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            user = Parcels.unwrap(intent.getParcelableExtra("user"));
        }
        ComponentsUtil.displayImageFromApi(ivAvatar, user.getPhotoUrl());
        tvDisplayName.setText(user.getDisplayName());
        tipMessage.getEditText().setText("Xin chào, mình là " + currentUser.getDisplayName() + ". Kết bạn với mình nhé!");
    }

    private void init() throws ParseException {
        zcToolbar = findViewById(R.id.zochat_toolbar_no_search_view);
        ivAvatar = findViewById(R.id.send_request_user_ava);
        tvDisplayName = findViewById(R.id.send_request_user_displayName);
        tipMessage = findViewById(R.id.tipSendRequestMessage);
        btnSendRequest = findViewById(R.id.btnSendRequest);
        currentUser = new SessionManager(this).getCurrentUserInfo();
        db = FirebaseFirestore.getInstance();
    }

    public void sendFriendRequest(View view) {
        String message = StringUtil.getInputText(tipMessage);
        FriendRequest friendRequest = FriendRequest.createNewFriendRequest(db, user, message);

        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .add(friendRequest)
                .addOnSuccessListener(documentReference -> {
                    if (user.getEmail().contains(StringUtil.EMAIL_ZOCHAT_SUFFIX))
                        showAddToDeviceContactDialog();
                    else
                        backToPreviousScreen();
                })
                .addOnFailureListener(e -> {
                    Log.d(SendFriendRequestActivity.class.getSimpleName(), e.getMessage());
                });
    }

    private void showAddToDeviceContactDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle("Thông báo")
                .setMessage("Thêm số điện thoại của người này vào danh bạ điện thoại?")
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addPhoneNumberToDeviceContact();
                    }
                })
                .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface1, int i1) {
                        dialogInterface1.dismiss();
                        backToPreviousScreen();
                    }
                })
                .create()
                .show();
    }

    private void backToPreviousScreen() {
        Intent intent = new Intent();
        intent.putExtra("user", Parcels.wrap(user));
        setResult(RESULT_OK, intent);
        finish();
    }

    private void addPhoneNumberToDeviceContact() {
        if (user.getPhoneNumber() != null) {
            Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
            intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.NAME, user.getDisplayName());
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, StringUtil.revertPhoneNumberWithoutCountryCode(user.getPhoneNumber()));
            intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        backToPreviousScreen();
    }
}