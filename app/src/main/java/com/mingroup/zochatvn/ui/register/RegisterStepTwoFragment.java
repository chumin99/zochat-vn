package com.mingroup.zochatvn.ui.register;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;


import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.AuthMethod;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.FragmentUtil;

import java.util.List;

import static com.mingroup.zochatvn.util.StringUtil.generateEmailFromPhone;
import static com.mingroup.zochatvn.util.StringUtil.generatePhoneWithCountryCode;
import static com.mingroup.zochatvn.util.StringUtil.getInputText;
import static com.mingroup.zochatvn.util.ValidatorUtil.isConfirmPasswordMatch;
import static com.mingroup.zochatvn.util.ValidatorUtil.isValidEmail;
import static com.mingroup.zochatvn.util.ValidatorUtil.isVietnamesePhone;

public class RegisterStepTwoFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private TextInputLayout tipUsername, tipPassword, tipPasswordConfirm;
    private FloatingActionButton fabStepTwoNext;
    private UserFormViewModel userFormViewModel;
    private String username, password;
    private Spinner spnRegisterMethod;
    private String authType;
    private TextView tvDisplayError;
    private FirebaseAuth zcAuth;
    private FirebaseFirestore db;
    private RelativeLayout relativeLayout;
    private boolean isValid = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_step_two, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

        tipUsername.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateEmptyForm(charSequence.toString(),
                        getInputText(tipPassword),
                        getInputText(tipPasswordConfirm));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateEmptyForm(charSequence.toString(),
                        getInputText(tipPassword),
                        getInputText(tipPasswordConfirm));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tipPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateEmptyForm(charSequence.toString(),
                        getInputText(tipUsername),
                        getInputText(tipPasswordConfirm));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateEmptyForm(charSequence.toString(),
                        getInputText(tipUsername),
                        getInputText(tipPasswordConfirm));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tipPasswordConfirm.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateEmptyForm(charSequence.toString(),
                        getInputText(tipUsername),
                        getInputText(tipPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateEmptyForm(charSequence.toString(),
                        getInputText(tipUsername),
                        getInputText(tipPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        fabStepTwoNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateForm();
            }
        });

        userFormViewModel.getUsername().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                tipUsername.getEditText().setText(s);
            }
        });
    }

    private void validateForm() {
        tvDisplayError.setText(null);
        tipPasswordConfirm.setError(null);
        tipUsername.setBoxStrokeColor(getResources().getColor(R.color.colorPrimary, getActivity().getTheme()));

        username = getInputText(tipUsername);
        password = getInputText(tipPassword);

        if (isConfirmPasswordMatch(password, getInputText(tipPasswordConfirm))) {
            if (authType.equals(AuthMethod.PHONE)) {
                if (isVietnamesePhone(username)) {
                    username = generateEmailFromPhone(username);
                }
                else {
                    tvDisplayError.setText(getString(R.string.register_invalid_phone));
                    tipUsername.setBoxStrokeColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
                }
            } else if (authType.equals(AuthMethod.EMAIL)) {
                if (!isValidEmail(username)) {
                    tvDisplayError.setText(getString(R.string.register_invalid_email));
                    tipUsername.setBoxStrokeColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
                }
            }
        } else {
            tipPasswordConfirm.setError(getString(R.string.register_password_confirm_not_match));
        }

        checkExistsAccount(username);
    }

    private void checkExistsAccount(String emailOrPhone) {
        db.collection(CollectionConst.COLLECTION_USER)
                .whereEqualTo(authType, emailOrPhone)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<User> resultList = queryDocumentSnapshots.toObjects(User.class);
                    if (!resultList.isEmpty()) {
                        tvDisplayError.setText("Tài khoản này đã tồn tại trong hệ thống");
                        tipUsername.setBoxStrokeColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
                    } else {
                        showSendOTPConfirmDialog();
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d("Find User", e.getMessage());
                    Snackbar.make(relativeLayout, "Dường như có lỗi xảy ra. Xin hãy thử lại!", BaseTransientBottomBar.LENGTH_SHORT);
                });
    }

    private void init(View view) {
        tipUsername = view.findViewById(R.id.tipPhoneNumber);
        fabStepTwoNext = view.findViewById(R.id.fabStepTwoNext);
        fabStepTwoNext.setAlpha(.2f);
        userFormViewModel = new ViewModelProvider(requireActivity()).get(UserFormViewModel.class);
        spnRegisterMethod = view.findViewById(R.id.spn_register_method);
        spnRegisterMethod.setOnItemSelectedListener(this);
        tvDisplayError = view.findViewById(R.id.tvDisplayError);
        tipPassword = view.findViewById(R.id.tipPassword);
        tipPasswordConfirm = view.findViewById(R.id.tipPasswordConfirm);
        zcAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        relativeLayout = view.findViewById(R.id.register_step_two_rl);
    }

    private void showSendOTPConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        if (authType.equals(AuthMethod.PHONE))
            username = generatePhoneWithCountryCode(username);
        String message = "<h4 style=\"text-align: center;\"><strong>" + username + "</strong></h4>" + "Chúng tôi sẽ gửi mã xác nhận đến số điện thoại hoặc email trên. Vui lòng xác nhận thông tin này là đúng.";
        builder.setTitle("Xác nhận")
                .setMessage(Html.fromHtml(message))
                .setPositiveButton("Xác nhận", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        createNewUser();
                    }
                })
                .setNegativeButton(Html.fromHtml("<span style=\"color: #000000;\">Thay đổi</span>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                        if (keyCode == KeyEvent.KEYCODE_BACK)
                            dialogInterface.dismiss();
                        return true;
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void createNewUser() {
        userFormViewModel.getUsername().setValue(getInputText(tipUsername));
        new SessionManager(getActivity()).storeDisplayName(userFormViewModel.getDisplayName().getValue());
        userFormViewModel.getAuthMethod().setValue(authType);
        navigateToVerifyStep();
    }

    private void navigateToVerifyStep() {
        Bundle userPhoneBundle = new Bundle();
        userPhoneBundle.putString("password", password);
        FragmentUtil.replaceFragment(getActivity(), R.id.register_fragment_container,
                new RegisterStepThreeFragment(), userPhoneBundle, RegisterStepThreeFragment.class.getSimpleName(), true);
    }

    private void validateEmptyForm(String str1, String str2, String str3) {
        if (!str1.isEmpty() && !str2.isEmpty() && !str3.isEmpty()) {
            fabStepTwoNext.setClickable(true);
            fabStepTwoNext.setAlpha(1f);
        } else {
            fabStepTwoNext.setClickable(false);
            fabStepTwoNext.setAlpha(.2f);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView.getSelectedItemPosition() == 0) {
            tipUsername.setPrefixText(getString(R.string.register_vn_phone));
            tipUsername.getEditText().setHint(getString(R.string.register_phone_number_hint));
            tipUsername.getEditText().setInputType(InputType.TYPE_CLASS_PHONE);
            authType = AuthMethod.PHONE;
        } else {
            tipUsername.setPrefixText(null);
            tipUsername.getEditText().setHint(getString(R.string.register_email_hint));
            tipUsername.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            authType = AuthMethod.EMAIL;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
