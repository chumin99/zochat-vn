package com.mingroup.zochatvn.ui.register;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.util.FragmentUtil;

public class RegisterStepOneFragment extends Fragment {
    private TextInputLayout tipDisplayname;
    private FloatingActionButton fabStepOneNext;
    private UserFormViewModel userFormViewModel;

    public RegisterStepOneFragment() {
        setArguments(new Bundle());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_step_one, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

        tipDisplayname.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputText(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputText(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkInputText(editable.toString());
            }
        });

        fabStepOneNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String displayName = tipDisplayname.getEditText().getText().toString();
                tipDisplayname.setError(null);
                if (fabStepOneNext.isClickable() && !TextUtils.isEmpty(displayName)) {
                    userFormViewModel.getDisplayName().setValue(displayName);
                    FragmentUtil.replaceFragment(getActivity(), R.id.register_fragment_container,
                            new RegisterStepTwoFragment(), null, RegisterStepTwoFragment.class.getSimpleName(), true);
                } else {
                    tipDisplayname.setError(getResources().getString(R.string.register_invalid_display_name));
                }
            }
        });

        userFormViewModel.getDisplayName().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                tipDisplayname.getEditText().setText(s);
            }
        });
    }

    private void init(View view) {
        tipDisplayname = view.findViewById(R.id.tipDisplayName);
        fabStepOneNext = view.findViewById(R.id.fabStepOneNext);
        fabStepOneNext.setAlpha(.2f);
        userFormViewModel = new ViewModelProvider(requireActivity()).get(UserFormViewModel.class);
    }

    private void checkInputText(String str) {
        if (!str.isEmpty()) {
            fabStepOneNext.setClickable(true);
            fabStepOneNext.setAlpha(1f);
        } else {
            fabStepOneNext.setClickable(false);
            fabStepOneNext.setAlpha(.2f);
        }
    }
}
