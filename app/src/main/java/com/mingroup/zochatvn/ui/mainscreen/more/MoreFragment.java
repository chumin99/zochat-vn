package com.mingroup.zochatvn.ui.mainscreen.more;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.User;

import lombok.SneakyThrows;

import static com.mingroup.zochatvn.ui.mainscreen.more.profile.ProfileFragment.EDIT_PROFILE_REQUEST_CODE;
import static com.mingroup.zochatvn.util.ComponentsUtil.displayImageFromApi;

public class MoreFragment extends Fragment {
    private ViewPager2 vpMore;
    private TabLayout tabLayout;
    private FirebaseAuth zcAuth;
    private FirebaseFirestore db;
    private TextView tvProfileDisplayName;
    private RoundedImageView ivAvatar;
    private FrameLayout frameLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mainscreen_more, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        displayCurrentUserInfo();
        setupTabPage();
    }

    private void setupTabPage() {
        vpMore.setAdapter(new MorePagerAdapter(getActivity()));
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(
                tabLayout, vpMore, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0:
                        tab.setText("Thông tin");
                        break;
                    case 1:
                        tab.setText("Cài đặt");
                        break;
                }
            }
        }
        );
        tabLayoutMediator.attach();
    }

    private void init(View view) {
        vpMore = view.findViewById(R.id.vpMore);
        tabLayout = view.findViewById(R.id.moreTabLayout);
        zcAuth = FirebaseAuth.getInstance();
        tvProfileDisplayName = view.findViewById(R.id.profile_displayName);
        ivAvatar = view.findViewById(R.id.profile_avatar);
        frameLayout = view.findViewById(R.id.more_container);
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SneakyThrows
    private void displayCurrentUserInfo() {
        User currentUser = new SessionManager(getActivity()).getCurrentUserInfo();
        tvProfileDisplayName.setText(currentUser.getDisplayName());
        displayImageFromApi(ivAvatar, currentUser.getPhotoUrl());
    }

    @Override
    public void onStart() {
        super.onStart();
        displayCurrentUserInfo();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_PROFILE_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            displayCurrentUserInfo();
        }
    }
}