package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.makeramen.roundedimageview.RoundedImageView;

import com.mingroup.zochatvn.R;
import lombok.Getter;

@Getter
public class ImageMessageViewHolder extends RecyclerView.ViewHolder {
    private RoundedImageView ivAvatar;
    private TextView tvDisplayName, tvTimestamp, tvBreakTimeStamp, tvUploadError;

    private RoundedImageView ivContent;
    private GridView gvImages;
    private LinearProgressIndicator progressBar;
    private FrameLayout singleImageSection;
    private View gridViewSection;

    private OnMessageClickListener onMessageClickListener;

    public ImageMessageViewHolder(@NonNull View itemView, OnMessageClickListener onMessageClickListener, boolean fromLeft) {
        super(itemView);
        this.onMessageClickListener = onMessageClickListener;

        tvTimestamp = itemView.findViewById(R.id.media_message_item_timestamp);
        ivContent = itemView.findViewById(R.id.media_message_item_imageview);
        tvBreakTimeStamp = itemView.findViewById(R.id.break_timestamp);
        gvImages = itemView.findViewById(R.id.item_image_messages_gv);
        progressBar = itemView.findViewById(R.id.media_image_progress);
        tvUploadError = itemView.findViewById(R.id.tv_error_message_text);
        gridViewSection = itemView.findViewById(R.id.media_message_item_gv);

        singleImageSection = itemView.findViewById(R.id.media_message_single_image);
        singleImageSection.setVisibility(View.VISIBLE);

        if (fromLeft) {
            ivAvatar = itemView.findViewById(R.id.media_message_item_avatar);
            tvDisplayName = itemView.findViewById(R.id.media_message_item_display_name);
        }

        ivContent.setOnLongClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemLongClickListener(position);
                    return true;
                }
            }
            return false;
        });

        itemView.setOnClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemClickListener(position);
                }
            }
        });

        itemView.setOnLongClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemLongClickListener(position);
                    return true;
                }
            }
            return false;
        });
    }
}
