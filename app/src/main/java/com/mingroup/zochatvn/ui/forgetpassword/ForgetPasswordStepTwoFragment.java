package com.mingroup.zochatvn.ui.forgetpassword;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.util.concurrent.TimeUnit;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.util.FragmentUtil;

import static com.mingroup.zochatvn.util.StringUtil.generatePhoneNumber;

public class ForgetPasswordStepTwoFragment extends Fragment {
    private static final String FORGET_PASSWORD_PHONE_NUM = "forgetPhoneNumber";
    private OtpView inputOtp;
    private FloatingActionButton fabNext;
    private RelativeLayout relativeLayout;
    private String phoneNumber, strVerificationId;
    private FirebaseAuth zcAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks phoneCallbacks
            = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                inputOtp.setText(code);
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Snackbar.make(relativeLayout, "Xác thực OTP xảy ra lỗi", BaseTransientBottomBar.LENGTH_SHORT).show();
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            strVerificationId = s;
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forget_password_step_two, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        sendVerificationCode(generatePhoneNumber(phoneNumber));
        inputOtp.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                verifyVerificationCode(otp);
            }
        });
    }

    private void init(View view) {
        inputOtp = view.findViewById(R.id.input_forgot_pwd_input);
        fabNext = view.findViewById(R.id.fabForgetStepTwoNext);
        relativeLayout = view.findViewById(R.id.forget_password_step_two_rl);
        if (this.getArguments() != null) {
            phoneNumber = getArguments().getString(FORGET_PASSWORD_PHONE_NUM);
        }
        zcAuth = FirebaseAuth.getInstance();
        ((ForgetPasswordActivity) getActivity()).getSupportActionBar().setTitle("Nhập mã xác thực");
    }

    private void sendVerificationCode(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+84" + phoneNumber,
                60L,
                TimeUnit.SECONDS,
                getActivity(),
                phoneCallbacks
        );
    }

    private void verifyVerificationCode(String code) {
        try {
            String phoneNumber = "+84968958053";
            String smsCode = "190599";

            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseAuthSettings firebaseAuthSettings = firebaseAuth.getFirebaseAuthSettings();

            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode);

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(strVerificationId, code);
            signInWithPhoneAuthCredential(credential);
        } catch (Exception e) {
            Snackbar.make(relativeLayout, "Mã xác nhận không hợp lệ. Thử lại", BaseTransientBottomBar.LENGTH_SHORT).show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        zcAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FragmentUtil.replaceFragment(getActivity(), R.id.forgot_password_fragment_container, new ForgetPasswordStepThreeFragment(),
                                    null, ForgetPasswordStepTwoFragment.class.getSimpleName(), false);
                        } else {
                            String message = "Có lỗi xảy ra. Vui lòng thử lại sau!";
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                inputOtp.setError("Mã xác nhận không hợp lệ!");
                            }
                            Snackbar.make(relativeLayout, message, BaseTransientBottomBar.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}
