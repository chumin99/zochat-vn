package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.zochatvn.R;

import lombok.Getter;

@Getter
public class VideoMessageViewHolder extends RecyclerView.ViewHolder {
    private RoundedImageView ivAvatar;
    private TextView tvDisplayName, tvTimestamp, tvBreakTimeStamp;

    private PlayerView pvVideoContent;
    private LinearProgressIndicator progressBar;
    private View videoSection;

    private OnMessageClickListener onMessageClickListener;

    public VideoMessageViewHolder(@NonNull View itemView, OnMessageClickListener onMessageClickListener, boolean fromLeft) {
        super(itemView);
        this.onMessageClickListener = onMessageClickListener;
        tvBreakTimeStamp = itemView.findViewById(R.id.break_timestamp);
        tvTimestamp = itemView.findViewById(R.id.media_message_item_timestamp);
        pvVideoContent = itemView.findViewById(R.id.media_message_video);
        progressBar = itemView.findViewById(R.id.media_video_progress);
        videoSection = itemView.findViewById(R.id.partial_item_media_video);
        videoSection.setVisibility(View.VISIBLE);

        if (fromLeft) {
            ivAvatar = itemView.findViewById(R.id.media_message_item_avatar);
            tvDisplayName = itemView.findViewById(R.id.media_message_item_display_name);
        }

        itemView.setOnClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemClickListener(position);
                }
            }
        });

        itemView.setOnLongClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemLongClickListener(position);
                    return true;
                }
            }
            return false;
        });
    }
}
