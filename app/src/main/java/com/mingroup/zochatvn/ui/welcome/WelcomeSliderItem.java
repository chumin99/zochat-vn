package com.mingroup.zochatvn.ui.welcome;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WelcomeSliderItem {
    private int image;
    private String title;
    private String description;
}
