package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.chatoptions.groupmembers;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.mingroup.zochatvn.R;

public class GroupMembersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_members);
    }
}