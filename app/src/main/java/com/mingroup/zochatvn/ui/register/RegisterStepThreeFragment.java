package com.mingroup.zochatvn.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.constant.AuthMethod;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.request.UserProfileRequest;
import com.mingroup.zochatvn.ui.mainscreen.MainScreenActivity;
import com.mingroup.zochatvn.ui.welcome.WelcomeActivity;
import com.mingroup.zochatvn.util.StringUtil;

import static com.mingroup.zochatvn.util.StringUtil.generateEmailFromPhone;
import static com.mingroup.zochatvn.util.StringUtil.generatePhoneNumber;

public class RegisterStepThreeFragment extends Fragment {
    private static final String KEY_VERIFICATION_ID = "key_verification_id";
    private static final String TAG = RegisterStepThreeFragment.class.getSimpleName();
    private TextView tvOtpConfirm, tvPleaseFillOTP, tvNotify, tvPullToRefresh;
    private OtpView inputOtp;
    private FloatingActionButton fabStepThreeNext;
    private UserFormViewModel userFormViewModel;
    private FirebaseAuth zcAuth;
    private RelativeLayout rlStepThree;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ActionCodeSettings actionCodeSettings;
    private String strVerificationId, identifier, password;
    private boolean isVerifiedAfterLogin = false;
    private FirebaseFirestore db;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks phoneCallbacks
            = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null && code.length() == 6) {
                inputOtp.setText(code);
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Log.d(TAG, "phoneCallvback::onFailed: " + e.getMessage());
            if(e instanceof FirebaseAuthInvalidCredentialsException) {
                Snackbar.make(rlStepThree, "Xác thực OTP xảy ra lỗi", BaseTransientBottomBar.LENGTH_SHORT).show();
            } else if (e instanceof FirebaseTooManyRequestsException) {
                Snackbar.make(rlStepThree, "Dịch vụ xác thực SMS xảy ra lỗi. Chúng tôi sẽ kiểm tra lại sau!", BaseTransientBottomBar.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            strVerificationId = s;
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_step_three, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

        if (isVerifiedAfterLogin) {
            resendEmailVerification();
            String email = zcAuth.getCurrentUser().getEmail();
            if (!email.contains(StringUtil.EMAIL_ZOCHAT_SUFFIX)) {
                tvOtpConfirm.setText(getString(R.string.register_otp_verify_mail) + " \n " + email);
                tvPleaseFillOTP.setText(getString(R.string.register_please_fill));
                inputOtp.setVisibility(View.GONE);
                tvPullToRefresh.setVisibility(View.VISIBLE);
                fabStepThreeNext.setImageResource(R.drawable.ic_baseline_arrow_back_24);
                fabStepThreeNext.setClickable(true);
                fabStepThreeNext.setAlpha(1f);
                fabStepThreeNext.setOnClickListener(view1 -> {
                    zcAuth.signOut();
                    Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                });
            }
        } else {
            userFormViewModel.getUsername().observe(this.getViewLifecycleOwner(), new Observer<String>() {
                @Override
                public void onChanged(String s) {
                    if (userFormViewModel.getAuthMethod().getValue().equals(AuthMethod.PHONE)) {
                        tvOtpConfirm.setText(getString(R.string.register_otp_verify_1) + StringUtil.generatePhoneWithCountryCode(s));
                        tvPleaseFillOTP.setText(getString(R.string.register_otp_verify_2));
                        inputOtp.setVisibility(View.VISIBLE);
                    } else {
                        tvOtpConfirm.setText(getString(R.string.register_otp_verify_mail) + " \n " + s);
                        tvPleaseFillOTP.setText(getString(R.string.register_please_fill));
                        inputOtp.setVisibility(View.GONE);
                    }

                }
            });

            if (userFormViewModel.getAuthMethod().getValue().equals(AuthMethod.PHONE)) {
                sendVerificationCode(generatePhoneNumber(userFormViewModel.getUsername().getValue()));
                tvPullToRefresh.setVisibility(View.GONE);
                inputOtp.setOtpCompletionListener(otp -> {
                    if (otp.length() == 6)
                        verifyVerificationCode(otp);
                });
            } else {
                sendEmailVerification(userFormViewModel.getUsername().getValue(), password);
                tvPullToRefresh.setVisibility(View.VISIBLE);
            }
        }
        refreshForEmailVerification();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (strVerificationId == null && savedInstanceState != null) {
            onViewStateRestored(savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_VERIFICATION_ID, strVerificationId);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null)
            strVerificationId = savedInstanceState.getString(KEY_VERIFICATION_ID);
    }

    private void refreshForEmailVerification() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isVerifiedAfterLogin || userFormViewModel.getAuthMethod().getValue().equals(AuthMethod.EMAIL)) {
                    if (zcAuth.getCurrentUser() != null) {
                        zcAuth.getCurrentUser().reload();
                        if (zcAuth.getCurrentUser().isEmailVerified()) {
                            tvNotify.setVisibility(View.VISIBLE);
                            fabStepThreeNext.setClickable(true);
                            fabStepThreeNext.setAlpha(1f);
                            createUserProfile();
                            navigateToMainScreen();
                        } else {
                            tvNotify.setVisibility(View.GONE);
                            fabStepThreeNext.setClickable(false);
                            fabStepThreeNext.setAlpha(.2f);
                        }
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    private void navigateToMainScreen() {
        Intent intent = new Intent(getActivity(), MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void init(View view) {
        tvOtpConfirm = view.findViewById(R.id.tv_otp_confirm_phone);
        inputOtp = view.findViewById(R.id.input_otp);
        fabStepThreeNext = view.findViewById(R.id.fabStepThreeNext);
        tvPleaseFillOTP = view.findViewById(R.id.tv_please_fill_otp);
        tvNotify = view.findViewById(R.id.tv_notify);
        tvNotify.setVisibility(View.GONE);
        zcAuth = FirebaseAuth.getInstance();
        rlStepThree = view.findViewById(R.id.register_step_three_rl);
        fabStepThreeNext.setAlpha(.2f);
        fabStepThreeNext.setClickable(false);
        swipeRefreshLayout = view.findViewById(R.id.register_srf);
        tvPullToRefresh = view.findViewById(R.id.register_pull_to_refresh);
        userFormViewModel = new ViewModelProvider(requireActivity()).get(UserFormViewModel.class);
        identifier = userFormViewModel.getUsername().getValue();
        if (this.getArguments() != null) {
            password = getArguments().getString("password", "");
            isVerifiedAfterLogin = getArguments().getBoolean(MainScreenActivity.FLAG_RE_VERIFIED_MAIL);
        }
        db = FirebaseFirestore.getInstance();
    }

    private void verifyVerificationCode(String code) {
        String phoneNumber = "+84968958053";
        String smsCode = "190599";

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseAuthSettings firebaseAuthSettings = firebaseAuth.getFirebaseAuthSettings();

        firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode);

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(strVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void sendVerificationCode(String phoneNumber) {
        PhoneAuthOptions phoneAuthOptions = PhoneAuthOptions.newBuilder(zcAuth)
                .setPhoneNumber("+84" + phoneNumber)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(getActivity())
                .setCallbacks(phoneCallbacks)
                .build();

        PhoneAuthProvider.verifyPhoneNumber(phoneAuthOptions);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        zcAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(getActivity(), task -> {
                    if (task.isSuccessful()) {
                        linkCurrentUserWithEmailAuthProvider();
                    } else {
                        String message = "Có lỗi xảy ra. Vui lòng thử lại sau!";
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            inputOtp.setError("Mã xác nhận không hợp lệ!");
                        }
                        Snackbar.make(rlStepThree, message, BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                });
    }

    private void linkCurrentUserWithEmailAuthProvider() {
        String usernameFromPhone = generateEmailFromPhone(userFormViewModel.getUsername().getValue());
        AuthCredential credential = EmailAuthProvider.getCredential(usernameFromPhone, password);
        
        Objects.requireNonNull(zcAuth.getCurrentUser()).linkWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        createUserProfile();
                    } else {
                        Log.d("linkCurrentUser", task.getException().getMessage());
                    }
                });
    }


    private void createUserProfile() {
        String displayName = userFormViewModel.getDisplayName().getValue();
        FirebaseUser currentUser = zcAuth.getCurrentUser();
        if (isVerifiedAfterLogin) {
            displayName = new SessionManager(getActivity()).getStoredDisplayname();
        }
        User user = User.createUserProfile(Objects.requireNonNull(currentUser), displayName);

        if(currentUser.getPhoneNumber() != null) {
            user.setEmail(generateEmailFromPhone(currentUser.getPhoneNumber()));
        }

        db.collection(CollectionConst.COLLECTION_USER)
                .document(Objects.requireNonNull(zcAuth.getUid()))
                .set(user)
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.d(TAG, "createUserProfile::onSuccess: " + task.getException().getMessage());
                        Snackbar.make(rlStepThree, "Đăng ký xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT).show();
                    } else {
                        navigateToMainScreen();
                        Log.d(TAG, "createUserProfile::onSuccess: " + "Thành công");
                    }
                });
    }

    private void sendEmailVerification(String username, String password) {
        zcAuth.createUserWithEmailAndPassword(username, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        resendEmailVerification();
                    } else {
                        Log.d("CreateUserMail", task.getException().getMessage());
                    }
                });
    }

    private void resendEmailVerification() {
        zcAuth.getCurrentUser().sendEmailVerification()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.d("SendVerificationMail", task.getException().getMessage());
                    }
                });
    }
}
