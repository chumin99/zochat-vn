package com.mingroup.zochatvn.ui.mainscreen.contact.phonebookfriends;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.local.database.MyDatabase;
import com.mingroup.zochatvn.data.local.database.dao.ContactDao;
import com.mingroup.zochatvn.data.local.database.dao.PhonebookDao;
import com.mingroup.zochatvn.data.model.ContactInfo;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.response.PhonebookResponse;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.FriendResultDialogFragment;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

import org.parceler.Parcels;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;
import com.mingroup.zochatvn.data.model.response.PhonebookResponse;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.AddFriendActivity;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.SendFriendRequestActivity;
import com.mingroup.zochatvn.ui.mainscreen.contact.updatephonebook.UpdatePhonebookActivity;
import com.mingroup.zochatvn.util.StringUtil;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

import static com.mingroup.zochatvn.util.StringUtil.revertPhoneNumberFromContact;

public class PhonebookFriendsActivity extends AppCompatActivity implements PhonebookFriendAdapter.OnPhonebookFriendListener {
    private static final String TAG = PhonebookFriendsActivity.class.getSimpleName();
    private SearchView svPhoneBookFriend;
    private RecyclerView rvListPhoneBookFriend;
    private List<PhonebookResponse> phonebookFriendList;
    private PhonebookFriendAdapter phonebookFriendAdapter;
    private Button btnUpdatePhonebook;
    private Toolbar zcToolBar;
    private LinearLayout noResultSection, noDataSection, mContainer;
    private StringBuilder arrPhoneNumber;
    private ZCLoadingDialog loading;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FirebaseFirestore db;
    private ContactDao contactDao;
    private PhonebookDao phonebookDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phonebook_friends);
        init();
        setupActionBar();
        setupPhonebookList();
        btnUpdatePhonebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), UpdatePhonebookActivity.class));
            }
        });

        svPhoneBookFriend.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                phonebookFriendAdapter.getFilter().filter(query, i -> {
                    if (i == 0)
                        noResultSection.setVisibility(View.VISIBLE);
                    else
                        noResultSection.setVisibility(View.GONE);
                });
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                phonebookFriendAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {
        svPhoneBookFriend = findViewById(R.id.svPhoneBookFriend);
        rvListPhoneBookFriend = findViewById(R.id.rvListPhoneBookFriend);
        btnUpdatePhonebook = findViewById(R.id.btnUpdatePhonebook);
        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);
        noResultSection = findViewById(R.id.no_result_section);
        noDataSection = findViewById(R.id.no_phonebook_friend_section);
        mContainer = findViewById(R.id.activity_phonebook_friends_container);
        loading = ZCLoadingDialog.create(this).setStyle(ZCLoadingDialog.Style.MODERN_STYLE);
        db = FirebaseFirestore.getInstance();
        contactDao = new ContactDao(this);
        phonebookDao = MyDatabase.getDatabase(this).phonebookDao();
    }

    private void setupActionBar() {
        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.phonebook_friends));
    }

    private void setupPhonebookList() {
        loading.show();
        phonebookFriendList = new ArrayList<>();
        phonebookFriendAdapter = new PhonebookFriendAdapter(this, phonebookFriendList, this);
        rvListPhoneBookFriend.setAdapter(phonebookFriendAdapter);
        populateData();
    }

    private void populateData() {
        List<ContactInfo> contactList = phonebookDao.getAllContact();
        for (ContactInfo contactInfo : contactList) {

            db.collection(CollectionConst.COLLECTION_USER)
                    .whereEqualTo("phoneNumber", StringUtil.convertPhoneNumberFromContact(contactInfo.getContactPhoneNumber()))
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        DocumentReference userDocRef = queryDocumentSnapshots.getDocuments().get(0).getReference();

                        List<DocumentReference> users = new ArrayList<>(Arrays.asList(userDocRef, ComponentsUtil.getCurrentUserRef(db)));

                        Query getFriendRequestQuery = db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST).whereEqualTo("users", users);

                        getFriendRequestQuery
                                .get()
                                .addOnSuccessListener(queryDocumentSnapshots1 -> {
                                    if (!queryDocumentSnapshots1.isEmpty()) {
                                        addDataToList(queryDocumentSnapshots1, contactInfo);
                                    } else {
                                        Collections.reverse(users);
                                        getFriendRequestQuery
                                                .get()
                                                .addOnSuccessListener(queryDocumentSnapshots2 -> {
                                                    if(!queryDocumentSnapshots2.isEmpty()) {
                                                        addDataToList(queryDocumentSnapshots2, contactInfo);
                                                    } else {
                                                        PhonebookResponse phonebookResponse =
                                                            PhonebookResponse.createNoFriendPhonebook(contactInfo.getContactName(), userDocRef);
                                                        phonebookFriendList.add(phonebookResponse);
                                                    }
                                                })
                                        .addOnFailureListener(e -> {
                                            if (e instanceof TimeoutException) {
                                                Toast.makeText(this, "Vui lòng kiểm tra lại đường truyền Internet!", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }

                                    phonebookFriendAdapter.submitList(phonebookFriendList);
                                    checkNoDataSection();
                                })
                                .addOnFailureListener(e -> {
                                    if (e instanceof TimeoutException) {
                                        Toast.makeText(this, "Vui lòng kiểm tra lại đường truyền Internet!", Toast.LENGTH_SHORT).show();
                                    }
                                    Log.d(TAG, "::onFailure" + e.getMessage());
                                });

                    });
        }

        loading.dismiss();
    }

    private void addDataToList(QuerySnapshot queryDocumentSnapshots1, ContactInfo contactInfo) {
        PhonebookResponse phonebookResponse = null;
        phonebookResponse = queryDocumentSnapshots1.toObjects(PhonebookResponse.class).get(0);
        if (phonebookResponse != null) {
            phonebookResponse.setContactName(contactInfo.getContactName());
            phonebookResponse.setId(queryDocumentSnapshots1.getDocuments().get(0).getId());
            phonebookFriendList.add(phonebookResponse);
        }
    }

    private void checkNoDataSection() {
        if (phonebookFriendAdapter.getItemCount() > 0) {
            noDataSection.setVisibility(View.GONE);
        } else {
            noDataSection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPhonebookResponseClick(int position) {
        Toast.makeText(this, "Đã click item", Toast.LENGTH_SHORT).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onPhonebookFriendAddFriendClick(View view, int position) {
        PhonebookResponse item = phonebookFriendAdapter.getItem(position);

        switch (item.getStatus()) {
            case NOT_FRIEND:
                Intent intent = new Intent(this, SendFriendRequestActivity.class);
                item.getCreatedBy()
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            User userFriend = documentSnapshot.toObject(User.class);
                            intent.putExtra("user", Parcels.wrap(userFriend));
                            startActivityForResult(intent, AddFriendActivity.SEND_FRIEND_REQUEST_RESULT_CODE);
                        });
                break;
            case ACCEPTED:
                Toast.makeText(this, "Đã nhấn vào nhắn tin", Toast.LENGTH_SHORT).show();
                break;
            case PENDING:
                if (item.getCreatedBy().getId().equals(ComponentsUtil.currentUserUid)) {
                    db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                            .document(item.getId())
                            .delete()
                            .addOnSuccessListener(aVoid -> {
                                item.setStatus(FriendRequestStatus.NOT_FRIEND);
                                phonebookFriendAdapter.submitList(phonebookFriendList);
                                phonebookFriendAdapter.notifyItemChanged(position);
                            })
                            .addOnFailureListener(e -> {
                                Toast toast = Toast.makeText(PhonebookFriendsActivity.this, "Xoá lời mời kết bạn thất bại", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                Log.d("RejectFrRequest", e.getMessage());
                            });
                } else {
                    androidx.appcompat.widget.PopupMenu popupMenu = new androidx.appcompat.widget.PopupMenu(this, view, Gravity.TOP);
                    try {
                        Method method = popupMenu.getMenu().getClass().getDeclaredMethod("setOptionalIconsVisible", boolean.class);
                        method.setAccessible(true);
                        method.invoke(popupMenu.getMenu(), true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    popupMenu.getMenuInflater().inflate(R.menu.add_friend_menu, popupMenu.getMenu());
                    popupMenu.show();
                    popupMenu.setOnMenuItemClickListener(menuItem -> {
                        switch (menuItem.getItemId()) {
                            case R.id.menu_accept_friend_request:
                                db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                                        .document(item.getId())
                                        .update("status", FriendRequestStatus.ACCEPTED)
                                        .addOnSuccessListener(aVoid -> {
                                            item.setStatus(FriendRequestStatus.ACCEPTED);
                                            phonebookFriendAdapter.submitList(phonebookFriendList);
                                            phonebookFriendAdapter.notifyItemChanged(position);
                                        })
                                        .addOnFailureListener(e -> {
                                            Toast toast = Toast.makeText(PhonebookFriendsActivity.this, "Chấp nhận kết bạn thất bại", Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                            toast.show();
                                            Log.d("AcceptFrRequest", e.getMessage());
                                        });
                                return true;
                            case R.id.menu_remove_friend_request:
                                db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                                        .document(item.getId())
                                        .delete()
                                        .addOnSuccessListener(aVoid -> {
                                            item.setStatus(FriendRequestStatus.NOT_FRIEND);
                                            phonebookFriendAdapter.submitList(phonebookFriendList);
                                            phonebookFriendAdapter.notifyItemChanged(position);
                                        })
                                        .addOnFailureListener(e -> {
                                            Toast toast = Toast.makeText(PhonebookFriendsActivity.this, "Xoá lời mời kết bạn thất bại", Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER, 0, 0);
                                            toast.show();
                                            Log.d("RejectFrRequest", e.getMessage());
                                        });
                                return true;
                        }
                        return false;
                    });
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + item.getStatus());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AddFriendActivity.SEND_FRIEND_REQUEST_RESULT_CODE && resultCode == RESULT_OK && data != null) {
            setupPhonebookList();
        }
    }

    @Override
    public boolean onPhonebookResponseLongClick(int position) {
        String displayName = phonebookFriendList.get(position).getContactName();
        CharSequence[] actions = {"Xoá khỏi danh bạ máy"};
        new AlertDialog.Builder(this)
                .setTitle(Html.fromHtml("<h4>" + displayName + "</h4><br />", Html.FROM_HTML_MODE_COMPACT))
                .setItems(actions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        askDoUnfriend(position, displayName);
                    }
                })
                .create()
                .show();
        ;
        return true;
    }

    private void askDoUnfriend(int position, String displayName) {
        CharSequence[] choices = {"Huỷ kết bạn ZoChat"};
        AtomicBoolean isUnfriend = new AtomicBoolean(false);
        new AlertDialog.Builder(this)
                .setMessage("Bạn có muốn xoá " + displayName + " khỏi danh bạ điện thoại?")
                .setMultiChoiceItems(choices, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                        isUnfriend.set(b);
                    }
                })
                .setPositiveButton(Html.fromHtml("<span style=\"color: #ff0000;\">Xoá</span>", Html.FROM_HTML_MODE_COMPACT), (dialogInterface1, i1) -> {
                    TedPermission.with(PhonebookFriendsActivity.this)
                            .setPermissionListener(new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {
                                    ComponentsUtil.getFriendUser(phonebookFriendAdapter.getItem(position).getUsers())
                                            .get()
                                            .addOnSuccessListener(documentSnapshot -> {
                                                String phoneNumber = documentSnapshot.toObject(User.class).getPhoneNumber();
                                                if (contactDao.deleteContactFromDevice(revertPhoneNumberFromContact(phoneNumber))) {
                                                    if (isUnfriend.get())
                                                        doUnFriend(position);
                                                    phonebookFriendAdapter.removeItem(position);
                                                    checkNoDataSection();
                                                }
                                            });
                                }

                                @Override
                                public void onPermissionDenied(List<String> deniedPermissions) {
                                    Snackbar.make(mContainer, "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                                }
                            })
                            .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                            .setPermissions(Manifest.permission.WRITE_CONTACTS)
                            .check();
                })
                .setNegativeButton("Không", (dialogInterface1, i1) -> {
                    dialogInterface1.dismiss();
                })
                .create()
                .show();
    }

    private void doUnFriend(int position) {
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .document(phonebookFriendAdapter.getItem(position).getId())
                .delete()
                .addOnSuccessListener(unused -> {

                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "doUnFriend::onFailure" + e.getMessage());
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }
}