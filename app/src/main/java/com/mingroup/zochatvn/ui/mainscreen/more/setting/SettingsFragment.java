package com.mingroup.zochatvn.ui.mainscreen.more.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.ui.mainscreen.more.setting.changepassword.ChangePasswordFragment;
import com.mingroup.zochatvn.ui.welcome.WelcomeActivity;
import com.mingroup.zochatvn.util.FragmentUtil;

public class SettingsFragment extends PreferenceFragmentCompat {
    private FirebaseAuth zcAuth;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        switch (preference.getKey()) {
            case "settings_sign_out":
                return logoutCurrentUser();
            case "settings_delete_account":
                return deleteCurrentAccount();
            case "settings_change_password":
                return navigateToChangePassword();
        }
        return false;
    }

    private boolean navigateToChangePassword() {
        FragmentUtil.replaceFragment(getActivity(), R.id.more_container, new ChangePasswordFragment(), null, ChangePasswordFragment.class.getSimpleName(), true);
        return true;
    }

    private boolean deleteCurrentAccount() {
        return false;
    }

    private boolean logoutCurrentUser() {
        zcAuth = FirebaseAuth.getInstance();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Thông báo")
                .setMessage("Bạn có chắn chắn muốn thoát?")
                .setPositiveButton("Đăng xuất", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        zcAuth.signOut();
                        new SessionManager(getActivity()).logout();
                        Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
        return true;
    }
}