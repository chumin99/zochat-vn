package com.mingroup.zochatvn.ui.register;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.util.FragmentUtil;

public class RegisterActivity extends AppCompatActivity {
    private Toolbar zcToolBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);

        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.register));

        FragmentUtil.replaceFragment(this, R.id.register_fragment_container, new RegisterStepOneFragment(),
                null, RegisterStepOneFragment.class.getSimpleName(), false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
