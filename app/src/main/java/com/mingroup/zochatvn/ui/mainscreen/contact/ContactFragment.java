package com.mingroup.zochatvn.ui.mainscreen.contact;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.AddFriendActivity;
import com.mingroup.zochatvn.ui.mainscreen.contact.friendrequest.FriendRequestActivity;
import com.mingroup.zochatvn.ui.mainscreen.contact.phonebookfriends.PhonebookFriendsActivity;
import com.mingroup.zochatvn.ui.mainscreen.contact.updatephonebook.UpdatePhonebookActivity;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.ChatActivity;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

import java.util.ArrayList;
import java.util.List;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.User;

import org.parceler.Parcels;

public class ContactFragment extends Fragment implements ContactListAdapter.OnContactItemListener, View.OnClickListener {
    private static final String TAG = ContactFragment.class.getSimpleName();
    private SearchView searchView;
    private TextView tvQueryHint, btnRefreshContactList, tvContacListQuantity;
    private TableRow btnFriendRequest, btnPhonebookFriend;
    private Button btnUpdateContactList, btnAddFriend;
    private LinearLayout noDataSection;
    private RecyclerView rvContactList;
    private List<FriendRequest> contacts;
    private ContactListAdapter contactListAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ZCLoadingDialog zcLoadingDialog;
    private FirebaseFirestore db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        zcLoadingDialog = ZCLoadingDialog.create(getActivity()).setStyle(ZCLoadingDialog.Style.MODERN_STYLE);
        zcLoadingDialog.show();
        return inflater.inflate(R.layout.fragment_mainscreen_contact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            populateDataToContactList();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        setupContactList();
    }

    private void setupContactList() {
        contacts = new ArrayList<>();
        contactListAdapter = new ContactListAdapter(contacts, this, false);
        rvContactList.setAdapter(contactListAdapter);
        populateDataToContactList();
    }

    private void populateDataToContactList() {
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .whereArrayContains("users", ComponentsUtil.getCurrentUserRef(db))
                .whereEqualTo("status", FriendRequestStatus.ACCEPTED)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed.", error);
                        return;
                    }

                    if (value != null) {
                        value.getDocuments().forEach(documentSnapshot -> {
                            FriendRequest contact = documentSnapshot.toObject(FriendRequest.class);
                            if (contact != null) {
                                contact.setId(documentSnapshot.getId());
                                contacts.add(contact);
                            }
                        });
                        contactListAdapter.update(contacts);
                    }
                    updateDataSection();
                });
    }

    private void updateDataSection() {
        tvContacListQuantity.setText(contactListAdapter.getItemCount() + " bạn");
        if (contactListAdapter.getItemCount() <= 0) {
            noDataSection.setVisibility(View.VISIBLE);
        } else {
            noDataSection.setVisibility(View.GONE);
        }
        zcLoadingDialog.dismiss();
    }

    private void init(View view) {
        tvContacListQuantity = view.findViewById(R.id.tvContacListQuantity);
        noDataSection = view.findViewById(R.id.no_data_section);
        searchView = view.findViewById(R.id.top_bar_search);
        btnAddFriend = view.findViewById(R.id.btn_add_friend_contact);
        btnAddFriend.setOnClickListener(this);
        tvQueryHint = view.findViewById(R.id.tvQueryHint);
        rvContactList = view.findViewById(R.id.rvListContactPhone);
        btnFriendRequest = view.findViewById(R.id.btnFriendRequest);
        btnFriendRequest.setOnClickListener(this);
        btnPhonebookFriend = view.findViewById(R.id.btnPhonebookFriend);
        btnPhonebookFriend.setOnClickListener(this);
        btnUpdateContactList = view.findViewById(R.id.btnUpdateContactList);
        btnUpdateContactList.setOnClickListener(this);
        btnRefreshContactList = view.findViewById(R.id.btnRefreshContactList);
        btnRefreshContactList.setOnClickListener(this);
        swipeRefreshLayout = view.findViewById(R.id.contact_swipe_layout);
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onContactItemClick(int position) {

    }

    @Override
    public void onChatContactItemClick(int position) {
        ComponentsUtil.getFriendUser(contactListAdapter.getItem(position).getUsers())
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    User user = documentSnapshot.toObject(User.class);
                    user.setUid(documentSnapshot.getId());
                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                    intent.putExtra("chat_friend", Parcels.wrap(user));
                    startActivity(intent);
                })
                .addOnFailureListener(e -> Log.d(TAG, "chat_contact_click::onFailure: " + e.getMessage()));
    }

    @Override
    public void onRemoveContactItemClick(int position) {
        FriendRequest selectedFriendRequest = contactListAdapter.getItem(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ComponentsUtil.getFriendUser(selectedFriendRequest.getUsers())
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    User friendUser = documentSnapshot.toObject(User.class);
                    builder.setMessage("Bạn có muốn xoá bạn với " + friendUser.getDisplayName())
                            .setPositiveButton(Html.fromHtml("<span style=\"color: #ff0000;\">Xoá</span>"), (dialogInterface, i) -> {
                                db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                                        .document(selectedFriendRequest.getId())
                                        .delete()
                                        .addOnSuccessListener(aVoid -> {
                                            contactListAdapter.removeItem(position);
                                            contactListAdapter.notifyItemRemoved(position);
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    updateDataSection();
                                                }
                                            }, 1000);
                                        })
                                        .addOnFailureListener(e -> {
                                            dialogInterface.dismiss();
                                            Log.d("RemoveContactItemFailed", e.getMessage());
                                            Toast.makeText(getActivity(), "Có lỗi xảy ra. Chúng tôi sẽ kiểm tra lại!", Toast.LENGTH_SHORT).show();
                                        });
                            })
                            .setNegativeButton("Không", (dialogInterface, i) -> {
                                dialogInterface.dismiss();
                            })
                            .create()
                            .show();
                });
    }

    @Override
    public void onCheckboxCheckedChange(int position, CompoundButton compoundButton, boolean isChecked) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int idSelected = item.getItemId();
        if (idSelected == R.id.top_bar_action_add_friend) {
            Intent intent = new Intent(getActivity(), AddFriendActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnFriendRequest:
                startActivity(new Intent(getActivity(), FriendRequestActivity.class));
                break;
            case R.id.btnPhonebookFriend:
                startActivity(new Intent(getActivity(), PhonebookFriendsActivity.class));
                break;
            case R.id.btnRefreshContactList:
            case R.id.btnUpdateContactList:
                startActivity(new Intent(getActivity(), UpdatePhonebookActivity.class));
                break;
            case R.id.btn_add_friend_contact:
                startActivity(new Intent(getActivity(), AddFriendActivity.class));
                break;
        }
    }
}
