package com.mingroup.zochatvn.ui.mainscreen.group.addgroup;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.DocumentReference;

import java.util.ArrayList;
import java.util.List;

public class AddGroupViewModel extends ViewModel {
    private MutableLiveData<List<DocumentReference>> selectedUsers;
    private MutableLiveData<List<String>> selectedUsersUid;

    public MutableLiveData<List<DocumentReference>> getSelectedUsers() {
        if (selectedUsers == null) {
            selectedUsers = new MutableLiveData<>();
            selectedUsers.setValue(new ArrayList<DocumentReference>());
        }
        return selectedUsers;
    }

    public void setSelectedUsers(List<DocumentReference> selectedUserList) {
        selectedUsers.setValue(selectedUserList);
    }

    public void addUser(DocumentReference user) {
        List<DocumentReference> list = getSelectedUsers().getValue();
        if (list != null && !list.contains(user)) list.add(user);
        selectedUsers.setValue(list);
    }


    public void removeUser(DocumentReference user) {
        List<DocumentReference> list = getSelectedUsers().getValue();
        if (list != null && list.contains(user)) list.remove(user);
        selectedUsers.setValue(list);
    }
}
