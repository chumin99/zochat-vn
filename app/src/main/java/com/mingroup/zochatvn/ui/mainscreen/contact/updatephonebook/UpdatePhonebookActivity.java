package com.mingroup.zochatvn.ui.mainscreen.contact.updatephonebook;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.Date;
import java.util.List;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.local.database.dao.ContactDao;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

import static com.mingroup.zochatvn.util.StringUtil.formatDateTime;

public class UpdatePhonebookActivity extends AppCompatActivity {
    private static final int READ_CONTACTS_PERMISSION_CODE = 200;
    private Toolbar zcToolbar;
    private LinearLayout layout;
    private TextView tvLastUpdatePhonebook;
    private Button btnUpdate;
    private SessionManager sessionManager;
    private ContactDao contactDao;
    private ZCLoadingDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_phonebook);
        init();
        setupActionBar();
    }

    private void init() {
        zcToolbar = findViewById(R.id.zochat_toolbar_no_search_view);
        tvLastUpdatePhonebook = findViewById(R.id.tvLastUpdatePhonebook);
        btnUpdate = findViewById(R.id.btnUpdatePhonebookZochat);
        layout = findViewById(R.id.update_phonebook_layout);
        sessionManager = new SessionManager(this);
        contactDao = new ContactDao(this);
        loading = ZCLoadingDialog.create(this).setStyle(ZCLoadingDialog.Style.BASIC_STYLE);
        tvLastUpdatePhonebook.setText(sessionManager.getLastUpdatePhonebook() == null ? "Chưa có cập nhật" : sessionManager.getLastUpdatePhonebook());
    }

    private void setupActionBar() {
        setSupportActionBar(zcToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.update_phone_book_Zochat));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void doUpdatePhonebook(View view) {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        updateContactListToPhonebookFriend();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Snackbar.make(layout, "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS)
                .check();
    }

    private void displayUpdatePhonebookResult() {
        String lastUpdate = formatDateTime(new Date());
        sessionManager.storeLastUpdatePhonebook(lastUpdate);
        tvLastUpdatePhonebook.setText(sessionManager.getLastUpdatePhonebook());
    }

    private void updateContactListToPhonebookFriend() {
        loading.show();
        contactDao.updateContactListToPhonebookFriend();
        displayUpdatePhonebookResult();
        loading.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }
}