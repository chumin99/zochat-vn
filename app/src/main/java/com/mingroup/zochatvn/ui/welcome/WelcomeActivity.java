package com.mingroup.zochatvn.ui.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.ui.login.LoginActivity;
import com.mingroup.zochatvn.ui.register.RegisterActivity;

import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager2 vpInappWelcome;
    private WelcomeSliderAdapter welcomeSliderAdapter;
    private Handler welcomeSliderHandler = new Handler();
    private LinearLayout layoutIndicators;
    private Button btnGoToLogin, btnGoToRegister;

    private static List<WelcomeSliderItem> welcomeSliderItems = new ArrayList<>();
    static {
        welcomeSliderItems.add(
                new WelcomeSliderItem(R.drawable.vector_inapp_welcome_screen_01, "Gọi video ổn định"
                        , "Trò chuyện thật đã với chất lượng video ốn định mọi lúc, mọi nơi"));
        welcomeSliderItems.add(
                new WelcomeSliderItem(R.drawable.vector_inapp_welcome_screen_02, "Thoả sức kết nối với bạn bè toàn cầu"
                        , "Nơi cùng nhau trao đối, giữ liên lạc với gia đình, bạn bė, đồng nghiệp"));
        welcomeSliderItems.add(
                new WelcomeSliderItem(R.drawable.vector_inapp_welcome_screen_03, "Gửi đa phương tiện nhanh chóng"
                        , "Trao đổi đa phương tiện chất lượng cao với bạn bè và người thân thật nhanh và dễ dàng"));
        welcomeSliderItems.add(
                new WelcomeSliderItem(R.drawable.vector_inapp_welcome_screen_04, "Trải nghiệm stickers sống động"
                        , "Không dừng lại ở những tin nhắn văn bản, ZOchat cung cấp trải nghiệm tốt hơn bao giờ hết"));

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        init();
        setupInappWelcomeSlider();
    }

    private void init() {
        vpInappWelcome = findViewById(R.id.vpInappWelcome);
        layoutIndicators = findViewById(R.id.layoutIndicators);
        btnGoToLogin = findViewById(R.id.btnGoToLogin);
        btnGoToRegister = findViewById(R.id.btnGoToRegister);
        btnGoToLogin.setOnClickListener(this);
        btnGoToRegister.setOnClickListener(this);
    }

    private void setupInappWelcomeSlider() {
        welcomeSliderAdapter = new WelcomeSliderAdapter(vpInappWelcome, welcomeSliderItems);
        setupIndicators();

        vpInappWelcome.setAdapter(welcomeSliderAdapter);
        vpInappWelcome.setClipToPadding(false);
        vpInappWelcome.setClipChildren(false);
        vpInappWelcome.setOffscreenPageLimit(3);
        vpInappWelcome.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        vpInappWelcome.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                welcomeSliderHandler.removeCallbacks(welcomeSliderRunnable);
                welcomeSliderHandler.postDelayed(welcomeSliderRunnable, 3000);
                setCurrentIndicator(position);
            }
        });
    }

    private void setupIndicators() {
        ImageView[] indicators = new ImageView[welcomeSliderAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(8, 0, 8, 0);
        for (int i = 0; i < indicators.length; i++) {
            indicators[i] = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_dot_inactive));
            indicators[i].setLayoutParams(layoutParams);
            layoutIndicators.addView(indicators[i]);
        }

    }

    private void setCurrentIndicator(int index) {
        int childCount = layoutIndicators.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ImageView imageView = (ImageView) layoutIndicators.getChildAt(i);
            if (i == index) {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_dot_active));
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_dot_inactive));
            }
        }
    }

    private Runnable welcomeSliderRunnable = new Runnable() {
        @Override
        public void run() {
            int index = vpInappWelcome.getCurrentItem();
            if (index == (welcomeSliderItems.size() - 1))
                vpInappWelcome.setCurrentItem(0);
            else vpInappWelcome.setCurrentItem(vpInappWelcome.getCurrentItem() + 1);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGoToLogin:
                Intent intentLogin = new Intent(this, LoginActivity.class);
                startActivity(intentLogin);
                break;
            case R.id.btnGoToRegister:
                Intent intentRegister = new Intent(this, RegisterActivity.class);
                startActivity(intentRegister);
                break;
        }
    }

}
