package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.util.Collections;
import java.util.List;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.response.MessageResponse;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.StringUtil;
import com.mingroup.zochatvn.util.file.FileUtils;
import com.mingroup.zochatvn.util.file.ZCFileUploader;

import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;

public class ImageGvAdapter extends BaseAdapter {
    private static final String TAG = ImageGvAdapter.class.getSimpleName();
    private Context context;
    private MessageResponse messageItem;
    private FirebaseFirestore db;
    private String[] urlList;

    public ImageGvAdapter(Context context, MessageResponse messageItem, String[] urlList) {
        this.context = context;
        this.messageItem = messageItem;
        this.db = FirebaseFirestore.getInstance();
        this.urlList = urlList;
    }

    @Override
    public int getCount() {
        return urlList != null ? urlList.length : 0;
    }

    @Override
    public Object getItem(int i) {
        return urlList[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SneakyThrows
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageGvViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_gv_image, viewGroup, false);
            holder = new ImageGvViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ImageGvViewHolder) view.getTag();
        }

        String imageUrl = (String) getItem(i);

        if (imageUrl.contains(context.getString(R.string.google_storage_bucket))) { // uploaded
            ComponentsUtil.displayImageFromApi(holder.getImageView(), imageUrl);
        } else {
            ZCFileUploader fileUploader = new ZCFileUploader(context);
            List<File> fileList = Collections.singletonList(FileUtils.getFileFromUri(context, Uri.parse(imageUrl)));

            holder.imageView.setAlpha(.3f);
            holder.progressBar.setVisibility(View.VISIBLE);

            fileUploader.uploadFiles(CollectionConst.COLLECTION_CONSERVATION, Collections.singletonList(Uri.parse(imageUrl)), new ZCFileUploader.FileUploaderCallback() {
                @Override
                public void onError(Exception e) {
                    Log.d(TAG, "::onError: " + e.getMessage());
                }

                @Override
                public void onFinish(List<Uri> responses) {
                    holder.getImageView().setAlpha(1f);
                    holder.getProgressBar().setVisibility(View.GONE);

                    if(responses != null && !responses.isEmpty()) {
                        urlList[i] = responses.get(0).toString();

                        ComponentsUtil.displayImageFromApi(holder.getImageView(), urlList[i]);

                        if (i == (getCount() - 1)) {
                            db.collection(CollectionConst.COLLECTION_MESSAGE)
                                    .document(messageItem.getId())
                                    .update("content", StringUtil.convertListToString(urlList, ";"))
                                    .addOnSuccessListener(aVoid -> {
                                        Log.d(TAG, "updated thành công");
                                    })
                                    .addOnFailureListener(e -> {
                                        //holder.getTvDisplayError().setVisibility(View.VISIBLE);
                                        Log.d(TAG, e.getMessage());
                                    });
                        }
                    }
                }

                @Override
                public void onProgressUpdate(long currentpercent, long totalpercent, long filenumber) {
                    holder.progressBar.setProgress((int) currentpercent);
                }
            });
        }

        return view;
    }

    @Data
    private class ImageGvViewHolder {
        private RoundedImageView imageView;
        private LinearProgressIndicator progressBar;
        private TextView tvDisplayError;

        public ImageGvViewHolder(@NonNull View itemView) {
            imageView = itemView.findViewById(R.id.media_message_item_imageview);
            progressBar = itemView.findViewById(R.id.media_gv_image_progress);
            tvDisplayError = itemView.findViewById(R.id.tv_error_message_text);
        }
    }
}
