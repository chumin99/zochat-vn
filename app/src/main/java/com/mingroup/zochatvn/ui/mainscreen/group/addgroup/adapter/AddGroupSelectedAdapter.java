package com.mingroup.zochatvn.ui.mainscreen.group.addgroup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.util.Log;
import com.google.firebase.firestore.DocumentReference;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.DiffCallback;

import lombok.Getter;

public class AddGroupSelectedAdapter extends RecyclerView.Adapter<AddGroupSelectedAdapter.AddGroupSelectedViewHolder> {
    private static final String TAG = AddGroupSelectedAdapter.class.getSimpleName();
    private Context context;
    private List<DocumentReference> itemList;
    private OnAddGroupSelectedListener onAddGroupSelectedListener;

    public AddGroupSelectedAdapter(Context context, List<DocumentReference> itemList, OnAddGroupSelectedListener onAddGroupSelectedListener) {
        this.context = context;
        this.itemList = itemList;
        this.onAddGroupSelectedListener = onAddGroupSelectedListener;
    }

    @NonNull
    @Override
    public AddGroupSelectedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AddGroupSelectedViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_group_selected_user,
                parent, false), onAddGroupSelectedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AddGroupSelectedViewHolder holder, int position) {
        DocumentReference item = itemList.get(position);
        item.get()
                .addOnSuccessListener(documentSnapshot -> {
                    User user = documentSnapshot.toObject(User.class);
                    ComponentsUtil.displayImageFromApi(holder.getAvatar(), user.getPhotoUrl());
                })
                .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void update(List<DocumentReference> newUserList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new AddGroupSelectedDiffCallback(itemList, newUserList));
        this.itemList.clear();
        this.itemList.addAll(newUserList);
        diffResult.dispatchUpdatesTo(this);
    }

    public class AddGroupSelectedViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        @Getter
        public RoundedImageView avatar;
        @Getter
        public ImageView ivRemove;

        private OnAddGroupSelectedListener onAddGroupSelectedListener;

        private OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onAddGroupSelectedListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                        onAddGroupSelectedListener.onAddGroupSelectedItemClick(position);
                }
            }
        };

        public AddGroupSelectedViewHolder(@NonNull View itemView, OnAddGroupSelectedListener onAddGroupSelectedListener) {
            super(itemView);
            avatar = itemView.findViewById(R.id.add_group_selected_item);
            ivRemove = itemView.findViewById(R.id.add_group_remove);

            this.onAddGroupSelectedListener = onAddGroupSelectedListener;
            itemView.setOnClickListener(this::onClick);

            avatar.setOnClickListener(onClickListener);
            ivRemove.setOnClickListener(onClickListener);
        }

        @Override
        public void onClick(View view) {
            onAddGroupSelectedListener.onAddGroupSelectedItemClick(getAdapterPosition());
        }
    }

    public interface OnAddGroupSelectedListener {
        void onAddGroupSelectedItemClick(int position);
    }

    private class AddGroupSelectedDiffCallback extends DiffCallback<DocumentReference> {

        public AddGroupSelectedDiffCallback(List<DocumentReference> oldList, List<DocumentReference> newList) {
            super(oldList, newList);
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition)
                    .equals(newList.get(newItemPosition));
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).getId().equals(newList.get(newItemPosition).getId());
        }
    }
}
