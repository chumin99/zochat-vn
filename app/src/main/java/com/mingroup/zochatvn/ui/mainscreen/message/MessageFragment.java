package com.mingroup.zochatvn.ui.mainscreen.message;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.data.model.ConservationType;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.AddFriendActivity;
import com.mingroup.zochatvn.ui.mainscreen.contact.friendrequest.FriendRequestActivity;
import com.mingroup.zochatvn.ui.mainscreen.contact.friendrequest.FriendRequestListAdapter;
import com.mingroup.zochatvn.ui.mainscreen.group.ConservationListAdapter;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.ChatActivity;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

public class MessageFragment extends Fragment
        implements View.OnClickListener, FriendRequestListAdapter.OnFriendRequestItemListener, ConservationListAdapter.OnConservationItemListener {
    private static final String TAG = MessageFragment.class.getSimpleName();
    private RecyclerView rvGroupList, rvFriendRequestList;
    private Button btnLoadMoreConservation, btnLoadMoreFrSuggest, btnAddMoreFr, btnGoToAddFr;
    private LinearLayout friendSuggestSection;
    private LinearLayout noDataSection;
    private SwipeRefreshLayout swipeRefreshLayout;

    // Suggest Friends Section
    private FriendRequestListAdapter friendRequestAdapter;
    private List<FriendRequest> friendRequestList;

    // Conservation List Section
    private ConservationListAdapter conservationListAdapter;
    private List<Conservation> conservationList;

    private ZCLoadingDialog loading;

    private FirebaseFirestore db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mainscreen_message, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loading = ZCLoadingDialog.create(getContext()).setStyle(ZCLoadingDialog.Style.MODERN_STYLE);
        loading.show();
        init(view);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            populateDataToConservationRv();
            populateDataToFriendRequestRv();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        setupFriendRequestRv();
        setupChatConservationRv();
    }

    private void setupChatConservationRv() {
        loading.show();
        conservationListAdapter = new ConservationListAdapter(getActivity(), conservationList, this);
        rvGroupList.setAdapter(conservationListAdapter);
        populateDataToConservationRv();
    }

    private void populateDataToConservationRv() {
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .whereArrayContains("members", ComponentsUtil.getCurrentUserRef(db))
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.d(TAG, "Listen Failed: " + error.getMessage());
                        return;
                    }

                    if (value != null) {
                        conservationList.clear();
                        value.getDocuments().forEach(documentSnapshot -> {
                            Conservation conservation = documentSnapshot.toObject(Conservation.class);
                            if (conservation != null) {
                                conservation.setId(documentSnapshot.getId());
                                conservationList.add(conservation);
                            }
                        });
                        conservationListAdapter.update(conservationList);
                        updateChatConservationUI();
                    }
                });
    }

    private void updateChatConservationUI() {
        if (conservationListAdapter.getItemCount() > 0) {
            noDataSection.setVisibility(View.GONE);
        } else {
            noDataSection.setVisibility(View.VISIBLE);
        }
        loading.dismiss();
    }

    private void setupFriendRequestRv() {
        friendRequestAdapter = new FriendRequestListAdapter(getActivity(), friendRequestList, this);
        rvFriendRequestList.setAdapter(conservationListAdapter);
        populateDataToFriendRequestRv();
    }

    private void populateDataToFriendRequestRv() {
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .whereArrayContains("users", ComponentsUtil.getCurrentUserRef(db))
                .whereNotEqualTo("createdBy", ComponentsUtil.getCurrentUserRef(db))
                .whereEqualTo("status", FriendRequestStatus.PENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable @org.jetbrains.annotations.Nullable QuerySnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                        if (error != null) {
                            Log.d(TAG, "Listen Failed: " + error.getMessage());
                            return;
                        }

                        if (value != null) {
                            friendRequestList.clear();

                            value.getDocuments().forEach(documentSnapshot -> {
                                FriendRequest contact = documentSnapshot.toObject(FriendRequest.class);
                                if (contact != null) {
                                    contact.setId(documentSnapshot.getId());
                                    friendRequestList.add(contact);
                                }
                            });
                            friendRequestAdapter.update(friendRequestList);
                            updateFriendRequestUI();
                        }
                    }
                });
    }

    private void updateFriendRequestUI() {
        if (friendRequestAdapter.getItemCount() > 0)
            friendSuggestSection.setVisibility(View.VISIBLE);
        else {
            friendSuggestSection.setVisibility(View.GONE);
        }
    }

    private void init(View view) {
        rvGroupList = view.findViewById(R.id.rvGroupList);
        rvFriendRequestList = view.findViewById(R.id.rvFriendRequestList);
        btnLoadMoreConservation = view.findViewById(R.id.btn_load_more_conservation);
        btnLoadMoreFrSuggest = view.findViewById(R.id.btn_fr_suggest_show_more);
        btnAddMoreFr = view.findViewById(R.id.btn_add_more_fr);
        btnGoToAddFr = view.findViewById(R.id.btn_add_friend_fr_chat);
        friendSuggestSection = view.findViewById(R.id.friend_suggest_section);
        noDataSection = view.findViewById(R.id.no_data_section);
        friendRequestList = new ArrayList<>();
        conservationList = new ArrayList<>();
        swipeRefreshLayout = view.findViewById(R.id.conservation_swipe_layout);
        db = FirebaseFirestore.getInstance();

        btnLoadMoreFrSuggest.setOnClickListener(this);
        btnLoadMoreConservation.setOnClickListener(this);
        btnAddMoreFr.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_load_more_conservation:
                break;
            case R.id.btn_fr_suggest_show_more:
                startActivity(new Intent(getActivity(), FriendRequestActivity.class));
                break;
            case R.id.btn_add_friend_fr_chat:
            case R.id.btn_add_more_fr:
                startActivity(new Intent(getActivity(), AddFriendActivity.class));
                break;
        }
    }

    @Override
    public void onFriendRequestItemClick(int position) {

    }

    private void removeItemFromRv(int position) {
        friendRequestList.remove(position);
        friendRequestAdapter.update(friendRequestList);
        friendRequestAdapter.notifyItemRemoved(position);
        updateFriendRequestUI();
    }

    @Override
    public void onFriendRequestAcceptClick(int position) {
        loading.show();
        FriendRequest selectedFriendRequest = friendRequestAdapter.getItem(position);

        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .document(selectedFriendRequest.getId())
                .update("status", FriendRequestStatus.ACCEPTED)
                .addOnSuccessListener(aVoid -> {
                    removeItemFromRv(position);
                })
                .addOnFailureListener(e -> {
                    Log.d("AcceptFriendRequest::onError: ", e.getMessage());
                    Toast.makeText(getActivity(), "Có lỗi xảy ra. Xin hãy thử lại!", Toast.LENGTH_SHORT).show();
                });
        loading.dismiss();
    }

    @Override
    public void onFriendRequestRejectClick(int position) {
        loading.show();
        FriendRequest selectedFriendRequest = friendRequestAdapter.getItem(position);
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .document(selectedFriendRequest.getId())
                .delete()
                .addOnSuccessListener(aVoid -> {
                    removeItemFromRv(position);
                })
                .addOnFailureListener(e -> {
                    Log.d("RejectContactItem::onError: ", e.getMessage());
                    Toast.makeText(getActivity(), "Có lỗi xảy ra. Chúng tôi sẽ kiểm tra lại!", Toast.LENGTH_SHORT).show();
                });
        loading.dismiss();
    }

    @Override
    public void onConservationItemClick(int position) {
        Conservation selectedConservation = conservationListAdapter.getItem(position);
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra("conservation", Parcels.wrap(selectedConservation));
        startActivity(intent);
    }

    @Override
    public void onConservationItemLongClick(int position) {
        CharSequence[] actions = {"Xoá cuộc hội thoại"};
        Conservation selectedConservation = conservationListAdapter.getItem(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tác vụ")
                .setItems(actions, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Bạn có chắc chắn muốn xoá?")
                            .setPositiveButton("Xoá", (dialogInterface1, i1) -> {
                                db.collection(CollectionConst.COLLECTION_CONSERVATION)
                                        .document(selectedConservation.getId())
                                        .delete()
                                        .addOnSuccessListener(unused -> {
                                            dialogInterface1.dismiss();
                                            dialogInterface1.dismiss();
                                            populateDataToConservationRv();
                                        })
                                        .addOnFailureListener(e -> {
                                            Log.d(TAG, "deleteGroup::onError: " + e.getMessage());
                                            Toast.makeText(getActivity(), "Xoá nhóm xảy ra sự cố. Chúng tôi sẽ kiểm tra lại sau!", Toast.LENGTH_SHORT).show();
                                        });
                            })
                            .create()
                            .show();
                })
                .setNegativeButton("Huỷ", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .create()
                .show();
    }
}
