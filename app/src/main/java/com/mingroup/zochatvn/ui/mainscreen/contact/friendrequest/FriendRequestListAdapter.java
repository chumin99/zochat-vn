package com.mingroup.zochatvn.ui.mainscreen.contact.friendrequest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import org.ocpsoft.prettytime.PrettyTime;


import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;

import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.SneakyThrows;

import static com.mingroup.zochatvn.util.ComponentsUtil.getFriendUser;

public class FriendRequestListAdapter extends RecyclerView.Adapter<FriendRequestListAdapter.FriendRequestViewHolder> {
    private Context context;
    private List<FriendRequest> friendRequestList;
    private OnFriendRequestItemListener onFriendRequestItemListener;
    private PrettyTime prettyTime;
    private AsyncListDiffer<FriendRequest> mDiffer;
    private DiffUtil.ItemCallback<FriendRequest> mCallback = new DiffUtil.ItemCallback<FriendRequest>() {
        @Override
        public boolean areItemsTheSame(@NonNull FriendRequest oldItem, @NonNull FriendRequest newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull FriendRequest oldItem, @NonNull FriendRequest newItem) {
            return oldItem.getUsers().equals(newItem.getUsers());
        }
    };


    public FriendRequestListAdapter(Context context, List<FriendRequest> friendRequestList, OnFriendRequestItemListener onFriendRequestItemListener) {
        this.context = context;
        this.friendRequestList = friendRequestList;
        this.onFriendRequestItemListener = onFriendRequestItemListener;
        this.prettyTime = new PrettyTime(ComponentsUtil.getCurrentLocale(context));
        this.mDiffer = new AsyncListDiffer<FriendRequest>(this, mCallback);
    }

    @NonNull
    @Override
    public FriendRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FriendRequestViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_friend_request, parent, false
        ), onFriendRequestItemListener);
    }

    @SneakyThrows
    @Override
    public void onBindViewHolder(@NonNull FriendRequestViewHolder holder, int position) {
        FriendRequest item = getItem(position);

        Objects.requireNonNull(getFriendUser(item.getUsers()))
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    User friendUser = documentSnapshot.toObject(User.class);
                    ComponentsUtil.displayImageFromApi(holder.getAvatar(), friendUser.getPhotoUrl());
                    holder.getTvDisplayName().setText(friendUser.getDisplayName());
                    holder.getTvCreatedTime().setText(prettyTime.format(friendUser.getDob()));

                    String message = item.getMessage();
                    if (message != null && !message.isEmpty())
                        holder.getTvMessage().setText(message);
                    else
                        holder.getCvMessage().setVisibility(View.GONE);

                    if (position == 0) {
                        holder.getDividerLine().setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public FriendRequest getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<FriendRequest> newContactList) {
        mDiffer.submitList(newContactList);
    }

    @Getter
    public class FriendRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RoundedImageView avatar;
        private TextView tvDisplayName;
        private TextView tvCreatedTime;
        private TextView tvMessage;
        private View dividerLine;
        private CardView cvMessage;
        private Button btnAccept;
        private ImageView btnReject;
        private OnFriendRequestItemListener onFriendRequestItemListener;

        public FriendRequestViewHolder(@NonNull View itemView, OnFriendRequestItemListener onFriendRequestItemListener) {
            super(itemView);
            this.onFriendRequestItemListener = onFriendRequestItemListener;
            itemView.setOnClickListener(this);

            avatar = itemView.findViewById(R.id.friend_request_avatar);
            tvDisplayName = itemView.findViewById(R.id.friend_request_display_name);
            tvCreatedTime = itemView.findViewById(R.id.friend_request_created_time);
            tvMessage = itemView.findViewById(R.id.friend_request_message);
            dividerLine = itemView.findViewById(R.id.dividerLine);
            cvMessage = itemView.findViewById(R.id.cvMessage);
            btnAccept = itemView.findViewById(R.id.btnAccept);
            btnReject = itemView.findViewById(R.id.btnReject);

            btnAccept.setOnClickListener(view -> {
                if (onFriendRequestItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                        onFriendRequestItemListener.onFriendRequestAcceptClick(position);
                }
            });

            btnReject.setOnClickListener(view -> {
                if (onFriendRequestItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                        onFriendRequestItemListener.onFriendRequestRejectClick(position);
                }
            });
        }

        @Override
        public void onClick(View view) {
            onFriendRequestItemListener.onFriendRequestItemClick(getAdapterPosition());
        }
    }

    public interface OnFriendRequestItemListener {
        void onFriendRequestItemClick(int position);

        void onFriendRequestAcceptClick(int position);

        void onFriendRequestRejectClick(int position);
    }

}
