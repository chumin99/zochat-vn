package com.mingroup.zochatvn.ui.mainscreen.more.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.ui.mainscreen.more.profile.editprofile.EditProfileActivity;
import com.mingroup.zochatvn.util.StringUtil;

import java.text.ParseException;

import lombok.SneakyThrows;

public class ProfileFragment extends Fragment {
    public static final int EDIT_PROFILE_REQUEST_CODE = 288;
    private Button btnEditProfile;
    private TextView tvDisplayName, tvGender, tvDob, tvEmail, tvPhone;
    private TableRow tvPhoneNumberDesc, phoneNumberSection, emailSection;
    private User currentUser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_screen_more_profile, container, false);
    }

    @SneakyThrows
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        displayCurrentUserProfile();
        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivityForResult(intent, EDIT_PROFILE_REQUEST_CODE);
            }
        });
    }

    private void init(View view) {
        tvDisplayName = view.findViewById(R.id.profile_displayName);
        tvGender = view.findViewById(R.id.profile_gender);
        tvDob = view.findViewById(R.id.profile_dob);
        tvEmail = view.findViewById(R.id.profile_email);
        tvPhone = view.findViewById(R.id.profile_phone);
        tvPhoneNumberDesc = view.findViewById(R.id.tvPhoneNumberDesc);
        btnEditProfile = view.findViewById(R.id.btnEditProfile);
        phoneNumberSection = view.findViewById(R.id.phoneNumberSection);
        emailSection = view.findViewById(R.id.emailSection);
    }

    private void displayCurrentUserProfile() throws ParseException {
        User currentUser = new SessionManager(getActivity()).getCurrentUserInfo();
        tvDisplayName.setText(currentUser.getDisplayName());
        tvGender.setText(currentUser.isMale() ? "Nam" : "Nữ");
        tvDob.setText(StringUtil.convertDateToString(currentUser.getDob()));

        String email = currentUser.getEmail();
        if (email != null && !email.contains(StringUtil.EMAIL_ZOCHAT_SUFFIX)) {
            tvEmail.setText(email);
            phoneNumberSection.setVisibility(View.GONE);
            tvPhoneNumberDesc.setVisibility(View.GONE);
        } else {
            tvPhone.setText(currentUser.getPhoneNumber());
            tvPhoneNumberDesc.setVisibility(View.VISIBLE);
            emailSection.setVisibility(View.GONE);
        }
    }

    @SneakyThrows
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_PROFILE_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
            displayCurrentUserProfile();
        }
    }
}
