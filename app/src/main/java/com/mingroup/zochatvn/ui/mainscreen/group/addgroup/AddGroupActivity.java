package com.mingroup.zochatvn.ui.mainscreen.group.addgroup;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.cpoopc.scrollablelayoutlib.ScrollableHelper;
import com.cpoopc.scrollablelayoutlib.ScrollableLayout;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.makeramen.roundedimageview.RoundedDrawable;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.constant.StorageRefConst;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.adapter.AddGroupPagerAdapter;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.adapter.AddGroupSelectedAdapter;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.adapter.ResultListAdapter;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.ChatActivity;
import com.mingroup.zochatvn.ui.mainscreen.more.profile.editprofile.EditProfileActivity;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.file.FileUtils;

import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.listener.OnSelectedListener;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class AddGroupActivity extends AppCompatActivity implements View.OnFocusChangeListener, View.OnTouchListener, AddGroupSelectedAdapter.OnAddGroupSelectedListener, ResultListAdapter.OnResultListListener {
    private static final int TAKE_PHOTO_CODE = 132;
    private final String TAG = this.getClass().getSimpleName();
    private Toolbar zcToolbar;
    private ScrollableLayout scrollableAddGrLayout;
    private ImageView btnChooseGroupPhoto, btnNumberKeyboard, btnTextKeyboard, ivEmojicon;
    private ImageButton ibtnDone;
    private SearchView svAddGroup;
    private TextInputLayout tipGroupName;
    private ViewPager vpAddGroup;
    private TabLayout tabLayout;
    private InputMethodManager imm;
    private LinearLayout layout;
    private EmojiconEditText etGroupName;
    private int lastSearchType;
    private AddGroupViewModel addGroupViewModel;
    private BottomSheetBehavior sheetBehavior;
    private LinearLayout layoutBottomSheet;
    public ConstraintLayout groupNameSection;
    private int groupNameSectionHeight;
    private RecyclerView rvResultList;
    private ResultListAdapter resultListAdapter;
    private CombineListViewModel combineListViewModel;
    private TextView tvNoResult;
    private List<DocumentReference> selectedUserGroup;
    private List<String> listUserUid;
    private LayoutManager svLayoutManager;
    private Filter.FilterListener filterListener = new Filter.FilterListener() {
        @Override
        public void onFilterComplete(int i) {
            if (i == 0) {
                tvNoResult.setVisibility(View.VISIBLE);
                rvResultList.setVisibility(View.GONE);
            } else {
                rvResultList.post(new Runnable() {
                    @Override
                    public void run() {
                        List<String> userSelectedListName = new ArrayList<>();
                        for (DocumentReference userRef : selectedUserGroup) {
                            userRef.get()
                                    .addOnSuccessListener(documentSnapshot -> {
                                        User user = documentSnapshot.toObject(User.class);
                                        userSelectedListName.add(user.getDisplayName());
                                    });
                        }

                        for (int pos = 0; pos < svLayoutManager.getChildCount(); pos++) {
                            View view = svLayoutManager.findViewByPosition(pos);

                            if (view != null) {
                                CheckBox cbSelect = view.findViewById(R.id.cbSelect2);
                                TextView tvDisplayName = view.findViewById(R.id.result_user_display_name);
                                cbSelect.setChecked(userSelectedListName.contains(tvDisplayName.getText().toString()));
                            }

                        }
                    }
                });

                tvNoResult.setVisibility(View.GONE);
                rvResultList.setVisibility(View.VISIBLE);
            }
        }
    };
    private AddGroupSelectedAdapter adapter;
    private LayoutManager layoutManager;
    private RecyclerView rvAddGroupSelectedList;
    private FloatingActionButton fabCreateGroup;
    private String selectedPhotoUrl;
    private FirebaseFirestore db;
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        init();
        setupTabPage();
        setupActionBar();
        setupSwitchKeyboardForSearchView();
        setupSearchView();
        setupAddGroupBottomSheet();
        handleGroupNameSection();
        ivEmojicon.setVisibility(View.INVISIBLE);
    }

    private void handleGroupNameSection() {
        etGroupName.setOnFocusChangeListener(this);
        svAddGroup.setOnQueryTextFocusChangeListener(this);

        ViewTreeObserver observer = groupNameSection.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                groupNameSectionHeight = groupNameSection.getHeight();
            }
        });

        ibtnDone.setOnClickListener(view -> {
            scrollableAddGrLayout.scrollTo(0, groupNameSectionHeight);
        });

        etGroupName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    scrollableAddGrLayout.scrollTo(0, groupNameSectionHeight);
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    scrollableAddGrLayout.scrollTo(0, groupNameSectionHeight);
                    svAddGroup.requestFocus();
                    return true;
                }
                return false;
            }
        });

        scrollableAddGrLayout.setOnScrollListener(new ScrollableLayout.OnScrollListener() {
            @Override
            public void onScroll(int currentY, int maxY) {
                if (currentY >= groupNameSectionHeight) {
                    String groupName = etGroupName.getText().toString();
                    if (!groupName.isEmpty())
                        zcToolbar.setTitle("Tên nhóm: " + groupName);
                    else {
                        zcToolbar.setTitle("Nhóm chưa đặt tên");
                        zcToolbar.setSubtitle("Bấm để đặt tên nhóm");
                        zcToolbar.setClickable(true);
                        zcToolbar.setOnClickListener(view -> {
                            scrollableAddGrLayout.scrollTo(0, 0);
                            etGroupName.requestFocus();
                        });
                    }
                } else {
                    zcToolbar.setTitle("Nhóm mới");
                    zcToolbar.setSubtitle("Đã chọn: " + selectedUserGroup.size());
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (adapter.getItemCount() > 0) {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
                builder.setTitle("Xác nhận")
                        .setMessage("Bạn muốn huỷ tạo nhóm này?")
                        .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                                onBackPressed();
                            }
                        })
                        .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
            } else {
                finish();
                onBackPressed();
            }
            return true;
        }
        return false;
    }

    private void setupAddGroupBottomSheet() {
        selectedUserGroup = new ArrayList<>();
        layoutBottomSheet = findViewById(R.id.layoutAddGroupBottomSheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setDraggable(false);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        rvAddGroupSelectedList = layoutBottomSheet.findViewById(R.id.rvAddGroupSelectedList);
        fabCreateGroup = layoutBottomSheet.findViewById(R.id.fabCreateGroup);

        adapter = new AddGroupSelectedAdapter(this, selectedUserGroup, this::onAddGroupSelectedItemClick);
        rvAddGroupSelectedList.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvAddGroupSelectedList.setLayoutManager(layoutManager);

        addGroupViewModel.getSelectedUsers().observe(this, new Observer<List<DocumentReference>>() {
            @Override
            public void onChanged(List<DocumentReference> users) {
                adapter.update(users);
                zcToolbar.setSubtitle("Đã chọn: " + users.size());

                if (users.size() > 0) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            }
        });

        fabCreateGroup.setOnClickListener(view -> {
            if (selectedPhotoUrl != null) {
                if (!selectedPhotoUrl.contains(getString(R.string.google_storage_bucket))) {
                    updateGroupPhoto();
                } else {
                    createGroup();
                }
            } else {
                createGroup();
            }
        });
    }

    private void updateGroupPhoto() {
        Uri groupPhotoUri = Uri.parse(selectedPhotoUrl);
        StorageReference avatarRef = mStorageRef.child(StorageRefConst.FOLDER_GROUP_PHOTO)
                .child(FileUtils.getFileName(this, groupPhotoUri));

        avatarRef.putFile(groupPhotoUri)
                .addOnSuccessListener(taskSnapshot -> {
                    Task<Uri> task = Objects.requireNonNull(Objects.requireNonNull(taskSnapshot.getMetadata()).getReference()).getDownloadUrl();
                    task
                            .addOnSuccessListener(uri -> {
                                selectedPhotoUrl = uri.toString();
                                createGroup();
                            })
                            .addOnFailureListener(e -> {
                                Toast.makeText(AddGroupActivity.this, "Tạo nhóm mới xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "get_group_photo_url::onFailure: " + e.getMessage());
                            });
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(AddGroupActivity.this, "Tạo nhóm mới xảy ra sự cố. Thử lại!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "upload_photo_url::onFailure: " + e.getMessage());
                    }
                });
    }

    private void createGroup() {
        if (adapter.getItemCount() >= 2) {

            Conservation newGroup = Conservation.createNewGroup(etGroupName.getText().toString(),
                    selectedPhotoUrl, addGroupViewModel.getSelectedUsers().getValue(), ComponentsUtil.getCurrentUserRef(db));
            db.collection(CollectionConst.COLLECTION_CONSERVATION)
                    .add(newGroup)
                    .addOnSuccessListener(documentReference -> {
                        Intent intent = new Intent(AddGroupActivity.this, ChatActivity.class);
                        intent.putExtra("conservation", Parcels.wrap(newGroup));
                        startActivity(intent);
                        finish();
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(AddGroupActivity.this, "Tạo nhóm mới xảy sự cố. Thử lại!", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "addGroupMembers::onFailure : " + e.getMessage());
                    });
        } else if (adapter.getItemCount() == 1) {
            Intent intent = new Intent(this, ChatActivity.class);
            intent.putExtra("chat_friend", Parcels.wrap(selectedUserGroup.get(0)));
            startActivity(intent);
            finish();
        }
    }

    private void setupTabPage() {
        AddGroupPagerAdapter addGroupPagerAdapter = new AddGroupPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        vpAddGroup.setAdapter(addGroupPagerAdapter);
        tabLayout.setupWithViewPager(vpAddGroup);
        vpAddGroup.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                scrollableAddGrLayout.getHelper().setCurrentScrollableContainer((ScrollableHelper.ScrollableContainer) addGroupPagerAdapter.getItem(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setupSearchView() {
        resultListAdapter = new ResultListAdapter(this, combineListViewModel.getCombineList().getValue(), this);
        rvResultList.setAdapter(resultListAdapter);
        svLayoutManager = new LinearLayoutManager(this);
        rvResultList.setLayoutManager(svLayoutManager);

        svAddGroup.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                resultListAdapter.getFilter().filter(query, filterListener);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!TextUtils.isEmpty(newText)) {
                    btnNumberKeyboard.setVisibility(View.INVISIBLE);
                    btnTextKeyboard.setVisibility(View.INVISIBLE);

                    vpAddGroup.setVisibility(View.GONE);
                    resultListAdapter.getFilter().filter(newText, filterListener);
                    rvResultList.setVisibility(View.VISIBLE);
                } else {
                    vpAddGroup.setVisibility(View.VISIBLE);
                    rvResultList.setVisibility(View.GONE);
                    tvNoResult.setVisibility(View.GONE);

                    if (lastSearchType == InputType.TYPE_CLASS_TEXT)
                        btnTextKeyboard.setVisibility(View.VISIBLE);
                    else
                        btnNumberKeyboard.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });
    }

    private void setupSwitchKeyboardForSearchView() {
        btnNumberKeyboard.setOnClickListener(view -> {
            btnNumberKeyboard.setVisibility(View.INVISIBLE);
            btnTextKeyboard.setVisibility(View.VISIBLE);
            svAddGroup.setInputType(InputType.TYPE_CLASS_TEXT);
            svAddGroup.requestFocus();
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            lastSearchType = InputType.TYPE_CLASS_TEXT;
        });
        btnTextKeyboard.setOnClickListener(view -> {
            btnTextKeyboard.setVisibility(View.INVISIBLE);
            btnNumberKeyboard.setVisibility(View.VISIBLE);
            svAddGroup.setInputType(InputType.TYPE_CLASS_PHONE);
            svAddGroup.requestFocus();
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 1);
            lastSearchType = InputType.TYPE_CLASS_PHONE;
        });

        EmojIconActions emojiIconActions = new EmojIconActions(this, layout, etGroupName, ivEmojicon);
        emojiIconActions.setIconsIds(hani.momanii.supernova_emoji_library.R.drawable.ic_action_keyboard, R.drawable.ic_emoji_pack);
        emojiIconActions.ShowEmojIcon();
    }

    private void setupActionBar() {
        zcToolbar.setBackgroundColor(getColor(android.R.color.white));
        zcToolbar.setTitleTextColor(getColor(android.R.color.black));
        zcToolbar.setSubtitleTextColor(getColor(android.R.color.black));
        zcToolbar.setSubtitleTextAppearance(this, android.R.style.TextAppearance_Material_Caption);
        zcToolbar.getContext().setTheme(R.style.ZOchatTheme_WhiteToolbar);
        zcToolbar.setTitle("Nhóm mới");
        zcToolbar.setSubtitle("Đã chọn: 0");

        setSupportActionBar(zcToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init() {
        zcToolbar = findViewById(R.id.zochat_toolbar_no_search_view);
        btnChooseGroupPhoto = findViewById(R.id.btnChooseGroupPhoto);
        btnNumberKeyboard = findViewById(R.id.btnNumberKeyboard);
        btnTextKeyboard = findViewById(R.id.btnTextKeyboard);
        ibtnDone = findViewById(R.id.btnDone);
        svAddGroup = findViewById(R.id.svAddGroup);
        tipGroupName = findViewById(R.id.tipGroupName);
        vpAddGroup = findViewById(R.id.vpAddGroup);
        tabLayout = findViewById(R.id.add_group_tab_layout);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        layout = findViewById(R.id.add_group_layout);
        ivEmojicon = findViewById(R.id.ivEmojicon);
        etGroupName = findViewById(R.id.etGroupName);
        addGroupViewModel = new ViewModelProvider(this).get(AddGroupViewModel.class);
        groupNameSection = findViewById(R.id.group_name_section);
        scrollableAddGrLayout = findViewById(R.id.scrollableAddGrLayout);
        rvResultList = findViewById(R.id.rvResultList);
        combineListViewModel = new ViewModelProvider(this).get(CombineListViewModel.class);
        tvNoResult = findViewById(R.id.tvNoResult);
        listUserUid = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    public void changeGroupPhoto(View view) {
        View v = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_dialog_change_group_photo, null);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(v);
        bottomSheetDialog.show();

        final TableRow btnTakePicture = v.findViewById(R.id.change_grphoto_take_a_picture_selection);
        final TableRow btnChoosePicture = v.findViewById(R.id.change_grphoto_choose_existing_photo_selection);
        final ImageView ivCalendarGrPhoto = v.findViewById(R.id.calendar_group_photo);
        ivCalendarGrPhoto.setTag("calendar.jpg");
        final ImageView ivTaskGrPhoto = v.findViewById(R.id.task_group_photo);
        ivTaskGrPhoto.setTag("task.jpg");
        final ImageView ivSchoolGrPhoto = v.findViewById(R.id.school_group_photo);
        ivSchoolGrPhoto.setTag("school.jpg");
        final ImageView ivFamilyGrPhoto = v.findViewById(R.id.family_group_photo);
        ivFamilyGrPhoto.setTag("family.jpg");
        final TextView tvDone = v.findViewById(R.id.btn_update_group_photo_done);

        OnClickListener onClickListener = new OnClickListener() {
            @Override
            public void onClick(View view1) {
                Bitmap bitmap = ((RoundedDrawable) ((ImageView) view1).getDrawable()).getSourceBitmap();
                btnChooseGroupPhoto.setImageBitmap(bitmap);
                btnChooseGroupPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
                btnChooseGroupPhoto.setMinimumHeight(50);
                btnChooseGroupPhoto.setMinimumWidth(50);
                btnChooseGroupPhoto.setPadding(0, 0, 0, 0);
                mStorageRef.child(StorageRefConst.FOLDER_DEFAULT)
                        .child((String) view1.getTag())
                        .getDownloadUrl()
                        .addOnSuccessListener(uri -> {
                            selectedPhotoUrl = uri.toString();
                            Log.d(TAG, "photoUrl " + uri.toString());
                        })
                .addOnFailureListener(e -> Log.d(TAG, "default_group_photo::onFailure: " + e.getMessage()));
            }
        };

        ivCalendarGrPhoto.setOnClickListener(onClickListener);
        ivTaskGrPhoto.setOnClickListener(onClickListener);
        ivSchoolGrPhoto.setOnClickListener(onClickListener);
        ivFamilyGrPhoto.setOnClickListener(onClickListener);

        btnTakePicture.setOnClickListener(view1 -> {
            TedPermission.with(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            takeAPictureFromCamera();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Toast.makeText(getApplicationContext(), "Cấp phép quyền truy cập để tiếp tục.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.CAMERA)
                    .check();
        });

        btnChoosePicture.setOnClickListener(view1 -> {
            TedPermission.with(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            pickImageFromGallery();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Toast.makeText(getApplicationContext(), "Cấp phép quyền truy cập để tiếp tục.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.CAMERA)
                    .check();
        });
    }

    private void takeAPictureFromCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    private void pickImageFromGallery() {
        TedImagePicker.with(this)
                .title("Chọn ảnh")
                .buttonText("Xong")
                .buttonBackground(R.color.blue_magenta)
                .buttonTextColor(android.R.color.white)
                .start(new OnSelectedListener() {
                    @Override
                    public void onSelected(@NotNull Uri uri) {
                        selectedPhotoUrl = uri.toString();
                        ComponentsUtil.displayImage(btnChooseGroupPhoto, selectedPhotoUrl);
                        btnChooseGroupPhoto.setPadding(0, 0, 0, 0);
                        btnChooseGroupPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            selectedPhotoUrl = ComponentsUtil.getImageUri(AddGroupActivity.this, (Bitmap) data.getExtras().get("data")).toString();
            ComponentsUtil.displayImage(btnChooseGroupPhoto, selectedPhotoUrl);
            btnChooseGroupPhoto.setPadding(0, 0, 0, 0);
            btnChooseGroupPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId()) {
            case R.id.etGroupName:
                if (b) {
                    ivEmojicon.setVisibility(View.VISIBLE);
                    ibtnDone.setVisibility(View.VISIBLE);
                } else {
                    ivEmojicon.setVisibility(View.INVISIBLE);
                    ibtnDone.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.svAddGroup:
                if (b) {
                    scrollableAddGrLayout.scrollTo(0, groupNameSectionHeight);
                }
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view instanceof EditText) {
            view.setOnFocusChangeListener(this::onFocusChange);
        } else if (view instanceof SearchView) {
            view.setOnFocusChangeListener(this::onFocusChange);
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onAddGroupSelectedItemClick(int position) {
        addGroupViewModel.removeUser(selectedUserGroup.get(position));
    }

    @Override
    public void onUserItemClick(int position) {

    }

    @Override
    public void onCheckboxCheckedChange(int position, CompoundButton compoundButton, boolean isChecked) {
        DocumentReference selectedUser = resultListAdapter.getItem(position);
        if (compoundButton.isChecked()) {
            addGroupViewModel.addUser(selectedUser);
        } else {
            addGroupViewModel.removeUser(selectedUser);
        }
    }
}