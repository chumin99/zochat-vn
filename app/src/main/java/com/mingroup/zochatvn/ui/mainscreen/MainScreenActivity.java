package com.mingroup.zochatvn.ui.mainscreen;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.AddFriendActivity;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.AddGroupActivity;
import com.mingroup.zochatvn.ui.mainscreen.more.profile.editprofile.EditProfileActivity;
import com.mingroup.zochatvn.ui.register.RegisterStepThreeFragment;
import com.mingroup.zochatvn.util.FragmentUtil;
import com.mingroup.zochatvn.util.StringUtil;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class MainScreenActivity extends AppCompatActivity {
    public static final String FLAG_RE_VERIFIED_MAIL = "re_verified_mail";
    private Toolbar zcToolBar;
    private BottomNavigationView navView;
    private TextView tvQueryHint;
    private FirebaseAuth zcAuth;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        init();

        FirebaseUser currentUser = zcAuth.getCurrentUser();
        currentUser.reload();

        String currentUserEmail = currentUser.getEmail();
        if (currentUserEmail.contains(StringUtil.EMAIL_ZOCHAT_SUFFIX)) {
            storeCurrentUserSession();
        } else {
            if (!zcAuth.getCurrentUser().isEmailVerified()) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(FLAG_RE_VERIFIED_MAIL, true);
                FragmentUtil.replaceFragment(this, R.id.main_screen_container, new RegisterStepThreeFragment(),
                        bundle, RegisterStepThreeFragment.class.getSimpleName(), false);
            } else
                storeCurrentUserSession();
        }

        setSupportActionBar(zcToolBar);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.bottom_nav_message, R.id.bottom_nav_contact, R.id.bottom_nav_group, R.id.bottom_nav_more)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.bottom_nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        Locale locale = new Locale("vi", "VN");
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.setLocale(locale);
        createConfigurationContext(config);

        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                    }
                })
                .setDeniedMessage("Nếu từ chối, bạn sẽ có thể không thể sử dụng dịch vụ này\n\nXin hãy cấp quyền tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    private void init() {
        navView = findViewById(R.id.bottom_nav_view);
        zcToolBar = findViewById(R.id.zochat_toolbar);
        tvQueryHint = findViewById(R.id.tvQueryHint);
        zcAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_bar_menu, menu);
        SearchView searchView = findViewById(R.id.top_bar_search);
        if (navView.getSelectedItemId() == R.id.bottom_nav_contact) {
            menu.findItem(R.id.top_bar_action_more).setVisible(false);
        } else {
            menu.findItem(R.id.top_bar_action_add_friend).setVisible(false);
        }
        customizeSearchView(searchView);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.top_bar_action_more:
                final View menuItemView = findViewById(item.getItemId());
                PopupMenu popupMenu = new PopupMenu(this, menuItemView, Gravity.END, 0, R.style.MyPopupMenu);
                try {
                    Method method = popupMenu.getMenu().getClass().getDeclaredMethod("setOptionalIconsVisible", boolean.class);
                    method.setAccessible(true);
                    method.invoke(popupMenu.getMenu(), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.menu_more_add_friend:
                                startActivity(new Intent(getApplicationContext(), AddFriendActivity.class));
                                return true;
                            case R.id.menu_more_add_group:
                                startActivity(new Intent(getApplicationContext(), AddGroupActivity.class));
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.getMenuInflater().inflate(R.menu.child_more_menu, popupMenu.getMenu());
                popupMenu.show();
                return true;
            default:
                return false;
        }
    }

    private void customizeSearchView(SearchView searchView) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        // set action view mặc định không expanded.
        searchView.onActionViewCollapsed();
        searchView.clearFocus();

        // set edit text color white
        EditText editText = searchView.findViewById(androidx.appcompat.R.id.search_src_text);
        editText.setTextColor(Color.WHITE);
        editText.setHintTextColor(getResources().getColor(R.color.search_view_hint_color, getTheme()));

        // Change Search Icon when clicked
        ImageView ivSearchHint = searchView.findViewById(androidx.appcompat.R.id.search_mag_icon);
        ivSearchHint.setClickable(true);
        Drawable selectableItemBg = obtainStyledAttributes(new int[]{android.R.attr.selectableItemBackground}).getDrawable(0);
        ivSearchHint.setForeground(selectableItemBg);
        ivSearchHint.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_arrow_back_24, getTheme()));
        ivSearchHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.onActionViewCollapsed();
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) // expanded
                    tvQueryHint.setVisibility(View.INVISIBLE);
                else
                    tvQueryHint.setVisibility(View.VISIBLE);
            }
        });

        tvQueryHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView.onActionViewExpanded();
            }
        });
    }

    private void storeCurrentUserSession() {
        db.collection(CollectionConst.COLLECTION_USER)
                .document(Objects.requireNonNull(zcAuth.getCurrentUser()).getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.d("SaveCurrentUser", task.getException().getMessage());
                    } else {
                        User currentUser = Objects.requireNonNull(task.getResult()).toObject(User.class);

                        if (currentUser != null) {
                            try {
                                new SessionManager(getApplicationContext()).saveCurrentUserInfo(currentUser);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Intent intent = new Intent(MainScreenActivity.this, EditProfileActivity.class);
                            intent.putExtra("hasUserProfile", false);
                            startActivity(intent);
                        }
                    }
                });
    }
}
