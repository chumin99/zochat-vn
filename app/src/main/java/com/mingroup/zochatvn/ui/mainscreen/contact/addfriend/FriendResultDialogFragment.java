package com.mingroup.zochatvn.ui.mainscreen.contact.addfriend;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.auth.FirebaseAuth;

import org.parceler.Parcels;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class FriendResultDialogFragment extends DialogFragment {
    private static final String TAG = FriendResultDialogFragment.class.getSimpleName();

    public enum FriendResultStatus {
        ITS_ME,
        IS_FRIEND,
        NOT_FRIEND,
        SENT_FRIEND_REQUEST,
        PENDING_FRIEND_REQUEST
    }

    private User user;
    private FirebaseFirestore db;
    private FriendResultStatus status;
    private Button btnChat, btnAddFriend;
    private TextView tvDisplayName;
    private ImageView ivAvatar;
    private FirebaseAuth mAuth;
    private List<DocumentReference> users;
    private FriendRequest friendRequest;

    public FriendResultDialogFragment() {
    }

    public static FriendResultDialogFragment newInstance() {
        return new FriendResultDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return inflater.inflate(R.layout.dialog_add_friend_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View dialogView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(dialogView, savedInstanceState);
        init();
        btnChat = dialogView.findViewById(R.id.btn_add_friend_chat);
        btnAddFriend = dialogView.findViewById(R.id.btn_add_friend);
        tvDisplayName = dialogView.findViewById(R.id.add_friend_profile_displayName);
        ivAvatar = dialogView.findViewById(R.id.add_friend_profile_avatar);

        tvDisplayName.setText(user.getDisplayName());
        ComponentsUtil.displayImage(ivAvatar, user.getPhotoUrl());

        checkFriendStatus();

        btnChat.setOnClickListener(view -> {
            Toast.makeText(getActivity(), "Đã click chat", Toast.LENGTH_SHORT).show();
        });

        btnAddFriend.setOnClickListener(view -> {
            doAddFriend();
        });
    }

    private void checkFriendStatus() {
        if (user.getUid().equals(ComponentsUtil.currentUserUid)) {
            btnAddFriend.setVisibility(View.GONE);
            status = FriendResultStatus.ITS_ME;
        } else {
            Query query = db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                    .whereEqualTo("users", users);
            query.get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            checkFriendRequestStatus(queryDocumentSnapshots);
                        } else {
                            Collections.reverse(users);
                            query.get()
                                    .addOnSuccessListener(queryDocumentSnapshots1 -> {
                                        if(!queryDocumentSnapshots1.isEmpty()) {
                                            checkFriendRequestStatus(queryDocumentSnapshots1);
                                        } else {
                                            status = FriendResultStatus.NOT_FRIEND;
                                        }
                                    })
                            .addOnFailureListener(e -> {
                                if (e instanceof TimeoutException) {
                                    Toast.makeText(getContext(), "Vui lòng kiểm tra lại đường truyền Internet!", Toast.LENGTH_SHORT).show();
                                }
                                Log.d(TAG, "::onFailure" + e.getMessage());
                            });
                        }
                    })
                    .addOnFailureListener(e -> {
                        if (e instanceof TimeoutException) {
                            Toast.makeText(getContext(), "Vui lòng kiểm tra lại đường truyền Internet!", Toast.LENGTH_SHORT).show();
                        }
                        Log.d(TAG, "::onFailure" + e.getMessage());
                    });
        }
    }

    private void checkFriendRequestStatus(QuerySnapshot queryDocumentSnapshots) {
        friendRequest = queryDocumentSnapshots.toObjects(FriendRequest.class).get(0);
        friendRequest.setId(queryDocumentSnapshots.getDocuments().get(0).getId());
        switch (friendRequest.getStatus()) {
            case ACCEPTED:
                btnAddFriend.setVisibility(View.GONE);
                status = FriendResultStatus.IS_FRIEND;
                break;
            case PENDING:
                if (friendRequest.getCreatedBy().getId().equals(ComponentsUtil.currentUserUid)) {
                    btnAddFriend.setText("Huỷ kết bạn");
                    btnAddFriend.setBackgroundColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
                    status = FriendResultStatus.PENDING_FRIEND_REQUEST;
                } else {
                    btnAddFriend.setText("Đã gửi kết bạn");
                    btnAddFriend.setBackgroundColor(getResources().getColor(R.color.orange_pure_1, getActivity().getTheme()));
                    status = FriendResultStatus.SENT_FRIEND_REQUEST;
                }
                break;
        }
    }

    private void doAddFriend() {
        switch (status) {
            case NOT_FRIEND:
                Intent intent = new Intent(getActivity(), SendFriendRequestActivity.class);
                intent.putExtra("user", Parcels.wrap(user));
                startActivityForResult(intent, AddFriendActivity.SEND_FRIEND_REQUEST_RESULT_CODE);
                break;
            case SENT_FRIEND_REQUEST:
                PopupMenu popupMenu = new PopupMenu(getActivity(), btnAddFriend, Gravity.TOP);
                try {
                    Method method = popupMenu.getMenu().getClass().getDeclaredMethod("setOptionalIconsVisible", boolean.class);
                    method.setAccessible(true);
                    method.invoke(popupMenu.getMenu(), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                popupMenu.getMenuInflater().inflate(R.menu.add_friend_menu, popupMenu.getMenu());
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(menuItem -> {
                    switch (menuItem.getItemId()) {
                        case R.id.menu_accept_friend_request:
                            db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                                    .document(friendRequest.getId())
                                    .update("status", FriendRequestStatus.ACCEPTED)
                                    .addOnSuccessListener(aVoid -> {
                                        btnAddFriend.setVisibility(View.GONE);
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast toast = Toast.makeText(getActivity(), "Chấp nhận kết bạn thất bại", Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        Log.d("AcceptFrRequest", e.getMessage());
                                    });
                            return true;
                        case R.id.menu_remove_friend_request:
                            db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                                    .document(friendRequest.getId())
                                    .delete()
                                    .addOnSuccessListener(aVoid -> {
                                        btnAddFriend.setText("Kết bạn");
                                        btnAddFriend.setBackgroundColor(getResources().getColor(R.color.blue_magenta, getActivity().getTheme()));
                                        status = FriendResultStatus.NOT_FRIEND;
                                    })
                                    .addOnFailureListener(e -> {
                                        Toast toast = Toast.makeText(getActivity(), "Xoá lời mời kết bạn thất bại", Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER, 0, 0);
                                        toast.show();
                                        Log.d("RejectFrRequest", e.getMessage());
                                    });
                            return true;
                    }
                    return false;
                });
                break;
            case PENDING_FRIEND_REQUEST:
                db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                        .document(friendRequest.getId())
                        .delete()
                        .addOnSuccessListener(aVoid -> {
                            btnAddFriend.setText("Kết bạn");
                            btnAddFriend.setBackgroundColor(getResources().getColor(R.color.blue_magenta, getActivity().getTheme()));
                            status = FriendResultStatus.NOT_FRIEND;
                        })
                        .addOnFailureListener(e -> {
                            Toast toast = Toast.makeText(getActivity(), "Xoá lời mời kết bạn thất bại", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            Log.d("RejectFrRequest", e.getMessage());
                        });
                break;
        }
    }


    private void init() {
        db = FirebaseFirestore.getInstance();
        user = new User();
        users = new ArrayList<>();
        if (getArguments() != null) {
            user = (User) Parcels.unwrap(getArguments().getParcelable("user"));
            DocumentReference userFriend = db.collection(CollectionConst.COLLECTION_USER).document(user.getUid());
            users.addAll(Arrays.asList(userFriend, ComponentsUtil.getCurrentUserRef(db)));
        }
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        Point size = new Point();
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        window.setLayout((int) (size.x * 0.9), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AddFriendActivity.SEND_FRIEND_REQUEST_RESULT_CODE && resultCode == getActivity().RESULT_OK) {
            btnAddFriend.setText("Huỷ kết bạn");
            btnAddFriend.setBackgroundColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
            status = FriendResultStatus.PENDING_FRIEND_REQUEST;
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }
}
