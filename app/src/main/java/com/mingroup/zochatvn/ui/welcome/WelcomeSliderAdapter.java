package com.mingroup.zochatvn.ui.welcome;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.mingroup.zochatvn.R;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public class WelcomeSliderAdapter extends RecyclerView.Adapter<WelcomeSliderAdapter.WelcomeSliderViewHolder> {
    private ViewPager2 vpSlider;
    private List<WelcomeSliderItem> sliderItemList;

    @NonNull
    @Override
    public WelcomeSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WelcomeSliderViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_inapp_welcome_slider, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull WelcomeSliderViewHolder holder, int position) {
        WelcomeSliderItem item = sliderItemList.get(position);
        holder.setWelcomeSliderViewHolder(item);
    }

    @Override
    public int getItemCount() {
        return sliderItemList.size();
    }


    public class WelcomeSliderViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView title, description;

        public WelcomeSliderViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.inapp_welcome_image);
            title = itemView.findViewById(R.id.inapp_welcome_title);
            description = itemView.findViewById(R.id.inapp_welcome_description);
        }

        public void setWelcomeSliderViewHolder(WelcomeSliderItem item) {
            image.setImageResource(item.getImage());
            title.setText(item.getTitle());
            description.setText(item.getDescription());
        }
    }
}
