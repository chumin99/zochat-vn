package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ExoPlayerLibraryInfo;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.constant.StorageRefConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.MessageType;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.response.MessageResponse;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.ChatItemType;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.FileMessageViewHolder;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.ImageMessageViewHolder;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.OnMessageClickListener;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.StickerMessageViewHolder;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.TextMessageViewHolder;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.VideoMessageViewHolder;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.file.ZCFileUploader;

import lombok.SneakyThrows;

import static com.mingroup.zochatvn.constant.StorageRefConst.FOLDER_CONSERVATION_PHOTO;
import static com.mingroup.zochatvn.constant.UserSession.AUTHORIZATION;
import static com.mingroup.zochatvn.constant.UserSession.BEARER;
import static com.mingroup.zochatvn.util.StringUtil.formatChatDateTime;
import static com.mingroup.zochatvn.util.StringUtil.formatChatTime;
import static com.mingroup.zochatvn.util.StringUtil.getDateDiff;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = MessageAdapter.class.getSimpleName();
    private Context context;
    private List<MessageResponse> messageList;
    private OnMessageClickListener onChatItemListener;
    private AsyncListDiffer<MessageResponse> mDiffer;
    private String currentUserUid;
    private StorageReference mStorageRef;
    private FirebaseFirestore db;
    private ZCFileUploader fileUploader;
    private DiffUtil.ItemCallback<MessageResponse> itemCallback = new DiffUtil.ItemCallback<MessageResponse>() {
        @Override
        public boolean areItemsTheSame(@NonNull MessageResponse oldItem, @NonNull MessageResponse newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull MessageResponse oldItem, @NonNull MessageResponse newItem) {
            return oldItem.getContent().equals(newItem.getContent());
        }
    };

    public MessageAdapter(Context context, List<MessageResponse> messageList, OnMessageClickListener onChatItemListener) {
        this.context = context;
        this.currentUserUid = ComponentsUtil.currentUserUid;
        this.messageList = messageList;
        this.mDiffer = new AsyncListDiffer<>(this, itemCallback);
        this.onChatItemListener = onChatItemListener;
        this.db = FirebaseFirestore.getInstance();
        this.fileUploader = new ZCFileUploader(context);
    }

    private SimpleExoPlayer player;
    private boolean playWhenReady = true;
    private int currentWindow = 0;
    private long playbackPosition = 0;

    @Override
    public int getItemViewType(int position) {
        MessageResponse message = getItem(position);
        if (message.getSenderInfo().getId().equals(currentUserUid)) { // right
            switch (message.getType()) {
                case TEXT:
                    return ChatItemType.CHAT_ITEM_RIGHT_TEXT.getTypeId();
                case IMAGE:
                    return ChatItemType.CHAT_ITEM_RIGHT_IMAGE.getTypeId();
                case STICKER:
                    return ChatItemType.CHAT_ITEM_RIGHT_STICKER.getTypeId();
                case VIDEO:
                    return ChatItemType.CHAT_ITEM_RIGHT_VIDEO.getTypeId();
                case FILE:
                    return ChatItemType.CHAT_ITEM_RIGHT_FILE.getTypeId();
            }
        } else { // left
            switch (message.getType()) {
                case TEXT:
                    return ChatItemType.CHAT_ITEM_LEFT_TEXT.getTypeId();
                case IMAGE:
                    return ChatItemType.CHAT_ITEM_LEFT_IMAGE.getTypeId();
                case STICKER:
                    return ChatItemType.CHAT_ITEM_LEFT_STICKER.getTypeId();
                case VIDEO:
                    return ChatItemType.CHAT_ITEM_LEFT_VIDEO.getTypeId();
                case FILE:
                    return ChatItemType.CHAT_ITEM_LEFT_FILE.getTypeId();
            }
        }
        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        ChatItemType chatItemType = ChatItemType.getChatItemType(viewType);
        switch (chatItemType) {
            case CHAT_ITEM_RIGHT_TEXT:
                view = layoutInflater.inflate(R.layout.item_chat_message_right_text, parent, false);
                return new TextMessageViewHolder(view, onChatItemListener, false);
            case CHAT_ITEM_LEFT_TEXT:
                view = layoutInflater.inflate(R.layout.item_chat_message_left_text, parent, false);
                return new TextMessageViewHolder(view, onChatItemListener, true);
            case CHAT_ITEM_RIGHT_IMAGE:
                view = layoutInflater.inflate(R.layout.item_chat_message_right_media, parent, false);
                return new ImageMessageViewHolder(view, onChatItemListener, false);
            case CHAT_ITEM_LEFT_IMAGE:
                view = layoutInflater.inflate(R.layout.item_chat_message_left_media, parent, false);
                return new ImageMessageViewHolder(view, onChatItemListener, true);
            case CHAT_ITEM_RIGHT_STICKER:
                view = layoutInflater.inflate(R.layout.item_chat_message_right_media, parent, false);
                return new StickerMessageViewHolder(view, onChatItemListener, false);
            case CHAT_ITEM_LEFT_STICKER:
                view = layoutInflater.inflate(R.layout.item_chat_message_left_media, parent, false);
                return new StickerMessageViewHolder(view, onChatItemListener, true);
            case CHAT_ITEM_RIGHT_VIDEO:
                view = layoutInflater.inflate(R.layout.item_chat_message_right_media, parent, false);
                return new VideoMessageViewHolder(view, onChatItemListener, false);
            case CHAT_ITEM_LEFT_VIDEO:
                view = layoutInflater.inflate(R.layout.item_chat_message_left_media, parent, false);
                return new VideoMessageViewHolder(view, onChatItemListener, true);
            case CHAT_ITEM_RIGHT_FILE:
                view = layoutInflater.inflate(R.layout.item_chat_message_right_media, parent, false);
                return new FileMessageViewHolder(view, onChatItemListener, false);
            case CHAT_ITEM_LEFT_FILE:
                view = layoutInflater.inflate(R.layout.item_chat_message_left_media, parent, false);
                return new FileMessageViewHolder(view, onChatItemListener, true);
            default:
                throw new IllegalStateException("Unexpected value: " + viewType);
        }
    }

    @SneakyThrows
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MessageResponse messageItem = getItem(position);
        ChatItemType chatItemType = ChatItemType.getChatItemType(getItemViewType(position));
        boolean isBreakTimestamp = false;

        if (getItemCount() > 1 && position > 0) {
            MessageResponse previousMessage = getItem(position - 1);
            if (getDateDiff(previousMessage.getCreatedAt(), messageItem.getCreatedAt(), TimeUnit.DAYS) >= 1) {
                isBreakTimestamp = true;
            }
        } else if (position == 0) {
            isBreakTimestamp = true;
        }

        switch (chatItemType) {
            case CHAT_ITEM_RIGHT_TEXT:
                displayTextMessage((TextMessageViewHolder) holder, messageItem, false, isBreakTimestamp);
                break;
            case CHAT_ITEM_LEFT_TEXT:
                displayTextMessage((TextMessageViewHolder) holder, messageItem, true, isBreakTimestamp);
                break;
            case CHAT_ITEM_RIGHT_IMAGE:
                displayImageMessage((ImageMessageViewHolder) holder, messageItem, false, isBreakTimestamp);
                break;
            case CHAT_ITEM_LEFT_IMAGE:
                displayImageMessage((ImageMessageViewHolder) holder, messageItem, true, isBreakTimestamp);
                break;
            case CHAT_ITEM_RIGHT_STICKER:
                displayStickerMessage((StickerMessageViewHolder) holder, messageItem, false, isBreakTimestamp);
                break;
            case CHAT_ITEM_LEFT_STICKER:
                displayStickerMessage((StickerMessageViewHolder) holder, messageItem, true, isBreakTimestamp);
                break;
            case CHAT_ITEM_RIGHT_FILE:
                displayFileMessage((FileMessageViewHolder) holder, messageItem, false, isBreakTimestamp);
                break;
            case CHAT_ITEM_LEFT_FILE:
                displayFileMessage((FileMessageViewHolder) holder, messageItem, true, isBreakTimestamp);
                break;
            case CHAT_ITEM_RIGHT_VIDEO:
                displayVideoMessage((VideoMessageViewHolder) holder, messageItem, false, isBreakTimestamp);
                break;
            case CHAT_ITEM_LEFT_VIDEO:
                displayVideoMessage((VideoMessageViewHolder) holder, messageItem, true, isBreakTimestamp);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + chatItemType);
        }
        //holder.setIsRecyclable(false);
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d(TAG, "onLongClick");
                return true;
            }
        });
    }

    @SneakyThrows
    private void displayVideoMessage(VideoMessageViewHolder holder, MessageResponse messageItem, boolean fromLeft, boolean isBreakTimestamp) {
        holder.getTvTimestamp().setText(formatChatTime(messageItem.getCreatedAt()));

        String videoUrl = messageItem.getContent();
        PlayerView playerView = holder.getPvVideoContent();

        if (videoUrl.contains(context.getString(R.string.google_storage_bucket))) { // uploaded
            initializePlayer(playerView, messageItem.getContent());
        } else {
            holder.getProgressBar().setVisibility(View.VISIBLE);
            holder.getPvVideoContent().setAlpha(.3f);

            fileUploader.uploadFiles(FOLDER_CONSERVATION_PHOTO, Collections.singletonList(Uri.parse(videoUrl)), new ZCFileUploader.FileUploaderCallback() {
                @Override
                public void onError(Exception e) {
                    Log.d(TAG, "::onError: " + e.getMessage());
                }

                @Override
                public void onFinish(List<Uri> responses) {
                    holder.getProgressBar().setVisibility(View.GONE);
                    holder.getPvVideoContent().setAlpha(1f);

                    String uploadedUrl = responses.get(0).toString();
                    db.collection(CollectionConst.COLLECTION_MESSAGE)
                            .document(messageItem.getId())
                            .update("content", uploadedUrl)
                            .addOnSuccessListener(aVoid -> {
                                initializePlayer(playerView, uploadedUrl);
                            })
                            .addOnFailureListener(e -> {
                                Log.d(TAG, e.getMessage());
                            });
                }

                @Override
                public void onProgressUpdate(long currentpercent, long totalpercent, long filenumber) {
                    holder.getProgressBar().setProgress((int) currentpercent);
                }
            });
        }

        if (fromLeft) {
            messageItem.getSenderInfo()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        User fromUser = documentSnapshot.toObject(User.class);
                        if (fromUser != null) {
                            ComponentsUtil.displayImageFromApi(holder.getIvAvatar(), fromUser.getPhotoUrl());
                            holder.getTvDisplayName().setText(fromUser.getDisplayName());
                        }
                    })
                    .addOnFailureListener(e -> Log.d(TAG, "getSenderInfo::onFailure: " + e.getMessage()));

        }

        if (isBreakTimestamp) {
            holder.getTvBreakTimeStamp().setVisibility(View.VISIBLE);
            holder.getTvBreakTimeStamp().setText(formatChatDateTime(messageItem.getCreatedAt()));
        }
    }

    @SneakyThrows
    private void displayFileMessage(FileMessageViewHolder holder, MessageResponse messageItem, boolean fromLeft, boolean isBreakTimestamp) {
        holder.getTvTimestamp().setText(formatChatTime(messageItem.getCreatedAt()));

        if (fromLeft) {
            messageItem.getSenderInfo()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        User fromUser = documentSnapshot.toObject(User.class);
                        if (fromUser != null) {
                            ComponentsUtil.displayImageFromApi(holder.getIvAvatar(), fromUser.getPhotoUrl());
                            holder.getTvDisplayName().setText(fromUser.getDisplayName());
                        }
                    })
                    .addOnFailureListener(e -> Log.d(TAG, "getSenderInfo::onFailure: " + e.getMessage()));
        }

        if (isBreakTimestamp) {
            holder.getTvBreakTimeStamp().setVisibility(View.VISIBLE);
            holder.getTvBreakTimeStamp().setText(formatChatDateTime(messageItem.getCreatedAt()));
        }
    }

    @SneakyThrows
    private void displayStickerMessage(StickerMessageViewHolder holder, MessageResponse messageItem, boolean fromLeft, boolean isBreakTimestamp) {
        mStorageRef = FirebaseStorage.getInstance().getReference();

        holder.getTvTimestamp().setText(formatChatTime(messageItem.getCreatedAt()));

        mStorageRef.child("zochat-sticker")
                .child(messageItem.getContent())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    holder.getTvDisplayError().setVisibility(View.GONE);
                    ComponentsUtil.displayGif(holder.getIvContent(), uri.toString());
                })
                .addOnFailureListener(e -> {
                    holder.getTvDisplayError().setVisibility(View.VISIBLE);
                    Log.d(TAG, e.getMessage());
                });

        if (fromLeft) {
            messageItem.getSenderInfo()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        User fromUser = documentSnapshot.toObject(User.class);
                        if (fromUser != null) {
                            ComponentsUtil.displayImageFromApi(holder.getIvAvatar(), fromUser.getPhotoUrl());
                            holder.getTvDisplayName().setText(fromUser.getDisplayName());
                        }
                    })
                    .addOnFailureListener(e -> Log.d(TAG, "getSenderInfo::onFailure: " + e.getMessage()));
        }

        if (isBreakTimestamp) {
            holder.getTvBreakTimeStamp().setVisibility(View.VISIBLE);
            holder.getTvBreakTimeStamp().setText(formatChatDateTime(messageItem.getCreatedAt()));
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SneakyThrows
    private void displayImageMessage(ImageMessageViewHolder holder, MessageResponse messageItem, boolean fromLeft, boolean isBreakTimestamp) {
        holder.getTvTimestamp().setText(formatChatTime(messageItem.getCreatedAt()));

        String[] uploadUrls = messageItem.getContent().split(";");

        if (uploadUrls.length == 1) { // single image
            String imageUrl = uploadUrls[0];
            if (imageUrl.contains(context.getString(R.string.google_storage_bucket))) { // uploaded
                ComponentsUtil.displayImageFromApi(holder.getIvContent(), imageUrl);
            } else {
                holder.getProgressBar().setVisibility(View.VISIBLE);
                holder.getIvContent().setAlpha(.3f);

                fileUploader.uploadFiles(FOLDER_CONSERVATION_PHOTO, Collections.singletonList(Uri.parse(imageUrl)), new ZCFileUploader.FileUploaderCallback() {
                    @Override
                    public void onError(Exception e) {
                        Log.d(TAG, "::onError: " + e.getMessage());
                    }

                    @Override
                    public void onFinish(List<Uri> responses) {
                        holder.getProgressBar().setVisibility(View.GONE);
                        holder.getIvContent().setAlpha(1f);

                        if(responses != null && !responses.isEmpty()) {
                            String uploadedUrl = responses.get(0).toString();
                            db.collection(CollectionConst.COLLECTION_MESSAGE)
                                    .document(messageItem.getId())
                                    .update("content", uploadedUrl)
                                    .addOnSuccessListener(aVoid -> {
                                        ComponentsUtil.displayImageFromApi(holder.getIvContent(), uploadedUrl);
                                    })
                                    .addOnFailureListener(e -> {
                                        holder.getTvUploadError().setVisibility(View.VISIBLE);
                                        Log.d(TAG, e.getMessage());
                                    });
                        }
                    }

                    @Override
                    public void onProgressUpdate(long currentpercent, long totalpercent, long filenumber) {
                        holder.getProgressBar().setProgress((int) currentpercent);
                    }
                });
            }
        } else { // multi image
            holder.getGridViewSection().setVisibility(View.VISIBLE);
            holder.getSingleImageSection().setVisibility(View.GONE);

            ImageGvAdapter imageGvAdapter = new ImageGvAdapter(context, messageItem, uploadUrls);
            holder.getGvImages().setAdapter(imageGvAdapter);
        }

        if (fromLeft) {
            messageItem.getSenderInfo()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        User fromUser = documentSnapshot.toObject(User.class);
                        if (fromUser != null) {
                            ComponentsUtil.displayImageFromApi(holder.getIvAvatar(), fromUser.getPhotoUrl());
                            holder.getTvDisplayName().setText(fromUser.getDisplayName());
                        }
                    })
                    .addOnFailureListener(e -> Log.d(TAG, "getSenderInfo::onFailure: " + e.getMessage()));
        }

        if (isBreakTimestamp) {
            holder.getTvBreakTimeStamp().setVisibility(View.VISIBLE);
            holder.getTvBreakTimeStamp().setText(formatChatDateTime(messageItem.getCreatedAt()));
        }
    }

    @SneakyThrows
    private void displayTextMessage(TextMessageViewHolder textMessageViewHolder, MessageResponse messageItem, boolean fromLeft, boolean isBreakTimestamp) {
        textMessageViewHolder.getTvContent().setText(messageItem.getContent());
        textMessageViewHolder.getTvTimestamp().setText(formatChatTime(messageItem.getCreatedAt()));

        if (fromLeft) {
            messageItem.getSenderInfo()
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        User fromUser = documentSnapshot.toObject(User.class);
                        if (fromUser != null) {
                            ComponentsUtil.displayImageFromApi(textMessageViewHolder.getIvAvatar(), fromUser.getPhotoUrl());
                            textMessageViewHolder.getTvDisplayName().setText(fromUser.getDisplayName());
                        }
                    })
                    .addOnFailureListener(e -> Log.d(TAG, "getSenderInfo::onFailure: " + e.getMessage()));
        }

        if (isBreakTimestamp) {
            textMessageViewHolder.getTvBreakTimeStamp().setVisibility(View.VISIBLE);
            textMessageViewHolder.getTvBreakTimeStamp().setText(formatChatDateTime(messageItem.getCreatedAt()));
        }
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
        int position = holder.getAdapterPosition();
        releasePlayer();
        super.onViewRecycled(holder);
    }

    @SneakyThrows
    private void initializePlayer(PlayerView playerView, String videoLink) {
        HttpDataSource.Factory httpDataSourceFactory =
                new DefaultHttpDataSourceFactory(
                        ExoPlayerLibraryInfo.DEFAULT_USER_AGENT,
                        DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                        DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                        /* allowCrossProtocolRedirects= */ true);

        DataSource.Factory dataSourceFactory = () -> {
            HttpDataSource dataSource = httpDataSourceFactory.createDataSource();
            dataSource.setRequestProperty(AUTHORIZATION, BEARER + new SessionManager(context).getTokenId());
            return dataSource;
        };

        player = new SimpleExoPlayer.Builder(context)
                .setMediaSourceFactory(new DefaultMediaSourceFactory(dataSourceFactory))
                .build();

        playerView.setPlayer(player);
        player.setMediaItem(MediaItem.fromUri(videoLink));
        player.setPlayWhenReady(playWhenReady);
        playerView.setOnC
        player.seekTo(currentWindow, playbackPosition);
        player.prepare();
    }


    private void releasePlayer() {
        if (player != null) {
            playWhenReady = player.getPlayWhenReady();
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            player.release();
            player = null;
        }
    }

    public void addNewMessage(@NonNull MessageResponse chatMessage) {
        messageList.add(chatMessage);
        submitList(messageList);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public MessageResponse getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void submitList(List<MessageResponse> newList) {
        mDiffer.submitList(newList);
    }
}
