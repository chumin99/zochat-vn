package com.mingroup.zochatvn.ui.mainscreen.contact;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ValidatorUtil;
import lombok.Getter;

import static com.mingroup.zochatvn.util.ComponentsUtil.getFriendUser;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactListViewHolder> {
    private static final String TAG = ContactListAdapter.class.getSimpleName();
    private List<FriendRequest> contactList;
    private OnContactItemListener onContactItemListener;
    private boolean isMultipleChoiceList;
    private AsyncListDiffer<FriendRequest> mDiffer;

    private DiffUtil.ItemCallback<FriendRequest> mCallback = new DiffUtil.ItemCallback<FriendRequest>() {
        @Override
        public boolean areItemsTheSame(@NonNull FriendRequest oldItem, @NonNull FriendRequest newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull FriendRequest oldItem, @NonNull FriendRequest newItem) {
            return oldItem.getUsers().equals(newItem.getUsers());
        }
    };

    public ContactListAdapter(List<FriendRequest> contactList, OnContactItemListener onContactItemListener, boolean isMultipleChoiceList) {
        this.contactList = contactList;
        this.onContactItemListener = onContactItemListener;
        this.isMultipleChoiceList = isMultipleChoiceList;
        this.sortList(contactList);
        this.mDiffer = new AsyncListDiffer<FriendRequest>(this, mCallback);
    }

    @NonNull
    @Override
    public ContactListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactListViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_main_screen_contact_person_info, parent, false), onContactItemListener, isMultipleChoiceList);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactListViewHolder holder, int position) {
        FriendRequest item = getItem(position);
        Objects.requireNonNull(getFriendUser(item.getUsers()))
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    User friendUser = documentSnapshot.toObject(User.class);
                    ComponentsUtil.displayImageFromApi(holder.getIvAvatar(), friendUser.getPhotoUrl());

                    holder.getTvDisplayName().setText(friendUser.getDisplayName());

                    String firstLetter = friendUser.getDisplayName().substring(0, 1);
                    if (!ValidatorUtil.isAlphabet(firstLetter)) {
                        holder.getTvHeaderTitle().setText("#");
                    } else holder.getTvHeaderTitle().setText(firstLetter);

                    if (position > 0) {
                        int i = position - 1;
                        getFriendUser(getItem(i).getUsers()).get()
                                .addOnSuccessListener(documentSnapshot1 -> {
                                    User nextUserFriend = documentSnapshot1.toObject(User.class);
                                    if (nextUserFriend != null && i < getItemCount() && friendUser.getDisplayName().substring(0, 1).equals(nextUserFriend.getDisplayName().substring(0, 1))) {
                                        holder.getHeaderSection().setVisibility(View.GONE);
                                    }
                                })
                        .addOnFailureListener(e -> Log.d(TAG, e.getMessage()));
                    } else {
                        holder.getDividerLine().setVisibility(View.GONE);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, e.getMessage());
                });
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public FriendRequest getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    private void sortList(List<FriendRequest> friendRequestList) {
        friendRequestList.sort(new Comparator<FriendRequest>() {
            @Override
            public int compare(FriendRequest contactResponse, FriendRequest contactResponse2) {
                AtomicInteger compareInt = new AtomicInteger();
                getFriendUser(contactResponse.getUsers()).get()
                        .addOnSuccessListener(documentSnapshot -> {
                            getFriendUser(contactResponse2.getUsers()).get()
                                    .addOnSuccessListener(documentSnapshot1 -> {
                                        compareInt.set(Objects.requireNonNull(documentSnapshot.toObject(User.class)).getDisplayName()
                                        .compareTo(Objects.requireNonNull(documentSnapshot1.toObject(User.class)).getDisplayName()));
                                    });
                        });
                return compareInt.get();
            }
        });
    }

    public void update(List<FriendRequest> newContactList) {
       sortList(newContactList);
        mDiffer.submitList(newContactList);
    }

    public void removeItem(int position) {
        contactList.remove(getItem(position));
        update(contactList);
    }

    @Getter
    public class ContactListViewHolder extends RecyclerView.ViewHolder {
        public RoundedImageView ivAvatar;
        public TextView tvDisplayName;
        public TextView tvHeaderTitle;
        public LinearLayout headerSection;
        public ImageButton btnChat, btnRemove;
        public View dividerLine;
        public CheckBox cbSelect;

        private OnContactItemListener onContactItemListener;

        public ContactListViewHolder(@NonNull View itemView, OnContactItemListener onContactItemListener, boolean isMultipleChoiceList) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.ivAvatar);
            tvDisplayName = itemView.findViewById(R.id.contact_item_avatar);
            tvHeaderTitle = itemView.findViewById(R.id.headerTitle);
            headerSection = itemView.findViewById(R.id.headerSection);
            dividerLine = itemView.findViewById(R.id.dividerLine);

            this.onContactItemListener = onContactItemListener;
            itemView.setOnClickListener(view -> {
                if (onContactItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        if (isMultipleChoiceList) {
                            cbSelect.setChecked(!cbSelect.isChecked());
                        } else
                            onContactItemListener.onContactItemClick(position);
                    }

                }
            });

            if (isMultipleChoiceList) {
                cbSelect = itemView.findViewById(R.id.cbSelect);
                cbSelect.setVisibility(View.VISIBLE);
                cbSelect.setOnCheckedChangeListener((compoundButton, isChecked) -> {
                    if (onContactItemListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                            onContactItemListener.onCheckboxCheckedChange(position, compoundButton, isChecked);
                    }
                });
            } else {
                btnChat = itemView.findViewById(R.id.btnChat);
                btnRemove = itemView.findViewById(R.id.btnRemove);
                btnChat.setVisibility(View.VISIBLE);
                btnRemove.setVisibility(View.VISIBLE);

                btnChat.setOnClickListener(view -> {
                    if (onContactItemListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                            onContactItemListener.onChatContactItemClick(position);
                    }
                });

                btnRemove.setOnClickListener(view -> {
                    if (onContactItemListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                            onContactItemListener.onRemoveContactItemClick(position);
                    }
                });
            }
        }
    }

    public interface OnContactItemListener {
        void onContactItemClick(int position);

        void onChatContactItemClick(int position);

        void onRemoveContactItemClick(int position);

        void onCheckboxCheckedChange(int position, CompoundButton compoundButton, boolean isChecked);
    }
}
