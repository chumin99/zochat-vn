package com.mingroup.zochatvn.ui.mainscreen.group.addgroup.fragment;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.cpoopc.scrollablelayoutlib.ScrollableHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.ui.mainscreen.contact.ContactListAdapter;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.AddGroupViewModel;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.CombineListViewModel;
import com.mingroup.zochatvn.util.ComponentsUtil;

public class AddGroupContactFragment extends Fragment implements ContactListAdapter.OnContactItemListener, ScrollableHelper.ScrollableContainer {
    private static final String TAG = AddGroupContactFragment.class.getSimpleName();
    private RecyclerView rvContactList;
    private List<FriendRequest> contacts;
    private ContactListAdapter contactListAdapter;
    private AddGroupViewModel addGroupViewModel;
    private CombineListViewModel combineListViewModel;
    private TextView tvAddGroupContactCount;
    private FirebaseFirestore db;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_group_contact, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvContactList = view.findViewById(R.id.rvAddGroupContactList);
        tvAddGroupContactCount = view.findViewById(R.id.tvAddGroupContactCount);
        addGroupViewModel = new ViewModelProvider(requireActivity()).get(AddGroupViewModel.class);
        combineListViewModel = new ViewModelProvider(requireActivity()).get(CombineListViewModel.class);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onStart() {
        super.onStart();
        setupContactList();
        addGroupViewModel.getSelectedUsers().observe(requireActivity(), new Observer<List<DocumentReference>>() {
            @Override
            public void onChanged(List<DocumentReference> users) {
                for (int i = 0; i < rvContactList.getChildCount(); i++) {
                    View view = rvContactList.getChildAt(i);
                    CheckBox cbSelect = view.findViewById(R.id.cbSelect);
                    cbSelect.setChecked(users.contains(ComponentsUtil.getFriendUser(contactListAdapter.getItem(i).getUsers())));
                }
            }
        });
    }

    private void setupContactList() {
        contacts = new ArrayList<>();
        contactListAdapter = new ContactListAdapter(contacts, this, true);
        rvContactList.setAdapter(contactListAdapter);

        db = FirebaseFirestore.getInstance();
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .whereArrayContains("users", ComponentsUtil.getCurrentUserRef(db))
                .whereEqualTo("status", FriendRequestStatus.ACCEPTED)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed.", error);
                        return;
                    }

                    if (value != null) {
                        value.getDocuments().forEach(documentSnapshot -> {
                            FriendRequest contact = documentSnapshot.toObject(FriendRequest.class);
                            if (contact != null) {
                                contact.setId(documentSnapshot.getId());
                                contacts.add(contact);
                            }
                        });
                        contactListAdapter.update(contacts);
                        tvAddGroupContactCount.setText(contacts.size() + " bạn");
                        combineListViewModel.insertList(contacts.stream().map(friendRequest -> ComponentsUtil.getFriendUser(friendRequest.getUsers())).collect(Collectors.toCollection(ArrayList::new)));
                    }
                });
    }

    @Override
    public void onContactItemClick(int position) {

    }

    @Override
    public void onChatContactItemClick(int position) {

    }

    @Override
    public void onRemoveContactItemClick(int position) {

    }

    @Override
    public void onCheckboxCheckedChange(int position, CompoundButton compoundButton, boolean isChecked) {
        DocumentReference selectedUser = ComponentsUtil.getFriendUser(contactListAdapter.getItem(position).getUsers());
        if (compoundButton.isChecked()) {
            addGroupViewModel.addUser(selectedUser);
        } else {
            addGroupViewModel.removeUser(selectedUser);
        }
    }

    @Override
    public View getScrollableView() {
        return view;
    }
}
