package com.mingroup.zochatvn.ui.mainscreen.contact.phonebookfriends;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.firestore.DocumentReference;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.response.PhonebookResponse;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ValidatorUtil;
import lombok.Getter;

public class PhonebookFriendAdapter extends RecyclerView.Adapter<PhonebookFriendAdapter.PhonebookViewHolder> implements Filterable {
    private Context context;
    private List<PhonebookResponse> phonebookList;
    private List<PhonebookResponse> phonebookFilteredList;
    private OnPhonebookFriendListener onPhonebookFriendListener;
    private AsyncListDiffer<PhonebookResponse> mDiffer;
    private DiffUtil.ItemCallback<PhonebookResponse> mCallback = new DiffUtil.ItemCallback<PhonebookResponse>() {
        @Override
        public boolean areItemsTheSame(@NonNull PhonebookResponse oldItem, @NonNull PhonebookResponse newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull PhonebookResponse oldItem, @NonNull PhonebookResponse newItem) {
            return oldItem.getUsers().equals(newItem.getUsers());
        }
    };

    public PhonebookFriendAdapter(Context context, List<PhonebookResponse> phonebookList, OnPhonebookFriendListener onPhonebookFriendListener) {
        this.context = context;
        this.phonebookList = phonebookList;
        this.phonebookFilteredList = phonebookList;
        this.onPhonebookFriendListener = onPhonebookFriendListener;
        this.mDiffer = new AsyncListDiffer<PhonebookResponse>(this, mCallback);
    }

    @NonNull
    @Override
    public PhonebookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PhonebookViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_phonebook_friend, parent, false), onPhonebookFriendListener);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull PhonebookViewHolder holder, int position) {
        if (phonebookList != null) {
            holder.setIsRecyclable(false);
            phonebookList.sort(Comparator.comparing(PhonebookResponse::getContactName));

            PhonebookResponse item = getItem(position);

            DocumentReference userFriendRef = item.getCreatedBy();
            if(item.getStatus() != FriendRequestStatus.NOT_FRIEND) {
                userFriendRef = ComponentsUtil.getFriendUser(item.getUsers());
            }

            Objects.requireNonNull(userFriendRef)
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        User friendUser = documentSnapshot.toObject(User.class);
                        ComponentsUtil.displayImageFromApi(holder.getIvAvatar(), friendUser.getPhotoUrl());
                        holder.getTvDisplayName().setText(item.getContactName());
                        holder.getTvZochatName().setText(friendUser.getDisplayName());

                        String firstLetter = item.getContactName().substring(0, 1);
                        if (!ValidatorUtil.isAlphabet(firstLetter)) {
                            holder.getTvHeaderTitle().setText("#");
                        } else holder.getTvHeaderTitle().setText(firstLetter);
                    });

            if (position > 0) {
                int i = position - 1;
                if (i < phonebookList.size() && item.getContactName().substring(0, 1).equals(phonebookList.get(i).getContactName().substring(0, 1))) {
                    holder.getHeaderSection().setVisibility(View.GONE);
                }
            } else {
                holder.getDividerLine().setVisibility(View.GONE);
            }

            Button btnAddFriend = holder.getBtnAddFriend();
            switch (item.getStatus()) {
                case NOT_FRIEND:
                    holder.getBtnAddFriend().setVisibility(View.VISIBLE);
                    break;
                case ACCEPTED:
                    btnAddFriend.setText("Nhắn tin");
                    btnAddFriend.setBackgroundColor(context.getResources().getColor(R.color.orange_pure_1, context.getTheme()));
                    break;
                case PENDING:
                    if (item.getCreatedBy().getId().equals(ComponentsUtil.currentUserUid)) {
                        btnAddFriend.setText("Huỷ kết bạn");
                        btnAddFriend.setBackgroundColor(context.getResources().getColor(R.color.design_default_color_error, context.getTheme()));
                    } else {
                        btnAddFriend.setText("Đã gửi kết bạn");
                        btnAddFriend.setBackgroundColor(context.getResources().getColor(R.color.orange_pure_1, context.getTheme()));
                    }
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + item.getStatus());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public PhonebookResponse getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void submitList(List<PhonebookResponse> newList) {
        mDiffer.submitList(newList);
    }

    public void removeItem(int position) {
        phonebookList.remove(getItem(position));
        notifyItemRemoved(position);
        submitList(phonebookList);
    }

    private List<PhonebookResponse> getCurrentList() {
        return mDiffer.getCurrentList();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if (charSequence == null || charSequence.length() == 0) {
                    filterResults.count = phonebookFilteredList.size();
                    filterResults.values = phonebookFilteredList;
                } else {
                    String strQuery = charSequence.toString().toLowerCase().trim();
                    List<PhonebookResponse> resultData = new ArrayList<>();
                    for (PhonebookResponse item : phonebookFilteredList) {
                        DocumentReference userRef = item.getCreatedBy();
                        if(item.getStatus() == FriendRequestStatus.NOT_FRIEND) {
                            userRef = ComponentsUtil.getFriendUser(item.getUsers());
                        }

                        if(item.getContactName().toLowerCase().trim().contains(strQuery)) {
                            resultData.add(item);
                        }
                    }

                    filterResults.count = resultData.size();
                    filterResults.values = resultData;
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                phonebookList = (List<PhonebookResponse>) filterResults.values;
                submitList(phonebookList);
            }
        };
    }

    @Getter
    public class PhonebookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public RoundedImageView ivAvatar;
        public TextView tvDisplayName, tvZochatName, tvHeaderTitle;
        public LinearLayout headerSection;
        public View dividerLine;
        public MaterialButton btnAddFriend;

        private OnPhonebookFriendListener onPhonebookFriendListener;

        public PhonebookViewHolder(@NonNull View itemView, OnPhonebookFriendListener onPhonebookFriendListener) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.ivAvatar);
            tvDisplayName = itemView.findViewById(R.id.phonebook_display_name);
            tvZochatName = itemView.findViewById(R.id.phonebook_zochat_name);
            tvHeaderTitle = itemView.findViewById(R.id.headerTitle);
            headerSection = itemView.findViewById(R.id.headerSection);
            dividerLine = itemView.findViewById(R.id.dividerLine);
            btnAddFriend = itemView.findViewById(R.id.btnAddFriend);

            this.onPhonebookFriendListener = onPhonebookFriendListener;
            itemView.setOnClickListener(this);

            itemView.setOnLongClickListener(this::onLongClick);

            btnAddFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onPhonebookFriendListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                            onPhonebookFriendListener.onPhonebookFriendAddFriendClick(view, position);
                    }
                }
            });
        }

        @Override
        public void onClick(View view) {
            onPhonebookFriendListener.onPhonebookResponseClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            return onPhonebookFriendListener.onPhonebookResponseLongClick(getAdapterPosition());
        }
    }

    public interface OnPhonebookFriendListener {
        void onPhonebookResponseClick(int position);

        void onPhonebookFriendAddFriendClick(View view, int position);

        boolean onPhonebookResponseLongClick(int position);
    }
}
