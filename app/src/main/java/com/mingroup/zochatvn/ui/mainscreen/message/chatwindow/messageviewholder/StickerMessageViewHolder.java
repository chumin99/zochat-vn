package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import com.mingroup.zochatvn.R;
import lombok.Getter;

@Getter
public class StickerMessageViewHolder extends RecyclerView.ViewHolder {
    private RoundedImageView ivAvatar;
    private TextView tvDisplayName, tvTimestamp, tvBreakTimeStamp, tvDisplayError;

    private ImageView ivContent;

    private OnMessageClickListener onMessageClickListener;


    public StickerMessageViewHolder(@NonNull View itemView, OnMessageClickListener onMessageClickListener, boolean fromLeft) {
        super(itemView);

        tvTimestamp = itemView.findViewById(R.id.media_message_item_timestamp);
        tvBreakTimeStamp = itemView.findViewById(R.id.break_timestamp);
        ivContent = itemView.findViewById(R.id.media_message_item_sticker);
        View stickerSection = itemView.findViewById(R.id.media_message_sticker);
        tvDisplayError = itemView.findViewById(R.id.tv_error_message_text);
        stickerSection.setVisibility(View.VISIBLE);

        this.onMessageClickListener = onMessageClickListener;

        if (fromLeft) {
            ivAvatar = itemView.findViewById(R.id.media_message_item_avatar);
            tvDisplayName = itemView.findViewById(R.id.media_message_item_display_name);
        }

        itemView.setOnClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemClickListener(position);
                }
            }
        });

        itemView.setOnLongClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemLongClickListener(position);
                    return true;
                }
            }
            return false;
        });

        tvDisplayError.setOnClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onTrySendMessageAgain(position);
                }
            }
        });
    }
}
