package com.mingroup.zochatvn.ui.forgetpassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.ui.mainscreen.MainScreenActivity;
import com.mingroup.zochatvn.util.FragmentUtil;

public class ForgetPasswordStepThreeFragment extends Fragment {
    private Button btnLogin, btnLater;
    private RelativeLayout relativeLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forget_password_step_three, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        relativeLayout = view.findViewById(R.id.forget_password_step_three_rl);
        ((ForgetPasswordActivity) getActivity()).getSupportActionBar().setTitle("Tạo mật khẩu mới");

        btnLogin = view.findViewById(R.id.btnGoToLoginAfterReset);
        btnLater = view.findViewById(R.id.btnDoLater);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // chuyển sang ChangePasswordFragment
                FragmentUtil.replaceFragment(getActivity(), R.id.forgot_password_fragment_container, new ForgetPasswordStepFourFragment(),
                        null, ForgetPasswordStepThreeFragment.class.getSimpleName(), false);
            }
        });

        btnLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplicationContext(), MainScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
