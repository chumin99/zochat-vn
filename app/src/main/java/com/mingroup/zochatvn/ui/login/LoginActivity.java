package com.mingroup.zochatvn.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.ui.forgetpassword.ForgetPasswordActivity;
import com.mingroup.zochatvn.ui.mainscreen.MainScreenActivity;

import lombok.SneakyThrows;

import static com.mingroup.zochatvn.util.StringUtil.generateEmailFromPhone;
import static com.mingroup.zochatvn.util.ValidatorUtil.isValidEmail;

public class LoginActivity extends AppCompatActivity {
    private Toolbar zcToolBar;
    private TextView btnForgetPassword, tvDisplayError;
    private FloatingActionButton fabLogin;
    private TextInputLayout tipPhoneOrEmail, tipPassword;
    private FirebaseAuth zcAuth;
    private String bodyText;
    private String strPhoneOrEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        tipPhoneOrEmail.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateForm(charSequence.toString(), tipPassword.getEditText().getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateForm(charSequence.toString(), tipPassword.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tipPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateForm(tipPhoneOrEmail.getEditText().getText().toString(), charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateForm(tipPhoneOrEmail.getEditText().getText().toString(), charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void init() {
        btnForgetPassword = findViewById(R.id.btnForgotPassword);
        fabLogin = findViewById(R.id.fabLogin);
        fabLogin.setAlpha(.2f);
        tipPassword = findViewById(R.id.tipPassword);
        tipPhoneOrEmail = findViewById(R.id.tipPhoneOrEmail);
        tvDisplayError = findViewById(R.id.tvDisplayError);

        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);
        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.login));

        zcAuth = FirebaseAuth.getInstance();
    }

    public void loginWithPhoneOrEmail(View view) {
        // Login with email/password + phone/password
        tvDisplayError.setText(null);
        strPhoneOrEmail = tipPhoneOrEmail.getEditText().getText().toString().trim();
        if (!isValidEmail(strPhoneOrEmail)) {
            strPhoneOrEmail = generateEmailFromPhone(strPhoneOrEmail);
        }
        String strPassword = tipPassword.getEditText().getText().toString().trim();
        zcAuth.signInWithEmailAndPassword(strPhoneOrEmail, strPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @SneakyThrows
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            navigateToMainScreen();
                        } else {
                            if(task.getException() instanceof FirebaseAuthInvalidUserException) {
                                tvDisplayError.setText("Tài khoản không tồn tại hoặc đã bị khoá!");
                            } else {
                                tvDisplayError.setText(getString(R.string.login_failed_warning));
                            }
                        }
                    }
                });
    }

    public void navigateToMainScreen() {
        Intent intent = new Intent(getApplicationContext(), MainScreenActivity.class);
        startActivity(intent);
        finish();
    }

    public void navigateToForgetPassword(View view) {
        Intent intent = new Intent(getApplicationContext(), ForgetPasswordActivity.class);
        startActivity(intent);
    }

    private boolean validateForm(String phoneOrEmail, String password) {
        boolean valid = true;

        if (!phoneOrEmail.isEmpty() && !password.isEmpty()) {
            fabLogin.setClickable(true);
            fabLogin.setAlpha(1f);
        } else {
            fabLogin.setClickable(false);
            fabLogin.setAlpha(.2f);
            valid = false;
        }

        return valid;
    }
}
