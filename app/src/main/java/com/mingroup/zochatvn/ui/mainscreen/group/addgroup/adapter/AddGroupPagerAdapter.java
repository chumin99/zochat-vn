package com.mingroup.zochatvn.ui.mainscreen.group.addgroup.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.fragment.AddGroupContactFragment;
import com.mingroup.zochatvn.ui.mainscreen.group.addgroup.fragment.AddGroupRecentFragment;

public class AddGroupPagerAdapter extends FragmentPagerAdapter {

    public AddGroupPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new AddGroupRecentFragment();
            default:
                return new AddGroupContactFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 1:
                return "  Gần đây   ";
            default:
                return "   Danh bạ   ";
        }
    }
}
