package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.sj.emoji.EmojiBean;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.constant.ZCKeyboardConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.MessageType;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.response.MessageResponse;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.chatoptions.ChatOptionsActivity;
import com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder.OnMessageClickListener;
import com.mingroup.zochatvn.util.StringUtil;
import com.mingroup.zochatvn.util.file.FileUtils;
import com.mingroup.zochatvn.util.zcKeyboard.AppsGridView;
import com.mingroup.zochatvn.util.zcKeyboard.ChattingAppsAdapter;
import com.mingroup.zochatvn.util.zcKeyboard.ZCEmoticonsKeyBoard;
import com.mingroup.zochatvn.util.zcKeyboard.ZCKeyboardUtils;

import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.type.MediaType;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.widget.EmoticonsEditText;
import sj.keyboard.widget.FuncLayout;


public class ChatActivity extends AppCompatActivity implements
        FuncLayout.OnFuncKeyBoardListener,
        ZCEmoticonsKeyBoard.OnChatKeyboardListener,
        OnMessageClickListener,
        ChattingAppsAdapter.OnFuncAppClickListener {

    private static final int REQUEST_CODE_FILE = 333;

    enum ConservationType {
        CREATED,
        HAVE_NOT_CREATED_YET
    }

    private static final String TAG = ChatActivity.class.getSimpleName();
    private Toolbar zcToolBar;
    private ZCEmoticonsKeyBoard chatKeyBoard;
    private List<Uri> mSelected;
    private RecyclerView rvChatList;
    private List<MessageResponse> messages;
    private MessageAdapter messageAdapter;
    private Conservation conservation;
    private User chatFriend;
    private SessionManager sessionManager;
    private ConservationType conservationType;
    private FirebaseFirestore db;
    private StorageReference mStorageRef;
    private ListenerRegistration chatListenerRegistration;
    private ArrayList<Uri> mDocumentSelected;

    private EmoticonClickListener emoticonClickListener = new EmoticonClickListener() {
        @Override
        public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {
            if (isDelBtn) {
                int action = KeyEvent.ACTION_DOWN;
                int code = KeyEvent.KEYCODE_DEL;
                KeyEvent event = new KeyEvent(action, code);
                chatKeyBoard.getEtChat().onKeyDown(KeyEvent.KEYCODE_DEL, event);
            } else {
                if (o == null) {
                    return;
                }

                if (actionType == ZCKeyboardConst.EMOTICON_CLICK_BIGIMAGE) {
                    if (o instanceof EmoticonEntity) {
                        onSendSticker(((EmoticonEntity) o).getIconUri());
                    }
                } else {
                    String content = null;
                    if (o instanceof EmojiBean) {
                        content = ((EmojiBean) o).emoji;
                    }

                    if (TextUtils.isEmpty(content)) {
                        return;
                    }

                    int index = chatKeyBoard.getEtChat().getSelectionStart();
                    Editable editable = chatKeyBoard.getEtChat().getText();
                    editable.insert(index, content);
                }
            }
        }
    };

    private void onSendSticker(String iconUri) {
        String stickerName = Uri.parse(iconUri).getLastPathSegment();
        doSendMessage(stickerName, MessageType.STICKER);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        init();
        setupToolbar();
        setupChatList();
        setupKeyboard();
        onTypingInputText();
    }

    private void onTypingInputText() {
        chatKeyBoard.getEtChat().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setupChatList() {
        messages = new ArrayList<>();
        messageAdapter = new MessageAdapter(this, messages, this);
        rvChatList.setAdapter(messageAdapter);
        rvChatList.setHasFixedSize(true);
        rvChatList.setItemAnimator(null);
        rvChatList.setItemViewCacheSize(50);
        if (conservation != null) {
            listenForChatMessages();
        }
    }

    private void listenForChatMessages() {
        DocumentReference conservationRef = db.collection(CollectionConst.COLLECTION_CONSERVATION).document(conservation.getId());
        chatListenerRegistration = db.collection(CollectionConst.COLLECTION_MESSAGE)
                .whereEqualTo("conservation", conservationRef)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        Log.w(TAG, "Listen failed.", error);
                        return;
                    }

                    if (value != null) {
                        messages.clear();
                        for (DocumentSnapshot docSnap : value.getDocuments()) {
                            MessageResponse messageResponse = docSnap.toObject(MessageResponse.class);
                            if (messageResponse != null) {
                                messageResponse.setId(docSnap.getId());
                                switch (messageResponse.getType()) {
                                    case STICKER:
                                    case TEXT:
                                        messages.add(messageResponse);
                                        break;
                                    case VIDEO:
                                    case IMAGE:
                                    case FILE:
                                        if(messageResponse.getSenderInfo().equals(ComponentsUtil.getCurrentUserRef(db))) {
                                            messages.add(messageResponse);
                                        } else {
                                            if(messageResponse.getContent().contains(getResources().getString(R.string.google_storage_bucket))) {
                                                messages.add(messageResponse);
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        messages.sort(Comparator.comparing(MessageResponse::getCreatedAt));
                        messageAdapter.submitList(messages);
                        scrollToBottom();
                    }
                });
    }

    private void setupKeyboard() {
        ZCKeyboardUtils.initEmotionsEditText(chatKeyBoard.getEtChat());
        chatKeyBoard.setAdapter(ZCKeyboardUtils.getCommonAdapter(this, emoticonClickListener));
        chatKeyBoard.addOnFuncKeyBoardListener(this);
        chatKeyBoard.addFuncView(new AppsGridView(this, this));

        chatKeyBoard.getEtChat().setOnSizeChangedListener(new EmoticonsEditText.OnSizeChangedListener() {
            @Override
            public void onSizeChanged(int w, int h, int oldw, int oldh) {

            }
        });

        chatKeyBoard.getEmoticonsToolBarView().addFixedToolItemView(false, R.drawable.icon_add, null, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ChatActivity.this, "Add", Toast.LENGTH_SHORT).show();
            }
        });

        chatKeyBoard.getEmoticonsToolBarView().addToolItemView(R.drawable.icon_setting, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ChatActivity.this, "Setting", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (EmoticonsKeyboardUtils.isFullScreen(this)) {
            boolean isConsum = chatKeyBoard.dispatchKeyEventInFullScreen(event);
            return isConsum ? isConsum : super.dispatchKeyEvent(event);
        }
        return super.dispatchKeyEvent(event);
    }

    private void setupToolbar() {
        if (conservation != null) {
            if(conservation.getType() == com.mingroup.zochatvn.data.model.ConservationType.GROUP) {
                setToolbarUserInfo(conservation.getTitle(), conservation.getMembers().size());
            } else {
                ComponentsUtil.getFriendUser(conservation.getMembers())
                        .get()
                        .addOnSuccessListener(documentSnapshot -> {
                            User userFriend = documentSnapshot.toObject(User.class);
                            setToolbarUserInfo(userFriend.getDisplayName(), 0);
                        })
                        .addOnFailureListener(e -> Log.d(TAG, "getFriendUser::onFailure: " + e.getMessage()));
            }
        } else {
            if (chatFriend != null) {
                setToolbarUserInfo(chatFriend.getDisplayName(), 0);
            }
        }
    }

    private void setToolbarUserInfo(String title, int membersCount) {
        zcToolBar.setTitle(title == null ? "Nhóm không tên" : title);
        zcToolBar.setSubtitle(membersCount == 0 ? "Đang hoạt động" : membersCount + " thành viên");
        zcToolBar.setSubtitleTextAppearance(this, android.R.style.TextAppearance_Material_Caption);
        zcToolBar.setSubtitleTextColor(getColor(R.color.color_white_transparency_80));
        setSupportActionBar(zcToolBar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_conservation_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
            case R.id.chat_add_member:
                Toast.makeText(this, "Add Member here", Toast.LENGTH_SHORT).show();
                break;
            case R.id.chat_search_message:
                Toast.makeText(this, "Search message", Toast.LENGTH_SHORT).show();
                break;
            case R.id.chat_more_info:
                Intent intent = new Intent(ChatActivity.this, ChatOptionsActivity.class);
                intent.putExtra("conservation", Parcels.wrap(conservation));
                if (conservation.getType() == com.mingroup.zochatvn.data.model.ConservationType.SINGLE) {
                    intent.putExtra("chat_friend", Parcels.wrap(chatFriend));
                }
                startActivity(intent);
                break;
        }
        return true;
    }

    private void init() {
        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);
        rvChatList = findViewById(R.id.rvChatList);
        chatKeyBoard = findViewById(R.id.chatKeyboard);
        chatKeyBoard.setOnChatKeyboardListener(this);
        db = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        if (getIntent().getExtras() != null) {
            conservation = Parcels.unwrap(getIntent().getParcelableExtra("conservation"));
            chatFriend = Parcels.unwrap(getIntent().getParcelableExtra("chat_friend"));

            if (conservation != null) {
                conservationType = ConservationType.CREATED;
            } else {
                if (chatFriend != null) {
                    DocumentReference chatFriendRef = db.collection(CollectionConst.COLLECTION_USER)
                            .document(chatFriend.getUid());

                    db.collection(CollectionConst.COLLECTION_CONSERVATION)
                            .whereArrayContains("members", chatFriendRef)
                            .get()
                            .addOnSuccessListener(queryDocumentSnapshots -> {
                                if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                                    conservation = queryDocumentSnapshots.toObjects(Conservation.class).get(0);
                                    conservationType = ConservationType.CREATED;
                                } else {
                                    conservationType = ConservationType.HAVE_NOT_CREATED_YET;
                                }
                            })
                            .addOnFailureListener(e -> Log.d(TAG, "user_get_group::onFailure" + e.getMessage()));
                }
            }
        }

        sessionManager = new SessionManager(this);
    }


    private void scrollToBottom() {
        rvChatList.requestLayout();
        rvChatList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int count = rvChatList.getAdapter().getItemCount();
                if (count > 0) {
                    rvChatList.smoothScrollToPosition(count - 1);
                }
            }
        });
    }

    @Override
    public void OnFuncPop(int i) {

    }

    @Override
    public void OnFuncClose() {

    }

    @Override
    public void onChatImageClickListener(View view) {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        showImagePicker();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(ChatActivity.this, "Cấp quyền bị từ chối\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối, bạn sẽ có thể không thể sử dụng dịch vụ này\n\nXin hãy cấp quyền tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    @Override
    public void onSendButtonClickListener(View view) {
        String message = chatKeyBoard.getEtChat().getText().toString().trim();

        if (!TextUtils.isEmpty(message)) {
            doSendOrCreateGroupAndSend(message, MessageType.TEXT);
        }
    }

    private void doSendOrCreateGroupAndSend(String message, MessageType messageType) {
        if (conservationType == ConservationType.CREATED) {
            doSendMessage(message, messageType);
        } else {
            createNewConservation(message, messageType);
        }
    }

    private void createNewConservation(String message, MessageType messageType) {
        Conservation newConservation = Conservation.createSingleConservation(db, chatFriend.getUid());
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .add(newConservation)
                .addOnSuccessListener(documentReference -> {
                    conservation = newConservation;
                    conservationType = ConservationType.CREATED;
                    listenForChatMessages();
                    doSendMessage(message, messageType);
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(ChatActivity.this, "Tạo phòng chat mới xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "createConservation::onError : " + e.getMessage());
                });
    }

    private void doSendMessage(String message, MessageType messageType) {
        MessageResponse sendMessageRequest = MessageResponse.createNewMessage(db, conservation, message, messageType);
        db.collection(CollectionConst.COLLECTION_MESSAGE)
                .add(sendMessageRequest)
                .addOnSuccessListener(documentReference -> {
                    scrollToBottom();
                    chatKeyBoard.getEtChat().setText("");
                    saveLastMessage(sendMessageRequest);
                })
                .addOnFailureListener(e -> Log.d(TAG, "sendMessage::onFailure: " + e.getMessage()));
    }

    private void saveLastMessage(MessageResponse sendMessageRequest) {
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .document(conservation.getId())
                .update("lastMessage", sendMessageRequest)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "updateLastMessage::onSuccess");
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "updateLastMessage::onFailure: " + e.getMessage());
                });
    }

    private void showImagePicker() {
        chatKeyBoard.reset();
        TedBottomPicker.with(this)
                .setTitle("Chọn ảnh")
                .setPeekHeight(1500)
                .setCompleteButtonText("Xong")
                .setEmptySelectionText("Chưa chọn ảnh")
                .setSelectedUriList(mSelected)
                .showMultiImage(uriList -> {
                    if (uriList.size() > 0) {
                        List<String> imageUrls = new ArrayList<>();

                        for (Uri uri : uriList) {
                            String url = uri.toString();
                            if (FileUtils.isImageFile(url)) {
                                imageUrls.add(url);
                            }
                        }

                        if (!imageUrls.isEmpty()) {
                            String message = StringUtil.convertListToString(imageUrls, ";");
                            doSendOrCreateGroupAndSend(message, MessageType.IMAGE);
                        }
                    }

                });
    }

    @Override
    public void onMessageItemLongClickListener(int position) {
        View actionMessageView = getLayoutInflater().inflate(R.layout.bsd_message_action, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(actionMessageView);
        dialog.show();
        Log.d("onLongClick", "true");
    }

    @Override
    public void onTrySendMessageAgain(int position) {

    }

    @Override
    public void onMessageItemClickListener(int position) {
        Toast.makeText(this, position + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        chatKeyBoard.reset();
    }

    @Override
    public void onVideoFuncClickListener() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        showVideoPicker();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(ChatActivity.this, "Cấp quyền bị từ chối\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
                .setDeniedMessage("Nếu từ chối, bạn sẽ có thể không thể sử dụng dịch vụ này\n\nXin hãy cấp quyền tại [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

    @Override
    public void onFileFuncClickListener() {

    }

    private void showVideoPicker() {
        TedImagePicker.with(this)
                .mediaType(MediaType.VIDEO)
                .start(uri -> {
                    doSendOrCreateGroupAndSend(uri.toString(), MessageType.VIDEO);
                });
    }
}