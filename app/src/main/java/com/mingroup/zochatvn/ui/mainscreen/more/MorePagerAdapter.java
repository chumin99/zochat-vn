package com.mingroup.zochatvn.ui.mainscreen.more;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.mingroup.zochatvn.ui.mainscreen.more.profile.ProfileFragment;
import com.mingroup.zochatvn.ui.mainscreen.more.setting.SettingsFragment;

public class MorePagerAdapter extends FragmentStateAdapter {

    public MorePagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1:
                return new SettingsFragment();
            default:
                return new ProfileFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
