package com.mingroup.zochatvn.ui.splashscreen;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.mingroup.zochatvn.ui.mainscreen.MainScreenActivity;
import com.mingroup.zochatvn.ui.welcome.WelcomeActivity;

public class SplashScreenActivity extends AppCompatActivity {
    private FirebaseAuth zcAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        zcAuth = FirebaseAuth.getInstance();
        if (zcAuth.getCurrentUser() == null) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
        }
        else {
            startActivity(new Intent(this, MainScreenActivity.class));
            finish();
        }
    }
}
