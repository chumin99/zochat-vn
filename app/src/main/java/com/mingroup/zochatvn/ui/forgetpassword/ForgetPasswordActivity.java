package com.mingroup.zochatvn.ui.forgetpassword;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.util.FragmentUtil;

public class ForgetPasswordActivity extends AppCompatActivity {
    private Toolbar zcToolBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        setupToolbar();
        FragmentUtil.replaceFragment(this, R.id.forgot_password_fragment_container, new ForgetPasswordStepOneFragment(),
                null, ForgetPasswordStepOneFragment.class.getSimpleName(), false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setupToolbar() {
        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);
        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.login_forget_password));
    }
}
