package com.mingroup.zochatvn.ui.mainscreen.more.setting.changepassword;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.ui.mainscreen.MainScreenActivity;
import com.mingroup.zochatvn.util.StringUtil;

import static com.mingroup.zochatvn.util.StringUtil.getInputText;
import static com.mingroup.zochatvn.util.ValidatorUtil.isConfirmPasswordMatch;
import static com.mingroup.zochatvn.util.ValidatorUtil.isValidPasswordForChangePassword;

public class ChangePasswordFragment extends Fragment {
    private TextInputLayout tipCurrentPassword, tipNewPassword, tipNewConfirmPassword;
    private Button btnUpdate;
    private FirebaseAuth zcAuth;
    private FirebaseUser currentUser;
    private RelativeLayout relativeLayout;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings_change_password, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle("Đổi mật khẩu mới");
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        validateForm();
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doChangePassword(getInputText(tipCurrentPassword));
            }
        });

    }

    private void validateForm() {
        tipCurrentPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipNewPassword),
                        getInputText(tipNewConfirmPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipNewPassword),
                        getInputText(tipNewConfirmPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tipNewPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipCurrentPassword),
                        getInputText(tipNewConfirmPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipCurrentPassword),
                        getInputText(tipNewConfirmPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isValidPasswordForChangePassword(editable.toString(), currentUser.getDisplayName())) {
                    tipNewPassword.setError("Mật khẩu phải từ 6-32 ký tự, gồm chữ kèm theo số hoặc ký tự đặc biệt");
                } else {
                    tipNewPassword.setError(null);
                }
            }
        });

        tipNewConfirmPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipCurrentPassword),
                        getInputText(tipNewPassword));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateFormNotEmpty(charSequence.toString(),
                        getInputText(tipCurrentPassword),
                        getInputText(tipNewPassword));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!isConfirmPasswordMatch(StringUtil.getInputText(tipNewPassword), editable.toString())) {
                    tipNewConfirmPassword.setError("Mật khẩu không trùng khớp");
                } else {
                    tipNewConfirmPassword.setError(null);
                }
            }
        });
    }

    private void doChangePassword(String currentPassword) {
        AuthCredential authCredential = EmailAuthProvider.getCredential(currentUser.getEmail(), currentPassword);
        currentUser.reauthenticate(authCredential)
                .addOnSuccessListener(aVoid -> currentUser.updatePassword(getInputText(tipNewPassword))
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Snackbar.make(relativeLayout, "Cập nhật mật khẩu thành công", BaseTransientBottomBar.LENGTH_INDEFINITE)
                                            .setAction("Quay về", view -> getActivity().getSupportFragmentManager().popBackStack()).show();
                                } else {
                                    tipCurrentPassword.setError(null);
                                    Snackbar.make(relativeLayout, "Cập nhật mật khẩu thất bại. Hạy thử lại", BaseTransientBottomBar.LENGTH_INDEFINITE)
                                            .setAction("Thử lại", null).show();
                                }
                            }
                        }))
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        tipCurrentPassword.setError("Mật khẩu không đúng");
                    }
                });
    }

    private void init(View view) {
        tipCurrentPassword = view.findViewById(R.id.tipCurrentPassword);
        tipNewPassword = view.findViewById(R.id.tipNewPassword);
        tipNewConfirmPassword = view.findViewById(R.id.tipNewConfirmPassword);
        btnUpdate = view.findViewById(R.id.btnUpdatePassword);
        zcAuth = FirebaseAuth.getInstance();
        currentUser = zcAuth.getCurrentUser();
        relativeLayout = view.findViewById(R.id.change_password_rl);
    }

    private void validateFormNotEmpty(String str1, String str2, String str3) {
        if (!str1.isEmpty() && !str2.isEmpty() && !str3.isEmpty()) {
            btnUpdate.setClickable(true);
            btnUpdate.setAlpha(1f);
        } else {
            btnUpdate.setClickable(false);
            btnUpdate.setAlpha(.2f);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.findItem(R.id.top_bar_action_more).setVisible(false);
        ((MainScreenActivity) getActivity()).findViewById(R.id.search_section).setVisibility(View.GONE);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setTitle("Chỉnh sửa thông tin");
        ((MainScreenActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().getSupportFragmentManager().popBackStack();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

}