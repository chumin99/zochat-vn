package com.mingroup.zochatvn.ui.mainscreen.contact.sentfriendrequest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.util.ComponentsUtil;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.List;
import java.util.Objects;

import lombok.Getter;
import lombok.SneakyThrows;

import static com.mingroup.zochatvn.util.ComponentsUtil.getFriendUser;

public class SentFriendRequestListAdapter extends RecyclerView.Adapter<SentFriendRequestListAdapter.SentFriendRequestViewHolder> {
    private Context context;
    private List<FriendRequest> friendRequestItemList;
    private OnSentFriendRequestItemListener onSentFriendRequestItemListener;
    private PrettyTime prettyTime;
    private AsyncListDiffer<FriendRequest> mDiffer;
    private DiffUtil.ItemCallback<FriendRequest> mCallback = new DiffUtil.ItemCallback<FriendRequest>() {
        @Override
        public boolean areItemsTheSame(@NonNull FriendRequest oldItem, @NonNull FriendRequest newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull FriendRequest oldItem, @NonNull FriendRequest newItem) {
            return oldItem.getUsers().equals(newItem.getUsers());
        }
    };

    public SentFriendRequestListAdapter(Context context, List<FriendRequest> friendRequestItemList, OnSentFriendRequestItemListener OnSentFriendRequestItemListener) {
        this.context = context;
        this.friendRequestItemList = friendRequestItemList;
        this.onSentFriendRequestItemListener = OnSentFriendRequestItemListener;
        this.prettyTime = new PrettyTime(ComponentsUtil.getCurrentLocale(context));
        this.mDiffer = new AsyncListDiffer<>(this, mCallback);
    }

    @NonNull
    @Override
    public SentFriendRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SentFriendRequestViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_sent_friend_request, parent, false
        ), onSentFriendRequestItemListener);
    }

    @SneakyThrows
    @Override
    public void onBindViewHolder(@NonNull SentFriendRequestViewHolder holder, int position) {
        FriendRequest item = getItem(position);

        Objects.requireNonNull(getFriendUser(item.getUsers()))
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    User friendUser = documentSnapshot.toObject(User.class);
                    ComponentsUtil.displayImageFromApi(holder.getAvatar(), friendUser.getPhotoUrl());
                    holder.getTvDisplayName().setText(friendUser.getDisplayName());
                    holder.getTvCreatedTime().setText(prettyTime.format(friendUser.getDob()));

                    String message = item.getMessage();
                    if (message != null && !message.isEmpty())
                        holder.getTvMessage().setText(message);
                    else
                        holder.getCvMessage().setVisibility(View.GONE);

                    if (position == 0) {
                        holder.getDividerLine().setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public FriendRequest getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void update(List<FriendRequest> newContactList) {
        mDiffer.submitList(newContactList);
    }

    @Getter
    public class SentFriendRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RoundedImageView avatar;
        public TextView tvDisplayName;
        public TextView tvCreatedTime;
        public TextView tvMessage;
        public View dividerLine;
        public CardView cvMessage;
        public Button btnUndo;
        private OnSentFriendRequestItemListener onSentFriendRequestItemListener;

        public SentFriendRequestViewHolder(@NonNull View itemView, OnSentFriendRequestItemListener onSentFriendRequestItemListener) {
            super(itemView);
            this.onSentFriendRequestItemListener = onSentFriendRequestItemListener;
            itemView.setOnClickListener(this);

            avatar = itemView.findViewById(R.id.sent_friend_request_avatar);
            tvDisplayName = itemView.findViewById(R.id.sent_friend_request_display_name);
            tvCreatedTime = itemView.findViewById(R.id.sent_friend_request_created_time);
            tvMessage = itemView.findViewById(R.id.sent_friend_request_message);
            dividerLine = itemView.findViewById(R.id.dividerLine);
            cvMessage = itemView.findViewById(R.id.cvSentReqMessage);
            btnUndo = itemView.findViewById(R.id.btnUndo);

            btnUndo.setOnClickListener(view -> {
                if (onSentFriendRequestItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                        onSentFriendRequestItemListener.onSentFriendRequestCancelClick(position);
                }
            });

        }

        @Override
        public void onClick(View view) {
            this.onSentFriendRequestItemListener.onSentFriendRequestItemClick(getAdapterPosition());
        }
    }

    public interface OnSentFriendRequestItemListener {
        void onSentFriendRequestItemClick(int position);

        void onSentFriendRequestCancelClick(int position);
    }
}
