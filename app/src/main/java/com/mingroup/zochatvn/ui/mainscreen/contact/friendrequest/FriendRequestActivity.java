package com.mingroup.zochatvn.ui.mainscreen.contact.friendrequest;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.AddFriendActivity;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

public class FriendRequestActivity extends AppCompatActivity implements FriendRequestListAdapter.OnFriendRequestItemListener {
    private static final String TAG = FriendRequestActivity.class.getSimpleName();
    private Toolbar zcToolBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvFriendRequestList;
    private TextView tvFriendRequestCount;
    private List<FriendRequest> items;
    private FriendRequestListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private LinearLayout noDataSection;
    private ZCLoadingDialog loading;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_request);

        loading = ZCLoadingDialog.create(this).setStyle(ZCLoadingDialog.Style.MODERN_STYLE);
        loading.show();
        init();
        setupActionBar();
        setupFriendRequestListView();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                populateDataToListView();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        populateDataToListView();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return false;
    }

    private void setupFriendRequestListView() {
        layoutManager = new LinearLayoutManager(this);
        adapter = new FriendRequestListAdapter(this, items, this);
        rvFriendRequestList.setAdapter(adapter);
        rvFriendRequestList.setLayoutManager(layoutManager);
    }

    private void populateDataToListView() {
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .whereArrayContains("users", ComponentsUtil.getCurrentUserRef(db))
                .whereNotEqualTo("createdBy", ComponentsUtil.getCurrentUserRef(db))
                .whereEqualTo("status", FriendRequestStatus.PENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null) {
                            Log.d(TAG, "getFriendRequests::onError: " + error.getMessage());
                        }

                        if(value != null) {
                            items.clear();
                            value.getDocuments().forEach(documentSnapshot -> {
                                FriendRequest contact = documentSnapshot.toObject(FriendRequest.class);
                                if (contact != null) {
                                    contact.setId(documentSnapshot.getId());
                                    items.add(contact);
                                }
                            });
                            adapter.update(items);
                        }

                        updateFriendRequestListCount();
                        checkNoDataSection();
                    }
                });
    }

    private void checkNoDataSection() {
        if (adapter.getItemCount() > 0)
            noDataSection.setVisibility(View.GONE);
        else {
            noDataSection.setVisibility(View.VISIBLE);
        }
        loading.dismiss();
    }

    private void updateFriendRequestListCount() {
        tvFriendRequestCount.setText(String.format("Bạn có %s lời mời kết bạn", adapter.getItemCount() + ""));
    }

    private void setupActionBar() {
        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.friend_request));
    }

    private void init() {
        items = new ArrayList<>();
        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);
        rvFriendRequestList = findViewById(R.id.rvFriendRequestList);
        tvFriendRequestCount = findViewById(R.id.tvFriendRequestCount);
        noDataSection = findViewById(R.id.no_friend_request_section);
        swipeRefreshLayout = findViewById(R.id.friend_request_swipelayout);
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onFriendRequestItemClick(int position) {

    }

    @Override
    public void onFriendRequestAcceptClick(int position) {
        loading.show();
        FriendRequest selectedFriendRequest = adapter.getItem(position);

        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .document(selectedFriendRequest.getId())
                .update("status", FriendRequestStatus.ACCEPTED)
                .addOnSuccessListener(aVoid -> {
                    removeItemFromRv(position);
                })
                .addOnFailureListener(e -> {
                    Log.d("AcceptFriendRequest::onError: ", e.getMessage());
                    Toast.makeText(FriendRequestActivity.this, "Có lỗi xảy ra. Xin hãy thử lại!", Toast.LENGTH_SHORT).show();
                });
        loading.dismiss();
    }

    @Override
    public void onFriendRequestRejectClick(int position) {
        loading.show();
        FriendRequest selectedFriendRequest = adapter.getItem(position);
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .document(selectedFriendRequest.getId())
                .delete()
                .addOnSuccessListener(aVoid -> {
                    removeItemFromRv(position);
                })
                .addOnFailureListener(e -> {
                    Log.d("RejectContactItem::onError: ", e.getMessage());
                    Toast.makeText(FriendRequestActivity.this, "Có lỗi xảy ra. Chúng tôi sẽ kiểm tra lại!", Toast.LENGTH_SHORT).show();
                });
        loading.dismiss();
    }

    public void goToAddFriend(View view) {
        loading.show();
        startActivity(new Intent(this, AddFriendActivity.class));
    }

    private void removeItemFromRv(int position) {
        items.remove(position);
        adapter.update(items);
        adapter.notifyItemRemoved(position);
        checkNoDataSection();
        updateFriendRequestListCount();
    }
}
