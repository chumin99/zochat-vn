package com.mingroup.zochatvn.ui.mainscreen.contact.addfriend;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;

import org.parceler.Parcels;

import java.util.List;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.ui.mainscreen.contact.sentfriendrequest.SentFriendRequestActivity;

import static com.mingroup.zochatvn.util.StringUtil.generateEmailFromPhone;
import static com.mingroup.zochatvn.util.StringUtil.getInputText;

public class AddFriendActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int SEND_FRIEND_REQUEST_RESULT_CODE = 112;
    private static final String RESULT_DIALOG_TAG = "FriendResultDialog";
    private TextInputLayout tipAddFriendType, tipAddFriendPhoneOrEmail;
    private Button btnSearchFriend;
    private TableRow btnSentRequest, btnAddFromPhonebookFriend;
    private AutoCompleteTextView autoTvAddFriendType;
    private Toolbar zcToolBar;
    private TextView tvDisplayError;
    private FirebaseFirestore db;
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        init();
        setupTextInputFriendType();
        setupActionBar();
        tipAddFriendPhoneOrEmail.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnSearchFriend.setEnabled(charSequence != null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnSearchFriend.setEnabled(charSequence != null);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                btnSearchFriend.setEnabled(!editable.toString().isEmpty());
            }
        });
    }

    private void setupActionBar() {
        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_friend_title));
    }

    private void setupTextInputFriendType() {
        String[] types = {getString(R.string.email), getString(R.string.phone)};
        adapter = new ArrayAdapter(this, R.layout.dropdown_menu_popup_item, types);
        autoTvAddFriendType.setAdapter(adapter);
        autoTvAddFriendType.setText((CharSequence) adapter.getItem(0), false);
    }

    private void init() {
        tipAddFriendType = findViewById(R.id.tipAddFriendType);
        tipAddFriendPhoneOrEmail = findViewById(R.id.tipAddFriendPhoneOrEmail);
        btnSearchFriend = findViewById(R.id.btnSearchFriend);
        autoTvAddFriendType = findViewById(R.id.filled_exposed_dropdown);
        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);
        tvDisplayError = findViewById(R.id.tvAddFriendErrors);
        btnSentRequest = findViewById(R.id.btnSentRequest);
        btnSentRequest.setOnClickListener(this::onClick);
        btnAddFromPhonebookFriend = findViewById(R.id.btnAddFromPhonebookFriend);
        btnAddFromPhonebookFriend.setOnClickListener(this::onClick);
        db = FirebaseFirestore.getInstance();
    }

    private void showUserResultDialog(User user) {
        FriendResultDialogFragment resultDialogFragment = FriendResultDialogFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putParcelable("user", Parcels.wrap(user));
        resultDialogFragment.setArguments(bundle);
        resultDialogFragment.show(getSupportFragmentManager(), RESULT_DIALOG_TAG);
    }

    public void searchFriend(View view) {
        clearDisplayError();
        String username = getInputText(tipAddFriendPhoneOrEmail);

        if (!getInputText(tipAddFriendType).equalsIgnoreCase((String) adapter.getItem(0))) {
            username = generateEmailFromPhone(username);
        }

        db.collection(CollectionConst.COLLECTION_USER)
                .whereEqualTo("email", username)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<DocumentSnapshot> docSnapshots = queryDocumentSnapshots.getDocuments();
                    if (!docSnapshots.isEmpty()) {
                        DocumentSnapshot docSnap = docSnapshots.get(0);
                        User user = docSnap.toObject(User.class);
                        if (user != null) {
                            user.setUid(docSnap.getId());
                            showUserResultDialog(user);
                        }
                    } else {
                        tvDisplayError.setText("Số điện thoại hay email này \n không tồn tại trong hệ thống");
                        tvDisplayError.setVisibility(View.VISIBLE);
                    }
                })
                .addOnFailureListener(e -> {
                    Log.d("findFriend", e.getMessage());
                    tvDisplayError.setText("Có lỗi xảy ra \n Xin vui lòng hãy thử lại");
                    tvDisplayError.setVisibility(View.VISIBLE);
                });
    }


    private void clearDisplayError() {
        tvDisplayError.setText(null);
        tvDisplayError.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEND_FRIEND_REQUEST_RESULT_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                showUserResultDialog(Parcels.unwrap(data.getParcelableExtra("user")));
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSentRequest:
                startActivity(new Intent(this, SentFriendRequestActivity.class));
                break;
            case R.id.btnAddFromPhonebookFriend:
                //startActivity(new Intent(this, PhonebookFriendsActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }
}
