package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder;

public interface OnMessageClickListener {
    void onMessageItemLongClickListener(int position);

    void onTrySendMessageAgain(int position);

    void onMessageItemClickListener(int position);
}
