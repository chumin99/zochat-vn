package com.mingroup.zochatvn.ui.mainscreen.message.chatwindow.messageviewholder;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.makeramen.roundedimageview.RoundedImageView;

import com.mingroup.zochatvn.R;
import lombok.Getter;

@Getter
public class FileMessageViewHolder extends RecyclerView.ViewHolder {
    private RoundedImageView ivAvatar;
    private TextView tvDisplayName, tvTimestamp, tvBreakTimeStamp;

    private TextView tvFileName, tvFileInfo;
    private LinearProgressIndicator progressBar;
    private Button btnOpen;
    private View fileContainer;

    private OnMessageClickListener onMessageClickListener;

    public FileMessageViewHolder(@NonNull View itemView, OnMessageClickListener onMessageClickListener, boolean fromLeft) {
        super(itemView);
        this.onMessageClickListener = onMessageClickListener;

        tvBreakTimeStamp = itemView.findViewById(R.id.break_timestamp);
        tvTimestamp = itemView.findViewById(R.id.media_message_item_timestamp);
        tvFileName = itemView.findViewById(R.id.media_file_name);
        tvFileInfo = itemView.findViewById(R.id.media_file_info);
        progressBar = itemView.findViewById(R.id.media_file_progress);
        btnOpen = itemView.findViewById(R.id.media_file_open);
        fileContainer = itemView.findViewById(R.id.partial_item_media_file);

        fileContainer.setVisibility(View.VISIBLE);

        if (fromLeft) {
            ivAvatar = itemView.findViewById(R.id.media_message_item_avatar);
            tvDisplayName = itemView.findViewById(R.id.media_message_item_display_name);
        }

        itemView.setOnClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemClickListener(position);
                }
            }
        });

        btnOpen.setOnClickListener(view -> {
            itemView.performClick();
        });

        itemView.setOnLongClickListener(view -> {
            if (onMessageClickListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    onMessageClickListener.onMessageItemLongClickListener(position);
                    return true;
                }
            }
            return false;
        });
    }
}
