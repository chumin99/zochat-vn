package com.mingroup.zochatvn.ui.forgetpassword;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.FirebaseAuth;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.util.FragmentUtil;

import static com.mingroup.zochatvn.util.StringUtil.generatePhoneWithCountryCode;
import static com.mingroup.zochatvn.util.StringUtil.getInputText;
import static com.mingroup.zochatvn.util.ValidatorUtil.isValidEmail;
import static com.mingroup.zochatvn.util.ValidatorUtil.isVietnamesePhone;

public class ForgetPasswordStepOneFragment extends Fragment {
    private static final String FORGET_PASSWORD_PHONE_NUM = "forgetPhoneNumber";
    private TextInputLayout tipForgetPasswordIndentifier;
    private FloatingActionButton fabNext;
    private FirebaseAuth zcAuth;
    private ActionCodeSettings actionCodeSettings;
    private RelativeLayout relativeLayout;
    private String emailOrPhone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forget_password_step_one, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
        tipForgetPasswordIndentifier.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputText(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputText(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkInputText(editable.toString());
            }
        });

        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: kiểm tra email có trong hệ thống chưa ? Nếu có r thì gợi ý tạo tài khoản
                showEmailOrPhoneConfirmation();

            }
        });
    }

    private void init(View view) {
        tipForgetPasswordIndentifier = view.findViewById(R.id.tipIndentifier);
        fabNext = view.findViewById(R.id.fabForgetStepOneNext);
        fabNext.setAlpha(.2f);
        zcAuth = FirebaseAuth.getInstance();
        relativeLayout = view.findViewById(R.id.forget_password_step_one_rl);
    }

    private void showEmailOrPhoneConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        emailOrPhone = getInputText(tipForgetPasswordIndentifier);
        if (!isValidEmail(emailOrPhone))
            emailOrPhone = generatePhoneWithCountryCode(emailOrPhone);
        String message = "<h4 style=\"text-align: center;\"><strong>" + emailOrPhone + "</strong></h4>" + "Chúng tôi sẽ gửi mã xác nhận đến số điện thoại hoặc email trên để kích hoạt tài khoản của bạn. Vui lòng xác nhận thông tin này là đúng.";
        builder.setTitle("Xác nhận")
                .setMessage(Html.fromHtml(message))
                .setPositiveButton("Xác nhận", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // nếu là email thì gửi email xác nhận
                        // nếu là phone thì gửi sms xác nhận
                        if (isValidEmail(emailOrPhone)) {
                            sendPasswordResetEmail(emailOrPhone);
                        } else {
                            // chuyển sang bước tiếp theo để nhận otp reauthentication
                            Bundle bundle = new Bundle();
                            bundle.putString(FORGET_PASSWORD_PHONE_NUM, getInputText(tipForgetPasswordIndentifier));
                            FragmentUtil.replaceFragment(getActivity(), R.id.forgot_password_fragment_container,
                                    new ForgetPasswordStepTwoFragment(), bundle, ForgetPasswordStepTwoFragment.class.getSimpleName(), true);
                        }
                    }
                })
                .setNegativeButton(Html.fromHtml("<span style=\"color: #000000;\">Thay đổi</span>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                        if (keyCode == KeyEvent.KEYCODE_BACK)
                            dialogInterface.dismiss();
                        return true;
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void checkInputText(String str) {
        tipForgetPasswordIndentifier.setError(null);
        if (!str.isEmpty() && (isVietnamesePhone(str) || isValidEmail(str))) {
            fabNext.setClickable(true);
            fabNext.setAlpha(1f);
        } else {
            tipForgetPasswordIndentifier.setError("Vui lòng nhập Email hoặc Số điện thoại hợp lệ!");
            fabNext.setClickable(false);
            fabNext.setAlpha(.2f);
        }
    }

    private void buildActionCodeSettings() {
        actionCodeSettings = ActionCodeSettings.newBuilder()
                .setUrl("https://zochat-38944.firebaseapp.com/forget-password")
                .setHandleCodeInApp(true)
                .setAndroidPackageName(
                        "com.mingroup.zochatvn",
                        true,
                        "23")
                .build();
    }

    private void sendPasswordResetEmail(String email) {
        buildActionCodeSettings();
        zcAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                            showLoginHintDialog();
                        else {
                            Snackbar.make(relativeLayout, "Có lỗi xảy ra", BaseTransientBottomBar.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void showLoginHintDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Thông báo")
                .setMessage(Html.fromHtml("<h4 style=\"text-align: center;\"><strong>" + "Đã gửi mail đặt lại mật khẩu" + "</strong></h4>" + "Hãy nhập mật khẩu mới trong link xác nhận chúng tôi gửi cho bạn ở Email."))
                .setPositiveButton("Đi đến đăng nhập", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().finish();
                    }
                })
                .setNegativeButton(Html.fromHtml("<span style=\"color: #000000;\">Quay lại</span>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                        if (keyCode == KeyEvent.KEYCODE_BACK)
                            dialogInterface.dismiss();
                        return true;
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}