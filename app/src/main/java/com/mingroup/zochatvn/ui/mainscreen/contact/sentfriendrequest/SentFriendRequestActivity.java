package com.mingroup.zochatvn.ui.mainscreen.contact.sentfriendrequest;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.ui.mainscreen.contact.addfriend.AddFriendActivity;
import com.mingroup.zochatvn.util.ComponentsUtil;
import com.mingroup.zochatvn.util.ZCLoadingDialog;

public class SentFriendRequestActivity extends AppCompatActivity implements SentFriendRequestListAdapter.OnSentFriendRequestItemListener {
    private static final String TAG = SentFriendRequestActivity.class.getSimpleName();
    private Toolbar zcToolBar;
    private RecyclerView rvSendRequestList;
    private TextView tvSentRequestCount;
    private List<FriendRequest> items;
    private SentFriendRequestListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private LinearLayout noDataSection;
    private Button btnGotoAddFriend;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FirebaseFirestore db;
    private ZCLoadingDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_friend_request);

        loading = ZCLoadingDialog.create(this).setStyle(ZCLoadingDialog.Style.MODERN_STYLE);
        loading.show();
        init();
        setupActionBar();
        setupSentFriendRequestRecyclerView();
        btnGotoAddFriend.setOnClickListener(view -> {
            startActivity(new Intent(this, AddFriendActivity.class));
        });
        swipeRefreshLayout.setOnRefreshListener(() -> {
            populateDataToRecyclerView();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        populateDataToRecyclerView();
    }

    private void setupSentFriendRequestRecyclerView() {
        items = new ArrayList<>();
        adapter = new SentFriendRequestListAdapter(this, items, this);
        rvSendRequestList.setAdapter(adapter);
    }

    private void populateDataToRecyclerView() {
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .whereEqualTo("createdBy", ComponentsUtil.getCurrentUserRef(db))
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if(error != null) {
                            Log.d(TAG, "getSentFriendRequests::onError: " + error.getMessage());
                        }

                        if(value != null) {
                            value.getDocuments().forEach(documentSnapshot -> {
                                FriendRequest contact = documentSnapshot.toObject(FriendRequest.class);
                                if (contact != null) {
                                    contact.setId(documentSnapshot.getId());
                                    items.add(contact);
                                }
                            });
                            adapter.update(items);
                        }
                        updateSentFriendRequestListCount();
                        checkNoDataSection();
                    }
                });
    }

    private void checkNoDataSection() {
        if (adapter.getItemCount() > 0) {
            noDataSection.setVisibility(View.GONE);
        } else {
            noDataSection.setVisibility(View.VISIBLE);
        }
        loading.dismiss();
    }

    private void setupActionBar() {
        setSupportActionBar(zcToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.sent_friend_request));
    }

    private void init() {
        zcToolBar = findViewById(R.id.zochat_toolbar_no_search_view);
        rvSendRequestList = findViewById(R.id.rvSentFriendRequestList);
        tvSentRequestCount = findViewById(R.id.tvSentFriendRequestCount);
        noDataSection = findViewById(R.id.no_sent_friend_request_section);
        btnGotoAddFriend = findViewById(R.id.btn_add_friend_contact);
        swipeRefreshLayout = findViewById(R.id.sent_friend_request_swipe_layout);
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onSentFriendRequestItemClick(int position) {

    }

    @Override
    public void onSentFriendRequestCancelClick(int position) {
        loading.show();
        FriendRequest selectedFriendRequest = adapter.getItem(position);
        db.collection(CollectionConst.COLLECTION_FRIEND_REQUEST)
                .document(selectedFriendRequest.getId())
                .delete()
                .addOnSuccessListener(aVoid -> {
                    items.remove(position);
                    adapter.update(items);
                    adapter.notifyItemRemoved(position);
                    checkNoDataSection();
                    updateSentFriendRequestListCount();
                })
                .addOnFailureListener(e -> {
                    Log.d("SendFriendRequestCancelClick::onError: ", e.getMessage());
                    Toast.makeText(SentFriendRequestActivity.this, "Có lỗi xảy ra. Chúng tôi sẽ kiểm tra lại!", Toast.LENGTH_SHORT).show();
                });
        loading.dismiss();
    }

    private void updateSentFriendRequestListCount() {
        tvSentRequestCount.setText(String.format("Bạn có %s lời mời kết bạn", items.size() + ""));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }
}
