package com.mingroup.zochatvn.data.local.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mingroup.zochatvn.data.model.ContactInfo;

import java.util.List;

@Dao
public interface PhonebookDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertContact(ContactInfo contactInfo);

    @Query("DELETE FROM PHONEBOOK WHERE contactPhoneNumber = :phoneNumber")
    int deleteContactByPhoneNumber(String phoneNumber);

    @Query("SELECT * FROM PHONEBOOK WHERE contactPhoneNumber = :phoneNumber")
    ContactInfo getContactByPhoneNumber(String phoneNumber);

    @Query("SELECT * FROM PHONEBOOK")
    List<ContactInfo> getAllContact();
}
