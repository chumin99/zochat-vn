package com.mingroup.zochatvn.data.model;

import com.google.firebase.auth.FirebaseUser;
import com.google.gson.annotations.SerializedName;
import com.mingroup.zochatvn.data.model.request.UserProfileRequest;

import org.parceler.Parcel;

import java.util.Date;

import lombok.*;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class User {
    String uid;
    String email;
    @SerializedName("phone")
    String phoneNumber;
    String displayName;
    Date dob;
    boolean isMale;
    String photoUrl;
    @SerializedName("disabled")
    boolean isDisabled;

    public static User createUserProfile(FirebaseUser firebaseUser, String displayName) {
        return new User(firebaseUser.getUid(),
                firebaseUser.getEmail(),
                firebaseUser.getPhoneNumber(),
                displayName,
                null, true, null, false);
    }

    public static User updateUserProfile(FirebaseUser firebaseUser, UserProfileRequest request) {
        return new User(firebaseUser.getUid(),
                firebaseUser.getEmail(),
                firebaseUser.getPhoneNumber(),
                request.getDisplayName(),
                request.getDob(),
                request.isMale(),
                request.getPhotoUrl(),
                false);
    }
}