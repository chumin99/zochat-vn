package com.mingroup.zochatvn.data.model;

import android.os.Parcel;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.ParcelConverter;

import java.util.ArrayList;
import java.util.List;

public class DocumentReferenceConverterList implements ParcelConverter<List<DocumentReference>> {
    private final DocumentReferenceConverter converter = new DocumentReferenceConverter();

    @Override
    public void toParcel(List<DocumentReference> input, Parcel parcel) {
        if(input == null) {
            parcel.writeInt(-1);
        } else {
            parcel.writeInt(input.size());
            for(DocumentReference docRef : input) {
                converter.toParcel(docRef, parcel);
            }
        }
    }

    @Override
    public List<DocumentReference> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if(size < 0)
            return null;
        List<DocumentReference> list = new ArrayList<>();
        for(int i = 0; i < size; ++i) {
            list.add(converter.fromParcel(parcel));
        }
        return list;
    }
}
