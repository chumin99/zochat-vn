package com.mingroup.zochatvn.data.local.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.mingroup.zochatvn.data.model.User;
import com.mingroup.zochatvn.data.model.request.UserProfileRequest;

import java.text.ParseException;
import java.util.HashMap;

import static com.mingroup.zochatvn.constant.UserSession.SESSION_USER_SESSION;
import static com.mingroup.zochatvn.constant.UserSession.IS_REMEMBER_ME;
import static com.mingroup.zochatvn.constant.UserSession.KEY_AVATAR;
import static com.mingroup.zochatvn.constant.UserSession.KEY_DISPLAY_NAME;
import static com.mingroup.zochatvn.constant.UserSession.KEY_DOB;
import static com.mingroup.zochatvn.constant.UserSession.KEY_EMAIL;
import static com.mingroup.zochatvn.constant.UserSession.KEY_IS_DISABLED;
import static com.mingroup.zochatvn.constant.UserSession.KEY_IS_MALE;
import static com.mingroup.zochatvn.constant.UserSession.KEY_LAST_UPDATE_PHONEBOOK;
import static com.mingroup.zochatvn.constant.UserSession.KEY_PASSWORD;
import static com.mingroup.zochatvn.constant.UserSession.KEY_PHONE;
import static com.mingroup.zochatvn.constant.UserSession.KEY_UID;
import static com.mingroup.zochatvn.constant.UserSession.TOKEN;
import static com.mingroup.zochatvn.util.StringUtil.convertDateToString;
import static com.mingroup.zochatvn.util.StringUtil.convertStringToDate;

public class SessionManager {
    private SharedPreferences userSession;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context ctx) {
        this.context = ctx;
        this.userSession = context.getSharedPreferences(SESSION_USER_SESSION, Context.MODE_PRIVATE);
        this.editor = userSession.edit();
    }

    public void createSavedLoginSession(String emailOrPhone, String password) {
        editor.putBoolean(IS_REMEMBER_ME, true);
        editor.putString(KEY_EMAIL, emailOrPhone);
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }

    public HashMap<String, String> getUserInfoFromSession() {
        HashMap<String, String> userData = new HashMap<>();
        userData.put(KEY_EMAIL, userSession.getString(KEY_EMAIL, null));
        userData.put(KEY_PASSWORD, userSession.getString(KEY_PASSWORD, null));
        return userData;
    }

    public void saveCurrentUserInfo(User user) throws ParseException {
        editor.putString(KEY_UID, user.getUid());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putString(KEY_PHONE, user.getPhoneNumber());
        editor.putString(KEY_DISPLAY_NAME, user.getDisplayName());
        editor.putString(KEY_DOB, convertDateToString(user.getDob()));
        editor.putString(KEY_AVATAR, user.getPhotoUrl());
        editor.putBoolean(KEY_IS_MALE, user.isMale());
        editor.putBoolean(KEY_IS_DISABLED, user.isDisabled());
        editor.commit();
    }

    public User getCurrentUserInfo() throws ParseException {
        User currentUser = new User();
        currentUser.setUid(userSession.getString(KEY_UID, null));
        currentUser.setEmail(userSession.getString(KEY_EMAIL, null));
        currentUser.setPhoneNumber(userSession.getString(KEY_PHONE, null));
        currentUser.setDisplayName(userSession.getString(KEY_DISPLAY_NAME, null));
        currentUser.setDob(convertStringToDate(userSession.getString(KEY_DOB, null)));
        currentUser.setPhotoUrl(userSession.getString(KEY_AVATAR, null));
        currentUser.setMale(userSession.getBoolean(KEY_IS_MALE, false));
        currentUser.setDisabled(userSession.getBoolean(KEY_IS_DISABLED, false));
        return currentUser;
    }

    public void updateUserProfile(UserProfileRequest user) throws ParseException {
        editor.putString(KEY_DISPLAY_NAME, user.getDisplayName());
        editor.putString(KEY_DOB, convertDateToString(user.getDob()));
        editor.putString(KEY_AVATAR, user.getPhotoUrl());
        editor.putBoolean(KEY_IS_MALE, user.isMale());
    }

    public void logout() {
        editor.remove(KEY_UID);
        editor.remove(KEY_EMAIL);
        editor.remove(KEY_PHONE);
        editor.remove(KEY_DISPLAY_NAME);
        editor.remove(KEY_DOB);
        editor.remove(KEY_AVATAR);
        editor.remove(KEY_IS_MALE);
        editor.remove(KEY_IS_DISABLED);
        editor.remove(TOKEN);
        editor.commit();
    }

    public void storeTokenId(String tokenId) {
        editor.putString(TOKEN, tokenId);
        editor.commit();
    }

    public void storeDisplayName(String displayName) {
        editor.putString(KEY_DISPLAY_NAME, displayName);
        editor.commit();
    }

    public String getStoredDisplayname() {
        return userSession.getString(KEY_DISPLAY_NAME, null);
    }

    public String getCurrentUserUid() {
        return userSession.getString(KEY_UID, null);
    }

    public void storeLastUpdatePhonebook(String lastUpdate) {
        editor.putString(KEY_LAST_UPDATE_PHONEBOOK, lastUpdate);
        editor.commit();
    }

    public String getLastUpdatePhonebook() {
        return userSession.getString(KEY_LAST_UPDATE_PHONEBOOK, null);
    }

    public String getTokenId() {
        return userSession.getString(TOKEN, null);
    }
}
