package com.mingroup.zochatvn.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.util.ComponentsUtil;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class FriendRequest {
    String id;
    @ParcelPropertyConverter(DocumentReferenceConverterList.class)
    List<DocumentReference> users;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference createdBy;
    String message;
    Date createdAt;
    FriendRequestStatus status;

    public static FriendRequest createNewFriendRequest(FirebaseFirestore db, User to, String message) {
        DocumentReference toRef = db.collection(CollectionConst.COLLECTION_USER).document(to.getUid());
        DocumentReference fromRef = ComponentsUtil.getCurrentUserRef(db);
        List<DocumentReference> users = new ArrayList<>();
        users.addAll(Arrays.asList(toRef, fromRef));
        return new FriendRequest(null, users, fromRef, message, new Date(), FriendRequestStatus.PENDING);
    }
}
