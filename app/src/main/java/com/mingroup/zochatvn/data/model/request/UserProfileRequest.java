package com.mingroup.zochatvn.data.model.request;

import com.google.gson.annotations.SerializedName;
import com.mingroup.zochatvn.data.model.User;

import org.parceler.Parcel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileRequest {
    String displayName;
    Date dob;
    boolean isMale;
    String photoUrl;

    public static UserProfileRequest convertFromUser(User user) {
        return new UserProfileRequest(user.getDisplayName(), user.getDob(), user.isMale(), user.getPhotoUrl());
    }
}
