package com.mingroup.zochatvn.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(tableName = "PHONEBOOK")
@Parcel
@Data
@NoArgsConstructor
public class ContactInfo {
    @PrimaryKey
    @ColumnInfo
    long id;

    @ColumnInfo
    String contactName;

    @ColumnInfo
    String contactPhoneNumber;
}
