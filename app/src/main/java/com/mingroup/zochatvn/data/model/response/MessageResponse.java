package com.mingroup.zochatvn.data.model.response;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.Conservation;
import com.mingroup.zochatvn.data.model.DocumentReferenceConverter;
import com.mingroup.zochatvn.data.model.MessageType;
import com.mingroup.zochatvn.util.ComponentsUtil;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageResponse {
    String id;
    String content;
    MessageType type;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference conservation;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference senderInfo;
    Date createdAt;
    Date updatedAt;

    public static MessageResponse createNewMessage(FirebaseFirestore db, Conservation conservation, String content, MessageType messageType) {
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setConservation(db.collection(CollectionConst.COLLECTION_CONSERVATION).document(conservation.getId()));
        messageResponse.setContent(content);
        messageResponse.setType(messageType);
        messageResponse.setCreatedAt(new Date());
        messageResponse.setSenderInfo(ComponentsUtil.getCurrentUserRef(db));
        return messageResponse;
    }
}
