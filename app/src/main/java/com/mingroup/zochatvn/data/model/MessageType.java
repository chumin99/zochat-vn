package com.mingroup.zochatvn.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MessageType {
    TEXT("MESSAGE_TYPE_TEXT", 0),
    IMAGE("MESSAGE_TYPE_IMAGE", 1),
    VIDEO("MESSAGE_TYPE_VIDEO", 2),
    FILE("MESSAGE_TYPE_FILE", 3),
    STICKER("MESSAGE_TYPE_STICKER", 4);

    private String type;
    private int id;

    public static MessageType getMessageTypeFromTypeStr(final String typeStr) {
        for (MessageType messageType : MessageType.values()) {
            if (messageType.getType().equals(typeStr)) {
                return messageType;
            }
        }
        return null;
    }

    public static MessageType getMessageTypeFromId(final int id) {
        for (MessageType messageType : MessageType.values()) {
            if (messageType.id == id) {
                return messageType;
            }
        }
        return null;
    }
}
