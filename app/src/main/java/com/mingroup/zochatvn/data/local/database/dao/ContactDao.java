package com.mingroup.zochatvn.data.local.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.local.database.MyDatabase;
import com.mingroup.zochatvn.data.model.ContactInfo;
import com.mingroup.zochatvn.util.StringUtil;


import java.util.concurrent.atomic.AtomicBoolean;

import static com.mingroup.zochatvn.constant.LocalDatabaseConst.KEY_DISPLAY_NAME;
import static com.mingroup.zochatvn.constant.LocalDatabaseConst.KEY_ID;
import static com.mingroup.zochatvn.constant.LocalDatabaseConst.KEY_PHONE_NUMBER;

public class ContactDao {
    private final Context context;
    private final PhonebookDao phonebookDao;
    private final FirebaseFirestore db;

    public ContactDao(Context context) {
        this.context = context;
        this.phonebookDao = MyDatabase.getDatabase(context).phonebookDao();
        this.db = FirebaseFirestore.getInstance();
    }

    public void updateContactListToPhonebookFriend() {
        Cursor cursor = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, null, null,
                ContactsContract.CommonDataKinds.Phone.NUMBER);
        try {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                    if (hasPhoneNumber > 0) {
                        ContactInfo contactInfo = new ContactInfo();
                        String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                        Cursor phoneCursor = context.getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{contactId},
                                null
                        );

                        if (phoneCursor.moveToNext()) {
                            String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            contactInfo.setContactName(displayName);
                            contactInfo.setContactPhoneNumber(phoneNumber);

                            db.collection(CollectionConst.COLLECTION_USER)
                                    .whereEqualTo("phoneNumber", StringUtil.convertPhoneNumberFromContact(phoneNumber))
                                    .get()
                                    .addOnSuccessListener(queryDocumentSnapshots -> phonebookDao.insertContact(contactInfo));
                        }
                        phoneCursor.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }

    public boolean deleteContactFromDevice(String phoneNumber) {
        AtomicBoolean isDeleteSuccessful = new AtomicBoolean();
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = context.getContentResolver().query(contactUri, null, null, null, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    String lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
                    context.getContentResolver().delete(uri, null, null);
                    isDeleteSuccessful.set(phonebookDao.deleteContactByPhoneNumber(phoneNumber) > 0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d("DeleteContact", e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return isDeleteSuccessful.get();
    }
}
