package com.mingroup.zochatvn.data.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FriendRequestStatus {
    PENDING("0"), // Đang chờ accept
    ACCEPTED("1"), // Đã được accepted
    REJECTED("2"), // Đã bị từ chối
    NOT_FRIEND("3"), // Không phải bạn bè
    REQUESTED("4"); // Được gửi lời mời kết bạn đến

    private String id;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static FriendRequestStatus getFriendRequestStatus(final String code) {
        return Stream.of(FriendRequestStatus.values())
                .filter(targetStatus -> targetStatus.id.equalsIgnoreCase(code)).findFirst().get();
    }
}
