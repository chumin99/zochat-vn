package com.mingroup.zochatvn.data.model;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.annotations.SerializedName;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.model.response.MessageResponse;
import com.mingroup.zochatvn.util.ComponentsUtil;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Parcel
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Conservation {
    String id;
    String title;
    String groupPhotoUrl;
    @ParcelPropertyConverter(DocumentReferenceConverterList.class)
    List<DocumentReference> members;
    ConservationType type;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference createdBy;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference admin;
    Date createdAt;
    MessageResponse lastMessage;
    String description;
    List<MessageResponse> messsageList;

    public static Conservation createNewGroup(String title, String groupPhotoUrl, List<DocumentReference> members, DocumentReference createdBy) {
        Conservation conservation = new Conservation();
        conservation.setTitle(title);
        conservation.setMembers(members);
        conservation.setCreatedBy(createdBy);
        conservation.setAdmin(createdBy);
        conservation.setCreatedAt(new Date());
        conservation.setGroupPhotoUrl(groupPhotoUrl);
        conservation.setType(ConservationType.GROUP);
        return conservation;
    }

    public static Conservation createSingleConservation(FirebaseFirestore db, String userFriendUid) {
        Conservation conservation = new Conservation();
        DocumentReference userMe = ComponentsUtil.getCurrentUserRef(db);
        DocumentReference userFriend = db.collection(CollectionConst.COLLECTION_USER).document(userFriendUid);

        conservation.setType(ConservationType.SINGLE);
        conservation.setCreatedAt(new Date());
        conservation.setCreatedBy(userMe);
        conservation.setMembers(Arrays.asList(userMe, userFriend));
        return conservation;
    }
}