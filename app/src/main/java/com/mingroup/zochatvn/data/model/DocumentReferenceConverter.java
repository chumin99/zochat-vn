package com.mingroup.zochatvn.data.model;

import android.os.Parcel;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.parceler.ParcelConverter;

public class DocumentReferenceConverter implements ParcelConverter<DocumentReference> {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public void toParcel(DocumentReference input, Parcel parcel) {
        parcel.writeString(input.getPath());
    }

    @Override
    public DocumentReference fromParcel(Parcel parcel) {
        return db.document(parcel.readString());
    }
}
