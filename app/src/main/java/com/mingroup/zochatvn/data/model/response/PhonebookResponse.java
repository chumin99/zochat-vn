package com.mingroup.zochatvn.data.model.response;

import androidx.room.Entity;

import com.google.firebase.firestore.DocumentReference;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.FriendRequestStatus;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class PhonebookResponse extends FriendRequest {
    String contactName;

    public PhonebookResponse(String contactName, DocumentReference createdBy, FriendRequestStatus status) {
        this.contactName = contactName;
        super.setCreatedBy(createdBy);
        super.setStatus(status);
    }

    public static PhonebookResponse createNoFriendPhonebook(String contactName, DocumentReference createdBy) {
        return new PhonebookResponse(contactName, createdBy, FriendRequestStatus.NOT_FRIEND);
    }
}
