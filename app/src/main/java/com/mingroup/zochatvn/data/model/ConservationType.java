package com.mingroup.zochatvn.data.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ConservationType {
    SINGLE,
    GROUP
}