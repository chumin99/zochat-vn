package com.mingroup.zochatvn.data.local.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.mingroup.zochatvn.constant.LocalDatabaseConst;
import com.mingroup.zochatvn.data.local.database.dao.PhonebookDao;
import com.mingroup.zochatvn.data.model.ContactInfo;

import static com.mingroup.zochatvn.constant.LocalDatabaseConst.DATABASE_NAME;

@Database(entities = {ContactInfo.class}, version = LocalDatabaseConst.DATABASE_VERSION, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {
    public abstract PhonebookDao phonebookDao();
    private static volatile MyDatabase mInstance;

    public static MyDatabase getDatabase(final Context context) {
        if(mInstance == null) {
            synchronized (MyDatabase.class) {
                if(mInstance == null) {
                    mInstance = Room.databaseBuilder(context, MyDatabase.class, DATABASE_NAME)
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return mInstance;
    }
}
