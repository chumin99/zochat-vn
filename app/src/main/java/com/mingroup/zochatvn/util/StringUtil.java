package com.mingroup.zochatvn.util;

import com.google.android.material.textfield.TextInputLayout;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtil {
    private static final String VIETNAM_COUNTRY_CODE = "(+84) ";
    private static final String VIETNAM_COUNTRY_CODE_2 = "+84";
    public static final String EMAIL_ZOCHAT_SUFFIX = "@zochat.dev";
    public static final String SIMPLE_DATE_FORMAT_PATTERN = "dd/MM/yyyy";
    private static final DecimalFormat format = new DecimalFormat("#.##");
    private static final long MiB = 1024 * 1024;
    private static final long KiB = 1024;


    public static String generatePhoneWithCountryCode(String numPhone) {
        String phone = numPhone;
        if (numPhone.startsWith("0", 0)) {
            phone = phone.substring(1);
        }
        return VIETNAM_COUNTRY_CODE + phone.substring(0, 3) + " " + phone.substring(3, 6) + " " + phone.substring(6, 9);
    }

    //(093) 723-1868
    // +84968958053
    public static String convertPhoneNumberFromContact(String numPhone) {
        String phone = numPhone.replaceAll("[\\s()-]", "");
        if (phone.startsWith("0", 0)) {
            phone = phone.substring(1);
        }
        return VIETNAM_COUNTRY_CODE_2 + phone;
    }

    // +84968958053
    //(093) 723-1868
    public static String revertPhoneNumberFromContact(String phoneNumber) {
        StringBuilder phoneStrBuilder = new StringBuilder(phoneNumber);
        phoneStrBuilder.delete(0, 3);
        phoneStrBuilder.insert(0, "0");
        return phoneStrBuilder.toString().replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1) $2-$3");
    }

    // +84968958053
    // 0968958053
    public static String revertPhoneNumberWithoutCountryCode(String phoneNumberWithCountryCode) {
        StringBuilder phoneStrBuilder = new StringBuilder(phoneNumberWithCountryCode);
        return phoneStrBuilder.delete(0, 3).insert(0, "0").toString();
    }

    public static String generatePhoneNumber(String phoneNumber) {
        StringBuilder strBuilder = new StringBuilder(phoneNumber);
        if (phoneNumber.length() == 9) {
            strBuilder.insert(0, "0");
        }
        return strBuilder.toString();
    }

    public static String getInputText(TextInputLayout tip) {
        return tip.getEditText().getText().toString();
    }

    public static String generateEmailFromPhone(String phone) {
        return generatePhoneNumber(phone).trim() + EMAIL_ZOCHAT_SUFFIX;
    }

    // formatted: 21:46, 14/10/2020
    public static String formatDateTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a, dd/MM/yyyy");
        return sdf.format(date);
    }

    public static Date convertDatePickerTextToFormattedDate(String datePickerText) throws ParseException {
        datePickerText = datePickerText.replace("thg ", "");
        DateFormat originalDateFormat = new SimpleDateFormat("dd MM, yyyy");
        return originalDateFormat.parse(datePickerText);
    }

    public static String convertDateToString(Date date) {
        if(date != null) {
            DateFormat targetDateFormat = new SimpleDateFormat(SIMPLE_DATE_FORMAT_PATTERN);
            return targetDateFormat.format(date);
        }
        return null;
    }

    public static Date convertStringToDate(String dateStr) throws ParseException {
        if (dateStr != null && !dateStr.isEmpty()) {
            return new SimpleDateFormat(SIMPLE_DATE_FORMAT_PATTERN).parse(dateStr);
        }
        return null;
    }

    public static Date fromISODateString(String string) throws ParseException {
        if (string != null && !string.isEmpty())
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(string);
        return null;
    }

    public static String toISODateString(Date date) {
        if (date != null)
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(date);
        return null;
    }

    public static String formatChatDateTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a, dd/MM/yyyy");
        return sdf.format(date);
    }

    public static String formatChatTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(date);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static String convertListToString(List<String> list, String separator) {
        StringBuilder builder = new StringBuilder();
        for (String element : list) {
            builder.append(element);
            builder.append(separator);
        }
        return builder.toString();
    }

    public static String convertListToString(String[] list, String separator) {
        StringBuilder builder = new StringBuilder();
        for (String element : list) {
            builder.append(element);
            builder.append(separator);
        }
        return builder.toString();
    }

    public String getFileSize(File file) {
        if (!file.isFile()) {
            throw new IllegalArgumentException("Expected a file");
        }
        final double length = file.length();

        if (length > MiB) {
            return format.format(length / MiB) + " MB";
        }
        if (length > KiB) {
            return format.format(length / KiB) + " KB";
        }
        return format.format(length) + " B";
    }
}
