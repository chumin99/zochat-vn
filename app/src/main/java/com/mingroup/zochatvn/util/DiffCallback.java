package com.mingroup.zochatvn.util;


import androidx.recyclerview.widget.DiffUtil;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public abstract class DiffCallback<E> extends DiffUtil.Callback {
    protected List<E> oldList = new ArrayList<>();
    protected List<E> newList = new ArrayList<>();

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }
}
