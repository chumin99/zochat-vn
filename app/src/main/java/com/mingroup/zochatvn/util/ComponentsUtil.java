package com.mingroup.zochatvn.util;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.bumptech.glide.signature.ObjectKey;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.CollectionConst;
import com.mingroup.zochatvn.data.local.preference.SessionManager;
import com.mingroup.zochatvn.data.model.FriendRequest;
import com.mingroup.zochatvn.data.model.User;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import lombok.experimental.UtilityClass;

import static com.mingroup.zochatvn.constant.UserSession.AUTHORIZATION;
import static com.mingroup.zochatvn.constant.UserSession.BEARER;

@UtilityClass
public class ComponentsUtil {
    public static String currentUserUid;
    static {
        currentUserUid = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
    }

    public static void displayImage(ImageView imageView, String photoUrl) {
        Context context = imageView.getContext();
        Glide.with(context)
                .load(photoUrl)
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .format(DecodeFormat.PREFER_ARGB_8888)
                .placeholder(new ColorDrawable(context.getResources().getColor(R.color.icon_tint, context.getTheme())))
                .error(R.drawable.place_holder_image)
                .into(imageView);
    }

    public static void displayGif(ImageView imageView, String photoUrl) {
        Context context = imageView.getContext();
        Glide.with(context)
                .asGif()
                .load(photoUrl)
                .override(150, 150)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                .placeholder(R.drawable.sticker_place_holder)
                .into(new ImageViewTarget<GifDrawable>(imageView) {
                    @Override
                    protected void setResource(@Nullable GifDrawable resource) {
                        imageView.setImageDrawable(resource);
                    }
                });
    }

    public static void displayImageFromApi(ImageView imageView, String photoUrl) {
        Context context = imageView.getContext();

        if (photoUrl != null && !photoUrl.isEmpty()) {
            GlideUrl glideUrl = new GlideUrl(photoUrl, new LazyHeaders.Builder()
                    .addHeader(AUTHORIZATION, BEARER + new SessionManager(context).getTokenId())
                    .build());

            Glide.with(context)
                    .load(glideUrl)
                    .dontTransform()
                    .centerCrop()
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .signature(new ObjectKey(photoUrl))
                    .placeholder(new ColorDrawable(context.getResources().getColor(R.color.icon_tint, context.getTheme())))
                    .error(R.drawable.place_holder_image)
                    .into(imageView);
        } else {
            displayImage(imageView, photoUrl);
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public static InputStream bitmapToInputStream(Bitmap bitmap) {
        int size = bitmap.getHeight() * bitmap.getRowBytes();
        ByteBuffer buffer = ByteBuffer.allocate(size);
        bitmap.copyPixelsToBuffer(buffer);
        return new ByteArrayInputStream(buffer.array());
    }

    public static Uri getImageUri(Activity inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static Uri resourceToUri(Context context, int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID));
    }

    public static Uri convertFromResource(Resources resources, @DrawableRes int resourceId) {
        return (new Uri.Builder())
                .scheme(ContentResolver.SCHEME_ANDROID_RESOURCE)
                .authority(resources.getResourcePackageName(resourceId))
                .appendPath(resources.getResourceTypeName(resourceId))
                .appendPath(resources.getResourceEntryName(resourceId))
                .build();
    }

    public static Locale getCurrentLocale(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales().get(0);
        } else {
            return context.getResources().getConfiguration().locale;
        }
    }

    public static DocumentReference getFriendUser(List<DocumentReference> usersRef) {
        for (DocumentReference u : usersRef) {
            if (!u.getId().equals(currentUserUid)) {
                return u;
            }
        }
        return null;
    }

    public static DocumentReference getCurrentUserRef(FirebaseFirestore db) {
        return db.collection(CollectionConst.COLLECTION_USER).document(currentUserUid);
    }
}
