package com.mingroup.zochatvn.util.zcKeyboard;

import android.content.Context;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.mingroup.zochatvn.R;
import com.mingroup.zochatvn.constant.ZCKeyboardConst;
import com.mingroup.zochatvn.util.file.FileUtils;
import com.mingroup.zochatvn.util.zcKeyboard.emoticonadapter.BigEmoticonsAdapter;
import com.sj.emoji.DefEmoticons;
import com.sj.emoji.EmojiBean;
import com.sj.emoji.EmojiDisplay;
import com.sj.emoji.EmojiSpan;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;

import sj.keyboard.adpater.EmoticonsAdapter;
import sj.keyboard.adpater.PageSetAdapter;
import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.data.EmoticonPageEntity;
import sj.keyboard.data.EmoticonPageSetEntity;
import sj.keyboard.interfaces.EmoticonClickListener;
import sj.keyboard.interfaces.EmoticonDisplayListener;
import sj.keyboard.interfaces.EmoticonFilter;
import sj.keyboard.interfaces.PageViewInstantiateListener;
import sj.keyboard.utils.EmoticonsKeyboardUtils;
import sj.keyboard.widget.EmoticonPageView;
import sj.keyboard.widget.EmoticonsEditText;

import static com.mingroup.zochatvn.constant.ZCKeyboardConst.EMOTICON_CLICK_EMO;
import static com.mingroup.zochatvn.util.zcKeyboard.ZCKeyboardFileUtils.parseDataFromFile;

public class ZCKeyboardUtils {
    public static PageSetAdapter sCommonPageSetAdapter;

    static class EmojiFilter extends EmoticonFilter {

        private int emojiSize = -1;

        @Override
        public void filter(EditText editText, CharSequence text, int start, int lengthBefore, int lengthAfter) {
            emojiSize = emojiSize == -1 ? EmoticonsKeyboardUtils.getFontHeight(editText) : emojiSize;
            clearSpan(editText.getText(), start, text.toString().length());
            Matcher m = EmojiDisplay.getMatcher(text.toString().substring(start, text.toString().length()));
            if (m != null) {
                while (m.find()) {
                    String emojiHex = Integer.toHexString(Character.codePointAt(m.group(), 0));
                    //EmojiDisplay.emojiDisplay(editText.getContext(), editText.getText(), emojiHex, emojiSize, start + m.start(), start + m.end());
                }
            }
        }

        private void clearSpan(Spannable spannable, int start, int end) {
            if (start == end) {
                return;
            }
            EmojiSpan[] oldSpans = spannable.getSpans(start, end, EmojiSpan.class);
            for (int i = 0; i < oldSpans.length; i++) {
                spannable.removeSpan(oldSpans[i]);
            }
        }
    }

    public static void initEmotionsEditText(EmoticonsEditText etChat) {
        etChat.addEmoticonFilter(new EmojiFilter());
    }

    public static EmoticonClickListener getCommonEmoticonClickListener(final EditText editText) {
        return new EmoticonClickListener() {
            @Override
            public void onEmoticonClick(Object o, int actionType, boolean isDelBtn) {
                if (isDelBtn) {
                    delClick(editText);
                } else {
                    if (o == null) {
                        return;
                    }
                    if (actionType == ZCKeyboardConst.EMOTICON_CLICK_TEXT) {
                        String content = null;
                        if (o instanceof EmojiBean) {
                            content = ((EmojiBean) o).emoji;
                        }

                        if (TextUtils.isEmpty(content)) {
                            return;
                        }
                        int index = editText.getSelectionStart();
                        Editable editable = editText.getText();
                        editable.insert(index, content);
                    } else if (actionType == ZCKeyboardConst.EMOTICON_CLICK_BIGIMAGE) {
                        if (o instanceof EmoticonEntity) {
                            //OnSendImage(((EmoticonEntity)o).getIconUri());
                        }
                    }
                }
            }
        };
    }

    public static PageSetAdapter getCommonAdapter(Context context, EmoticonClickListener emoticonClickListener) {

        if (sCommonPageSetAdapter != null) {
            return sCommonPageSetAdapter;
        }

        PageSetAdapter pageSetAdapter = new PageSetAdapter();

        addEmojiPageSetEntity(pageSetAdapter, context, emoticonClickListener);

        addHuyenHeoPageSetEntity(pageSetAdapter, context, emoticonClickListener);

        addViVongPageSetEntity(pageSetAdapter, context, emoticonClickListener);


        return pageSetAdapter;
    }

    private static void addViVongPageSetEntity(PageSetAdapter pageSetAdapter, Context context, EmoticonClickListener emoticonClickListener) {
        String filePath = FileUtils.getFolderPath("stickershpooky");
        EmoticonPageSetEntity<EmoticonEntity> emoticonPageSetEntity = parseDataFromFile(context, filePath, "stickershpooky.zip", "stickershpooky.xml");

        if (emoticonPageSetEntity == null) {
            return;
        }

        EmoticonPageSetEntity pageSetEntity
                = new EmoticonPageSetEntity.Builder()
                .setLine(emoticonPageSetEntity.getLine())
                .setRow(emoticonPageSetEntity.getRow())
                .setEmoticonList(emoticonPageSetEntity.getEmoticonList())
                .setIPageViewInstantiateItem(getEmoticonPageViewInstantiateItem(BigEmoticonsAdapter.class, emoticonClickListener))
                .setIconUri(R.drawable.shpooky_1)
                .setShowIndicator(true)
                .setShowDelBtn(EmoticonPageEntity.DelBtnStatus.GONE)
                .build();
        pageSetAdapter.add(pageSetEntity);
        pageSetAdapter.notifyDataSetChanged();
    }

    public static void addHuyenHeoPageSetEntity(PageSetAdapter pageSetAdapter, Context context, EmoticonClickListener emoticonClickListener) {
        String filePath = FileUtils.getFolderPath("stickerwalowpig");
        EmoticonPageSetEntity<EmoticonEntity> emoticonPageSetEntity = parseDataFromFile(context, filePath, "stickerwalowpig.zip", "stickerwalowpig.xml");

        if (emoticonPageSetEntity == null) {
            return;
        }

        EmoticonPageSetEntity pageSetEntity
                = new EmoticonPageSetEntity.Builder()
                .setLine(emoticonPageSetEntity.getLine())
                .setRow(emoticonPageSetEntity.getRow())
                .setEmoticonList(emoticonPageSetEntity.getEmoticonList())
                .setIPageViewInstantiateItem(getEmoticonPageViewInstantiateItem(BigEmoticonsAdapter.class, emoticonClickListener))
                .setIconUri(R.drawable.walowpig_13)
                .setShowIndicator(true)
                .setShowDelBtn(EmoticonPageEntity.DelBtnStatus.GONE)
                .build();
        pageSetAdapter.add(pageSetEntity);
        pageSetAdapter.notifyDataSetChanged();
    }

    public static void addEmojiPageSetEntity(PageSetAdapter pageSetAdapter, Context context, final EmoticonClickListener emoticonClickListener) {
        ArrayList<EmojiBean> emojiArray = new ArrayList<>();
        Collections.addAll(emojiArray, DefEmoticons.getDefEmojiArray());
        EmoticonPageSetEntity emojiPageSetEntity
                = new EmoticonPageSetEntity.Builder()
                .setLine(3)
                .setRow(7)
                .setEmoticonList(emojiArray)
                .setIPageViewInstantiateItem((PageViewInstantiateListener<EmoticonPageEntity>) (viewGroup, i, pageEntity) -> {
                    if (pageEntity.getRootView() == null) {
                        EmoticonPageView pageView = new EmoticonPageView(viewGroup.getContext());
                        pageView.setNumColumns(pageEntity.getRow());
                        pageEntity.setRootView(pageView);
                        try {
                            EmoticonsAdapter adapter = new EmoticonsAdapter(viewGroup.getContext(), pageEntity, null);
                            // emoticon instantiate
                            adapter.setOnDisPlayListener(getCommonEmoticonDisplayListener(emoticonClickListener, EMOTICON_CLICK_EMO));
                            pageView.getEmoticonsGridView().setAdapter(adapter);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    return pageEntity.getRootView();
                })
                .setShowDelBtn(EmoticonPageEntity.DelBtnStatus.LAST)
                .setIconUri(R.drawable.emoji_pack_emo)
                .build();
        pageSetAdapter.add(emojiPageSetEntity);
        pageSetAdapter.notifyDataSetChanged();
    }


    @SuppressWarnings("unchecked")
    public static Object newInstance(Class _Class, Object... args) throws Exception {
        return newInstance(_Class, 0, args);
    }

    @SuppressWarnings("unchecked")
    public static Object newInstance(Class _Class, int constructorIndex, Object... args) throws Exception {
        Constructor cons = _Class.getConstructors()[constructorIndex];
        return cons.newInstance(args);
    }

    public static PageViewInstantiateListener<EmoticonPageEntity> getDefaultEmoticonPageViewInstantiateItem(final EmoticonDisplayListener<Object> emoticonDisplayListener) {
        return getEmoticonPageViewInstantiateItem(EmoticonsAdapter.class, null, emoticonDisplayListener);
    }

    public static PageViewInstantiateListener<EmoticonPageEntity> getEmoticonPageViewInstantiateItem(final Class<BigEmoticonsAdapter> _class, EmoticonClickListener onEmoticonClickListener) {
        return getEmoticonPageViewInstantiateItem(_class, onEmoticonClickListener, null);
    }

    public static PageViewInstantiateListener<EmoticonPageEntity> getEmoticonPageViewInstantiateItem(final Class _class, final EmoticonClickListener onEmoticonClickListener, final EmoticonDisplayListener<Object> emoticonDisplayListener) {
        return (container, position, pageEntity) -> {
            if (pageEntity.getRootView() == null) {
                EmoticonPageView pageView = new EmoticonPageView(container.getContext());
                pageView.setNumColumns(pageEntity.getRow());
                pageEntity.setRootView(pageView);
                try {
                    EmoticonsAdapter adapter = (EmoticonsAdapter) newInstance(_class, container.getContext(), pageEntity, onEmoticonClickListener);
                    if (emoticonDisplayListener != null) {
                        adapter.setOnDisPlayListener(emoticonDisplayListener);
                    }
                    pageView.getEmoticonsGridView().setAdapter(adapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return pageEntity.getRootView();
        };
    }

    public static EmoticonDisplayListener<Object> getCommonEmoticonDisplayListener(final EmoticonClickListener onEmoticonClickListener, final int type) {
        return (position, parent, viewHolder, object, isDelBtn) -> {
            final EmojiBean emojiBean = (EmojiBean) object;
            if (emojiBean == null && !isDelBtn) {
                return;
            }

            viewHolder.ly_root.setBackgroundResource(com.keyboard.view.R.drawable.bg_emoticon);

            if (isDelBtn) {
                viewHolder.iv_emoticon.setImageResource(R.drawable.ic_backspace_solid);
            } else {
                viewHolder.iv_emoticon.setImageResource(emojiBean.icon);
            }

            viewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEmoticonClickListener.onEmoticonClick(emojiBean, type, isDelBtn);
                }
            });
        };
    }

    public static void delClick(EditText editText) {
        int action = KeyEvent.ACTION_DOWN;
        int code = KeyEvent.KEYCODE_DEL;
        KeyEvent event = new KeyEvent(action, code);
        editText.onKeyDown(KeyEvent.KEYCODE_DEL, event);
    }
}
