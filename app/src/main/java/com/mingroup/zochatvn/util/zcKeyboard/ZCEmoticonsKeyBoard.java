package com.mingroup.zochatvn.util.zcKeyboard;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.mingroup.zochatvn.R;

import sj.keyboard.XhsEmoticonsKeyBoard;
import sj.keyboard.utils.EmoticonsKeyboardUtils;

public class ZCEmoticonsKeyBoard extends XhsEmoticonsKeyBoard {
    private OnChatKeyboardListener onChatKeyboardListener;
    private ImageView btnChatImage, btnSend;
    private boolean isToggleMultimedia = false;
    private final int APP_HEIGHT = 533;

    public void setOnChatKeyboardListener(OnChatKeyboardListener onChatKeyboardListener) {
        this.onChatKeyboardListener = onChatKeyboardListener;
    }

    public ZCEmoticonsKeyBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    protected void inflateKeyboardBar() {
        //super.inflateKeyboardBar();
        View v = mInflater.inflate(R.layout.view_zochat_keyboard, this);
        btnSend = v.findViewById(R.id.btnSend);
        btnChatImage = v.findViewById(R.id.btnChatImage);
        btnSend.setOnClickListener(this);
        btnChatImage.setOnClickListener(this);
    }

    @Override
    protected View inflateFunc() {
        //return super.inflateFunc();
        return mInflater.inflate(R.layout.view_func_emoticon, null);
    }

    @Override
    public void reset() {
        super.reset();
        EmoticonsKeyboardUtils.closeSoftKeyboard(getContext());
        mLyKvml.hideAllFuncView();
        mBtnFace.setImageResource(R.drawable.ic_fb_emoji);
    }

    @Override
    public void initEditView() {
        mEtChat.setOnTouchListener((v, event) -> {
            if (!mEtChat.isFocused()) {
                mEtChat.setFocusable(true);
                mEtChat.setFocusableInTouchMode(true);
            }
            return false;
        });

        mEtChat.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    btnSend.setVisibility(GONE);
                    btnChatImage.setVisibility(VISIBLE);
                    mBtnMultimedia.setVisibility(VISIBLE);
                } else {
                    mBtnMultimedia.setVisibility(GONE);
                    btnChatImage.setVisibility(GONE);
                    btnSend.setVisibility(VISIBLE);
                }

            }
        });
    }


    @Override
    public void onFuncChange(int key) {
        //super.onFuncChange(key);
        if (FUNC_TYPE_EMOTION == key) {
            mBtnFace.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.blue_magenta, getContext().getTheme())));
        } else {
            mBtnFace.setImageTintList(null);
        }
    }

    @Override
    public void OnSoftClose() {
        super.OnSoftClose();
        if (mLyKvml.getCurrentFuncKey() == FUNC_TYPE_APPPS) {
            setFuncViewHeight(APP_HEIGHT);
        }
    }

    @Override
    protected void showText() {
        //super.showText();
        mEtChat.setVisibility(VISIBLE);
        mBtnFace.setVisibility(VISIBLE);
    }

    @Override
    protected void showVoice() {
        super.showVoice();
        mEtChat.setVisibility(GONE);
        mBtnFace.setVisibility(GONE);
        reset();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChatImage:
                if (onChatKeyboardListener != null) {
                    onChatKeyboardListener.onChatImageClickListener(v);
                }
                break;
            case R.id.btnSend:
                if (onChatKeyboardListener != null) {
                    onChatKeyboardListener.onSendButtonClickListener(v);
                }
                break;
            case R.id.btn_multimedia:
                if (!isToggleMultimedia) {
                    mBtnMultimedia.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.blue_magenta, getContext().getTheme())));
                } else {
                    mBtnMultimedia.setImageTintList(null);
                }
                toggleFuncView(FUNC_TYPE_APPPS);
                setFuncViewHeight(EmoticonsKeyboardUtils.getDefKeyboardHeight(getContext()));
                isToggleMultimedia = !isToggleMultimedia;
                break;
            case R.id.btn_face:
                toggleFuncView(FUNC_TYPE_EMOTION);
            default:
                break;

        }
    }

    public interface OnChatKeyboardListener {
        void onChatImageClickListener(View view);
        void onSendButtonClickListener(View view);
    }
}