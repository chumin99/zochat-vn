package com.mingroup.zochatvn.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.mingroup.zochatvn.R;


public class ZCLoadingDialog {
    public enum Style {
        BASIC_STYLE, MODERN_STYLE
    }

    private static final String DEFAULT_LABEL = "Đang tải...";
    private Context mContext;
    private AlertDialog mLoadingDialog;
    private String mLabel;
    private int mRawRes;
    private Style mStyle;

    public ZCLoadingDialog(Context mContext) {
        setStyle(Style.BASIC_STYLE);
        mLabel = DEFAULT_LABEL;
        this.mContext = mContext;
    }

    public static ZCLoadingDialog create(Context context) {
        return new ZCLoadingDialog(context);
    }

    private void initLoadingDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(mContext);
        View dialogView = LayoutInflater.from(mContext).inflate(R.layout.zochat_loading, null);

        builder.setView(dialogView);
        builder.setCancelable(false);

        LottieAnimationView lavLoading = dialogView.findViewById(R.id.progress_loading);
        lavLoading.setAnimation(mRawRes);
        lavLoading.playAnimation();

        if (this.mStyle == Style.BASIC_STYLE) {
            TextView tvLabel = dialogView.findViewById(R.id.tvLabel);
            tvLabel.setText(mLabel);
            LinearLayout mContainerLayout = dialogView.findViewById(R.id.loading_container);
            mContainerLayout.setBackground(mContext.getDrawable(R.drawable.shape_background_loading));
            lavLoading.getLayoutParams().height = 30;
            lavLoading.getLayoutParams().width = 30;
        }

        mLoadingDialog = builder.create();

        Window window = mLoadingDialog.getWindow();
        Point size = new Point();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.dimAmount = 0.5f;
        window.setAttributes(layoutParams);

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        window.setLayout((int) (size.x * 0.3), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
    }

    public ZCLoadingDialog show() {
        if (!isShowing()) {
            initLoadingDialog();
            mLoadingDialog.show();
        }
        return this;
    }

    public ZCLoadingDialog setStyle(Style style) {
        switch (style) {
            case BASIC_STYLE:
                mRawRes = R.raw.zochat_loading;
                mStyle = Style.BASIC_STYLE;
                break;
            case MODERN_STYLE:
                mRawRes = R.raw.zochat_loading_2;
                mStyle = Style.MODERN_STYLE;
                break;
        }
        return this;
    }

    public ZCLoadingDialog setmLabel(String text) {
        mLabel = text;
        return this;
    }

    public boolean isShowing() {
        return mLoadingDialog != null && mLoadingDialog.isShowing();
    }

    public void dismiss() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
        }
    }
}
