package com.mingroup.zochatvn.util;

import java.util.regex.Pattern;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ValidatorUtil {
    private static final String VIETNAMESE_PHONE_REGEX = "(03|07|08|09|01[2|6|8|9])+([0-9]{8})\\b";
    private static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    private static final String VIETNAMESE_ALPHABET_REGEX = "\\p{L}";

    //Password matching expression. Password must be at least 6 characters, no more than 32 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit.
    private static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,32}$";

    public static boolean isVietnamesePhone(String phoneNumber) {
        return Pattern.matches(VIETNAMESE_PHONE_REGEX, StringUtil.generatePhoneNumber(phoneNumber));
    }

    public static boolean isConfirmPasswordMatch(String password, String confirmPassword) {
        if (!password.equals(confirmPassword))
            return false;
        return true;
    }

    public static boolean isValidEmail(String email) {
        return Pattern.matches(EMAIL_REGEX, email);
    }

    public static boolean isValidPasswordForChangePassword(String password, String displayName) {
        boolean regexFlag = Pattern.matches(PASSWORD_REGEX, password);
        return regexFlag;
    }

    public static boolean isAlphabet(String str) {
        return Pattern.matches(VIETNAMESE_ALPHABET_REGEX, str);
    }
}
