package com.mingroup.zochatvn.util.zcKeyboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.mingroup.zochatvn.R;

import java.util.ArrayList;

public class AppsGridView extends RelativeLayout {
    protected View view;
    private ChattingAppsAdapter.OnFuncAppClickListener onFuncAppClickListener;

    public AppsGridView(Context context, ChattingAppsAdapter.OnFuncAppClickListener onFuncAppClickListener) {
        super(context, null);
        this.onFuncAppClickListener = onFuncAppClickListener;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.view_apps, this);
        init();
    }

    protected void init() {
        GridView gv_apps = (GridView) view.findViewById(R.id.gv_apps);
        ArrayList<AppBean> mAppBeanList = new ArrayList<>();
        mAppBeanList.add(new AppBean(1, R.mipmap.icon_photo, "Hình ảnh"));
        mAppBeanList.add(new AppBean(2, R.mipmap.icon_camera, "Camera"));
        mAppBeanList.add(new AppBean(3, R.mipmap.icon_audio, "Video"));
        mAppBeanList.add(new AppBean(4, R.mipmap.icon_file, "File"));
        ChattingAppsAdapter adapter = new ChattingAppsAdapter(getContext(), mAppBeanList, onFuncAppClickListener);
        gv_apps.setAdapter(adapter);
    }
}