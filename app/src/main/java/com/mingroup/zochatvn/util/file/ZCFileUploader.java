package com.mingroup.zochatvn.util.file;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mingroup.zochatvn.constant.StorageRefConst;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lombok.SneakyThrows;

import static com.mingroup.zochatvn.util.file.FileUtils.getFileFromUri;

public class ZCFileUploader {
    private static final String TAG = ZCFileUploader.class.getSimpleName();
    private Context context;
    private List<Uri> requestUris;
    private List<Uri> responseUris;
    public ZCFileUploader.FileUploaderCallback fileUploaderCallback;
    private StorageReference mStorageRef;
    private String folderName;
    public int uploadIndex = 0;
    private long totalFileLength = 0;
    private long totalFileUploaded = 0;

    public interface FileUploaderCallback {
        void onError(Exception e);

        void onFinish(List<Uri> responses);

        void onProgressUpdate(long currentpercent, long totalpercent, long filenumber);
    }

    public ZCFileUploader(Context context) {
        this.context = context;
        this.mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    public void uploadFiles(String folderName, List<Uri> files, ZCFileUploader.FileUploaderCallback fileUploaderCallback) {
        this.folderName = folderName == null ? StorageRefConst.FOLDER_CONSERVATION_PHOTO : folderName;
        this.fileUploaderCallback = fileUploaderCallback;
        this.requestUris = files;
        this.uploadIndex = 0;
        this.totalFileUploaded = 0;
        this.totalFileLength = 0;
        this.responseUris = new ArrayList<>();
        files.forEach(uri -> {
            try {
                totalFileLength += getFileFromUri(context, uri).length();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        doUploadFile();
    }

    private void doUploadFile() {
        if(requestUris != null && !requestUris.isEmpty()) {
            requestUris.forEach(this::uploadSingleFile);
        } else {
            fileUploaderCallback.onFinish(responseUris);
        }
    }

    @SneakyThrows
    private void uploadSingleFile(Uri uri) {
        File file = FileUtils.getFileFromUri(context, uri);
        mStorageRef.child(folderName)
                .child(file.getName())
                .putFile(uri)
                .addOnSuccessListener(taskSnapshot -> {
                            Task<Uri> task = Objects.requireNonNull(Objects.requireNonNull(taskSnapshot.getMetadata()).getReference()).getDownloadUrl();
                    task
                            .addOnSuccessListener(uploadedUri -> {
                                uploadIndex++;
                                totalFileUploaded += file.length();
                                responseUris.add(uploadedUri);
                                if(uploadIndex == requestUris.size()) {
                                    fileUploaderCallback.onFinish(responseUris);
                                }
                            })
                            .addOnFailureListener(e -> Log.d(TAG, "getDownloadUrl: " + e.getMessage()));
                })
                .addOnProgressListener(snapshot -> {
                    long current_percent = 100 * (snapshot.getBytesTransferred()/snapshot.getTotalByteCount());
                    long total_percent = (int) (100 * (totalFileUploaded + snapshot.getBytesTransferred()) / totalFileLength);
                    fileUploaderCallback.onProgressUpdate(current_percent, total_percent, requestUris.indexOf(uri) + 1);
                })
                .addOnFailureListener(e -> {
                    Log.d(TAG, "uploadSingleFile" + e.getMessage());
                    fileUploaderCallback.onError(e);
                });
    }
}
