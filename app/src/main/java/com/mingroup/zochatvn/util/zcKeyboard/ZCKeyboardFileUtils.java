package com.mingroup.zochatvn.util.zcKeyboard;

import android.content.Context;

import com.mingroup.zochatvn.util.file.FileUtils;

import java.io.File;
import java.io.IOException;

import sj.keyboard.data.EmoticonEntity;
import sj.keyboard.data.EmoticonPageSetEntity;

public class ZCKeyboardFileUtils {

    public static EmoticonPageSetEntity<EmoticonEntity> parseDataFromFile(Context context, String filePath, String assetsFileName, String xmlName) {
        String xmlFilePath = filePath + "/" + xmlName;
        File file = new File(xmlFilePath);
        if (!file.exists()) {
            try {
                FileUtils.unzip(context.getAssets().open(assetsFileName), filePath);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        XmlUtil xmlUtil = new XmlUtil(context);
        return xmlUtil.ParserXml(filePath, xmlUtil.getXmlFromSD(xmlFilePath));
    }
}
