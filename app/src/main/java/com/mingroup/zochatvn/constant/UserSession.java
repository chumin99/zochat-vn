package com.mingroup.zochatvn.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UserSession {
    public static final String SESSION_USER_SESSION = "userLoginSession";
    public static final String IS_REMEMBER_ME = "isLoggedIn";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_UID = "uid";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_DISPLAY_NAME = "displayName";
    public static final String KEY_DOB = "dob";
    public static final String KEY_IS_MALE = "isMale";
    public static final String KEY_AVATAR = "avatar";
    public static final String KEY_IS_DISABLED = "isDisabled";
    public static final String KEY_LAST_UPDATE_PHONEBOOK = "lastUpdatePhonebook";
    public static final String TOKEN = "token";
    public static final String BEARER = "Bearer ";
    public static final String AUTHORIZATION = "Authorization";
}
