package com.mingroup.zochatvn.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class LocalDatabaseConst {
    public static final String DATABASE_NAME = "zochatdb";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_PHONEBOOK = "PHONEBOOK";
    public static final String KEY_ID = "_id";
    public static final String KEY_DISPLAY_NAME = "displayName";
    public static final String KEY_PHONE_NUMBER = "phoneNumber";
}
