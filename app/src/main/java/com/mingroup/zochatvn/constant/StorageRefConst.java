package com.mingroup.zochatvn.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StorageRefConst {
    public static final String FOLDER_USER_AVATAR = "user_avatar";
    public static final String FOLDER_GROUP_PHOTO = "group_photo";
    public static final String FOLDER_DEFAULT = "default";
    public static final String FOLDER_CONSERVATION_PHOTO = "conservation_photos";
}
