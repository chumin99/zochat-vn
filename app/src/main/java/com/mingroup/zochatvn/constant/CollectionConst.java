package com.mingroup.zochatvn.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CollectionConst {
    public static final String COLLECTION_MESSAGE = "messages";
    public static final String COLLECTION_USER = "users";
    public static final String COLLECTION_CONSERVATION = "conservations";
    public static final String COLLECTION_FRIEND_REQUEST = "friend_requests";
}
