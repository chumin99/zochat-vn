package com.mingroup.zochatvn.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AuthMethod {
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String GOOGLE = "google";
    public static final String FACEBOOK = "facebook";
}
